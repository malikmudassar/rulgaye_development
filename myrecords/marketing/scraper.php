<?php
/* Autho: Rizwan Abbas rizwan@zeropoint.it
 * Organization: zeropoint.it
 * This script crawls the data from OLX only
 * Currently it targets cars category and all its html elements
 * 
 */
include_once('simple_html_dom.php');
error_reporting(E_ALL);
ini_set('display_errors', 1);

function getBetween($var1="",$var2="",$pool){
	$temp1 = strpos($pool,$var1)+strlen($var1);
	$result = substr($pool,$temp1,strlen($pool));
	$dd=strpos($result,$var2);
	if($dd == 0){
		$dd = strlen($result);
	}
}
	
function getDetails($url) {
	 
	   $html = file_get_html($url);
        
	    
	   $attributes	=	array();
	  
		$attributes['attributes']['Ref No']	=	trim($html ->find('span[class=rel inlblk]',0)->innertext); 
		if($attributes['attributes']['Ref No']==''){

			return array();
		}
										
	//	$attributes['jobinfo']['price'] =	trim(ltrim(str_replace(',','',@$html ->find('strong[class=xxxx-large margintop7 inlblk not-arranged]',0)->innertext),' Rs'));
			
	//		if($attributes['jobinfo']['price']==''){
	//							$attributes['jobinfo']['price'] = trim(ltrim(str_replace(',','',@$html ->find('strong[class=xxxx-large margintop7 inlblk not-arranged]',0)->innertext),' Rs'));
	//		}
		
	 	$attributes['jobinfo']['Title'] 	=	trim($html ->find('h1[class=brkword lheight28]',0)->innertext); 
		if($attributes['jobinfo']['Title']==''){
		$attributes['jobinfo']['Title'] 	=	trim($html ->find('div[class=clr offerheadinner pding15 pdingright20]>h1',0)->innertext); 
		}
		

		
		$attributes['attributes']['Salary Period']	=	trim($html ->find('strong[class=block]',1)->innertext);  // this give model not year 
						//	details fixed marginbott20 margintop5	
	//	$attributes['attributes']['Year']	=	trim(str_replace('Year:','',strip_tags(trim(@$html ->find('table[class=details fixed marginbott20 margintop5] >td',2)->innertext))));  // this gives year not Engine Type
		
		$attributes['jobinfo']['Position Type'] = trim(str_replace('Position type:','',strip_tags(trim(@$html ->find('table[class=details fixed marginbott20 margintop5] >td',0)->innertext))));

		/*if($html ->find('div[class=pding5_10]',3)){
			$attributes['attributes']['Engine Type'] =	trim(rtrim(trim(str_replace(',','',$html ->find('div[class=pding5_10]',3)->find('strong[class=block]',0)->innertext)),'km'));  // this gives engine type not mileage
		}


		if($html ->find('div[class=pding5_10]',4)){
			$attributes['attributes']['Mileage'] =	trim(rtrim(trim(str_replace(',','',$html ->find('div[class=pding5_10]',4)->find('strong[class=block]',0)->innertext)),'km'));  // this gives engine type not mileage
		}	

	*/

		$attributes['attributes']['Description']	=	nl2br(strip_tags(trim($html ->find('p[class=pding10 lheight20 large]',0)->innertext))); 
		$attributes['jobinfo']['Description'] =trim($attributes['attributes']['Description']);
	  	$attributes['jobinfo']['url'] = $url;	
	  	$attributes['jobinfo']['portal_ad_id'] = trim($attributes['attributes']['Ref No']);
	  	$attributes['adinfo']['source'] = 'olx';
	  	$attributes['adinfo']['url'] = $url;

	  	//$attributes['contactinfo']['Cell Number'] =trim(str_replace(' ','', trim($html ->find('strong[class=xx-large lheight20 fnormal abtest-3333-h]',0)->innertext)));
	  	//$attributes['contactinfo']['Contact Name'] = trim($html ->find('span[class=block color-5 brkword xx-large]',0)->innertext);
	  	
	  	$i=0;
        $image ='';
        $imageTag= str_replace(' ','-',strtolower($attributes['jobinfo']['Title']));
        $images	=	$html ->find('.fleft>a');
        $attributes['jobinfo']['images']	=	array();
        foreach($images as $img){
            if(!@strpos($img,$imageTag)){
            	continue;
            }
            $imgurl=str_replace('94x72','1000x700',$img->rel);
            if($i==0){
                 $image  = $imgurl;
            }
            $attributes['jobinfo']['images'][]  = $imgurl;
            $i++;
        }
       
        if(sizeof($attributes['jobinfo']['images'])>1){
        	unset($attributes['jobinfo']['images'][sizeof($attributes['jobinfo']['images'])-1]);
    	}
    	
    	$attributes['jobinfo']['image'] = $image;
		return $attributes;
}

//dump single url
if(isset($_GET['url'])){
	//echo $url=$_GET['url'];
	$attributes	=	getDetails($url);
	//print"<pre>";
	//print_r($attributes);
}

?>
