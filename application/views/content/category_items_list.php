<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/11/2015
 * Time: 7:10 PM
 */

?>
<div class="container">
    <!-- left side -->
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 list-grid">
        <h5>
            <a href=""><?php echo $cat_name['category']?></a> <i class="fa fa-angle-double-right"></i> <a href=""> <?php echo $cat_name['subcategory']?></a>
        </h5>
        <?php
        if(count($items)>0)
        {
        ?>

        <hr>
        <div class="row">
            <?php

                for($i=0;$i<count($items);$i++) {
                    $images=explode(',',$items[$i]['my_images']);

                        if($items[$i]['cat_id']==4)
                        {
                            $images[0]='avatar_job.png';
                        }


                    ?>
                    <div class="col-xs-12 item-listbox">
                        <div class="item-box">

                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                                <a href="<?php echo base_url().'item/itemDetail/'.$items[$i]['id'].'/'.implode('-',explode(' ',$items[$i]['title']))?>">

                                    <img src="<?php echo base_url().'img/'.$images[0]?>" class="img-thumbnail img-responsive"/>
                                </a>

                                <center>
                                    <!--<div class="item-author">
                                        By <span><?php /*echo $items[$i]['fname'].'-'.$items[$i]['lname']*/?></span>
                                    </div>-->
                                    <div class="item-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </center>

                            </div>

                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <div class="item_details">
                                    <div class="item-title">
                                        <h5><b><a href="<?php echo base_url().'item/itemDetail/'.$items[$i]['id'].'/'.implode('-',explode(' ',$items[$i]['title']))?>"><?php echo $items[$i]['title']?></a></b></h5>
                                    </div>
                                    <div class="item-cat">
                                        <?php echo $items[$i]['category']?> - <?php echo $items[$i]['sub_category']?>
                                    </div>
                                    <div class="item-cat">
                                        <p><?php echo substr($items[$i]['description'],0,200)?></p>
                                    </div>


                                    <div class="item-location">
                                        In <span style="font-weight: bolder"><?php echo $items[$i]['city_name']?></span> ,
                                        <span style="font-weight: bolder"><?php echo $items[$i]['state_name']?></span>
                                    </div>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <div class="pull-right">
                                    <div class="item-price">
                                        <b><i class="fa fa-inr"> </i> <?php echo $items[$i]['amount']?>.00</b>
                                    </div>
                                    <div class="item-date">
                                        <?php echo date('M, d Y',strtotime($items[$i]['date']))?>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                <?php
                }
                if($pagination){
                ?>
                 <div class="row" >
                     <div style="text-align: center">
                        <?php echo $pagination;?>
                     </div>
                 </div>
            <?php
                }}
                else
                {
                ?>
            <div class="pull-right list-icons">
                <a href="" ><i class="fa fa-th"></i></a>
                <a href="" ><i class="fa fa-list"></i></a>

            </div>
            <hr>
            <div class="row"></div>
                <div class="row">

                <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Notice!</strong> <?php echo ('No item is currently being listed in this Category');?>
                </div>
                    <div class="home-category">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h3>BUY/SELL PRODUCTS</h3>
                            <div class="row">
                                <?php
                                for($j=0;$j<count($categories[1]['sub']);$j++) {
                                    ?>
                                    <div class="col-xs-5 col-sm-2 col-md-2 col-lg-2 category-bg">
                                        <a href="#">
                                            <i class="fa <?php echo $categories[1]['sub'][$j]['icon']?>"></i>
                                            <h5><a href="<?php echo base_url().'item/catListing/'.$categories[1]['sub'][$j]['id']?>"><?php echo $categories[1]['sub'][$j]['name']?></a></h5>
                                        </a>
                                    </div>
                                <?php
                                }
                                ?>


                            </div>
                            </div>
                        </div>
                    <div class="home-category">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h3>BUY/SELL AUTOMOBILES</h3>
                            <div class="row">
                                <?php
                                for($j=0;$j<count($categories[0]['sub'])-1;$j++) {
                                    ?>
                                    <div class="col-xs-5 col-sm-2 col-md-2 col-lg-2 category-bg">
                                        <a href="#">
                                            <i class="fa <?php echo $categories[0]['sub'][$j]['icon']?>"></i>
                                            <h5><a href="<?php echo base_url().'item/catListing/'.$categories[0]['sub'][$j]['id']?>"><?php echo $categories[0]['sub'][$j]['name']?></a></h5>
                                        </a>
                                    </div>
                                <?php
                                }
                                ?>


                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>

        </div>
        <!--<div class="pagbox">
            <ul class="pagination">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
        </div>-->
    </div>
    <!-- left side end -->
    <!-- right side -->
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 adv-search">
        <div class="filter-search">
            <h3>Advanced Search</h3>
            <form action="<?php echo base_url().'search'?>" method="post">
            <div class="price-slider">
                <h5>Price</h5>

                <input name="price_from" type="number" required class="form-control" id="term" placeholder="Price From" onkeypress="return isNumberKey(event)"/>
                <input name="price_to" type="number" required class="form-control" id="term" placeholder="Price to" onkeypress="return isNumberKey(event)"/>

            </div>
            <div class="loc-filter">
                <h5>Product Category</h5>
                <select class="form-control" name="category" id="searchCategory" >
                    <option value="">Select Product Category</option>
                    <?php
                    for($i=0;$i<count($categories);$i++){?>
                    <option value="<?php echo $categories[$i]['id']?>"><?php echo $categories[$i]['name']?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="loc-filter">
                <h5>Product Sub Category</h5>
                <select class="form-control" name="sub_category" id="sub_category" >
                    <option value="">Select Sub Category</option>

                </select>
            </div>
            <div class="loc-filter">
                <h5>Location</h5>
                <select class="form-control" id="state" name="state" >
                    <option value="">Select State</option>
                    <?php
                    for($i=0;$i<count($cities);$i++) {
                        ?>

                        <option value="<?php echo $cities[$i]['id'] ?>"><?php echo $cities[$i]['name'] ?></option>
                    <?php
                    }
                    ?>
                </select>
                <select class="form-control" id="city" name="city" >
                    <option value="">Select City</option>
                </select>

            </div>

            <div class="loc-filter">
                <button type="submit" class="btn btn-danger btn-site btn-block ">Search</button>
            </div>
            </form>
        </div>
        <div class="cat-list">
            <h5>Browse Categories</h5>
            <ul>
                <?php
                for($i=0;$i<count($similar_categories);$i++) {
                    ?>
                    <li><a href="<?php echo base_url().'item/catListing/'.$similar_categories[$i]['id']?>"><?php echo $similar_categories[$i]['category']?><span>&nbsp;(<?php echo $similar_categories[$i]['items']?>)</span></a></li>
                <?php
                }
                ?>

            </ul>
        </div>
    </div>
    <!--right side end -->
</div>
    <script>
        $("#ex2").slider({});

        $('#searchCategory').change(function(){
            var id = $('#searchCategory').val();
            $.ajax({
                type: "POST",
                url: base_url+"home/getSubCategory",
                data: {id:id},
                success: function(data) {
                    $("#sub_category").html(data);
                }
            });
        });

        $('#state').change(function(){
            var id = $('#state').val();
            $.ajax({
                type: "POST",
                url: base_url+"home/getCities",
                data: {id:id},
                success: function(data) {
                    $("#city").html(data);
                }
            });
        });

        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
                return false;
            return true;
        }

    </script>