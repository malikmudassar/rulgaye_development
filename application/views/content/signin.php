<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/7/2015
 * Time: 2:20 PM
 */
?>
<div id="fb-root"></div>
<div class="container">
    <div class="login-container">
        <div class="container">
        <form action="" method="post" class="signup-form">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h4>Sign In</h4>
                <div class="username">
                    <label class="control-label">Email</label>
                    <input type="text" name="email" class="form-control" placeholder="youremail@domain.com"/>
                </div>
                <div class="username">
                    <label class="control-label">Password</label>
                    <input type="password" name="password" class="form-control" placeholder="********"/>
                </div>
                <button type="submit" class="btn btn-primary">Log In</button>
                <a href="<?php echo base_url().'user/forgetPassword'?>" class="forget-pwd">Forgot Password?</a>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 social-login">
                    <a href="<?php echo base_url().'login'?>" class="btn btn-block btn-social btn-lg btn-facebook">
                        <i class="fa fa-facebook"></i>
                        Sign in with Facebook
                    </a>
                </div>
            </div>
            <?php
            if(isset($errors)){
                ?>
                <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong> <?php echo ($errors);?>
                </div>
            <?php }?>
        </form>
    </div>
</div>
    </div>
<div class="container">
    <!-- popular ads-->
    <div class="popular_ads">
        <div class="ads-heading">
            <h3>
                Most Viewed Products
            </h3>
        </div>
        <div class="popular-body">

            <div class="container">
                <div class="col-xs-12" >
                    <div class="carousel slide" id="myCarousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <?php
                                    if(count($items)>12)
                                    {
                                        $count_items=12;
                                    }
                                    else
                                    {
                                        $count_items=count($items);
                                    }
                                    ?>
                                    <?php for($i=0;$i<$count_items;$i++){?>
                                        <?php
                                        $images=explode(',',$items[$i]['my_images']);
                                        if(empty($images[0]))
                                        {
                                            if($items[$i]['category']==4)
                                            {
                                                $images[0]='avatar_job.png';
                                            }
                                            elseif($items[$i]['category']==3)
                                            {
                                                $images[0]='avatar_job.png';
                                            }
                                            elseif($items[$i]['category']==2)
                                            {
                                                $images[0]='avatar_motor.png';
                                            }
                                        }
                                        ?>

                                        <li class="col-sm-2">
                                            <div class="fff">

                                                <div class="thumbnail">

                                                    <a href="<?php echo base_url().'item/itemDetail/'.$items[$i]['id'];?>"><img style="height:175px" src="<?php echo base_url().'img/'.$images[0]?>" alt=""></a>
                                                </div>
                                                <div class="caption">
                                                    <p><a href="<?php echo base_url().'item/itemDetail/'.$items[$i]['id'];?>"><?php echo $items[$i]['title']?></a></p>
                                                    <b><i class="fa fa-inr"> </i> <?php echo $items[$i]['amount']?></b>
                                                </div>
                                            </div>
                                        </li>
                                    <?php }?>


                                </ul>
                            </div><!-- /Slide1 -->
                            <!-- /Slide2 -->
                        </div>
                        <!-- /.control-box -->
                    </div><!-- /#myCarousel -->
                </div><!-- /.col-xs-12 -->
            </div><!-- /.container -->
        </div>
    </div>
    <!-- popular ads end-->
    <!-- category start-->

    <!--end-->
</div>