<div class="container">
    <!-- left side -->
    <?php
    if(count($items)>0) {
        ?>
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 list-grid">
            <h3>
                Search Results
            </h3>

            <hr>
            <div class="row">
                <?php
                for ($i = 0; $i < count($items); $i++) {
                    ?>
                    <div class="col-xs-12 item-listbox">
                        <div class="item-box">
                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <?php
                                $images = explode(',', $items[$i]['my_images']);
                                if($items[$i]['cat_id']==4)
                                {
                                    $images[0]='avatar_job.png';
                                }
                                ?>
                                <a href="<?php echo base_url() . 'item/itemDetail/' . $items[$i]['id'] ?>">
                                    <img src="<?php echo base_url() . 'img/' . $images[0] ?>"
                                         class="img-thumbnail img-responsive" alt="<?php echo $items[$i]['title']?>"/>

                                </a>
                                <center>
                                    <!--<div class="item-author">
                                        By <span><?php /*echo $items[$i]['fname'] . ' ' . $items[$i]['lname'] */?></span>
                                    </div>-->
                                    <div class="item-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>

                                </center>

                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <div class="item_details">
                                    <div class="item-title">
                                        <h5>
                                            <b><a href="<?php echo base_url() . 'item/itemDetail/' . $items[$i]['id'] ?>"><?php echo $items[$i]['title'] ?>
                                            </b></a></h5>
                                    </div>

                                    <div class="item-cat">
                                        <?php echo $items[$i]['category'] ?> - <?php echo $items[$i]['sub_category'] ?>
                                    </div>

                                    <div class="item-bio">
                                        <?php echo substr($items[$i]['description'], 0, 200); ?>
                                    </div>
                                    <div class="item-favt">
                                        <div class="pull-left">
                                            <span><a href="<?php echo base_url().'item/addtofavorite/'.$items[$i]['id']?>" class="btn btn-danger btn-site"> Add to favorite</a></span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <div class="pull-right">
                                    <div class="item-price">
                                        <b><i class="fa fa-inr"></i> <?php echo $items[$i]['amount'] ?>.00</b>
                                    </div>
                                    <div class="item-date">
                                        <?php echo date('M, d Y', strtotime($items[$i]['date'])) ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php
                }
                ?>
            </div>

        </div>
    <?php
    }
    else {
        ?>
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 list-grid">
        <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Sorry!</strong> <?php echo ('No search result found as per your requirements');?>
        </div>
        <div class="home-category">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3>BUY/SELL PRODUCTS</h3>
                <div class="row">
                    <?php
                    for($j=0;$j<count($categories[1]['sub']);$j++) {
                        ?>
                        <div class="col-xs-5 col-sm-2 col-md-2 col-lg-2 category-bg">
                            <a href="#">
                                <i class="fa <?php echo $categories[1]['sub'][$j]['icon']?>"></i>
                                <h5><a href="<?php echo base_url().'item/catListing/'.$categories[1]['sub'][$j]['id']?>"><?php echo $categories[1]['sub'][$j]['name']?></a></h5>
                            </a>
                        </div>
                    <?php
                    }
                    ?>


                </div>
            </div>
        </div>
        <div class="home-category">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3>BUY/SELL AUTOMOBILES</h3>
                <div class="row">
                    <?php
                    for($j=0;$j<count($categories[0]['sub'])-1;$j++) {
                        ?>
                        <div class="col-xs-5 col-sm-2 col-md-2 col-lg-2 category-bg">
                            <a href="#">
                                <i class="fa <?php echo $categories[0]['sub'][$j]['icon']?>"></i>
                                <h5><a href="<?php echo base_url().'item/catListing/'.$categories[0]['sub'][$j]['id']?>"><?php echo $categories[0]['sub'][$j]['name']?></a></h5>
                            </a>
                        </div>
                    <?php
                    }
                    ?>


                </div>
            </div>
        </div>
    </div>
    <?php
    }
    ?>
    <!-- left side end -->
    <!-- right side -->
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 adv-search">
        <div class="filter-search">
            <h3>Advanced Search</h3>
            <form action="<?php echo base_url().'search'?>" method="post">
                <div class="price-slider">
                    <h4>Price</h4>

                    <input name="price_from" type="number" required class="form-control" id="term" placeholder="Price From" onkeypress="return isNumberKey(event)"/>
                    <input name="price_to" type="number" required class="form-control" id="term" placeholder="Price to" onkeypress="return isNumberKey(event)"/>

                </div>
                <div class="loc-filter">
                    <h4>Product Category</h4>
                    <select class="form-control" name="category" id="searchCategory" >
                        <option value="">Select Product Category</option>
                        <?php
                        for($i=0;$i<count($categories);$i++){?>
                            <option value="<?php echo $categories[$i]['id']?>"><?php echo $categories[$i]['name']?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="loc-filter">
                    <h4>Product Sub Category</h4>
                    <select class="form-control" name="sub_category" id="sub_category" >
                        <option value="">Select Sub Category</option>

                    </select>
                </div>
                <div class="loc-filter">
                    <h4>Location</h4>
                    <select class="form-control" id="state" name="state" >
                        <option value="">Select State</option>
                        <?php
                        for($i=0;$i<count($cities);$i++) {
                            ?>

                            <option value="<?php echo $cities[$i]['id'] ?>"><?php echo $cities[$i]['name'] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <select class="form-control" id="city" name="city" >
                        <option value="">Select City</option>
                    </select>

                </div>

                <div class="loc-filter">
                    <button type="submit" class="btn btn-danger btn-site btn-block">Search</button>
                </div>
            </form>
        </div>
        <div class="cat-list">
            <h4>Browse Categories</h4>
            <ul>
                <?php
                for($i=0;$i<count($similar_categories);$i++) {
                    ?>
                    <li><a href="<?php echo base_url().'item/catListing/'.$similar_categories[$i]['id']?>"><?php echo $similar_categories[$i]['category']?><span>(<?php echo $similar_categories[$i]['items']?>)</span></a></li>
                <?php
                }
                ?>

            </ul>
        </div>
    </div>
    <!--right side end -->
</div>
<script>
    $("#ex2").slider({});
    $('#searchCategory').change(function(){
        var id = $('#searchCategory').val();
        $.ajax({
            type: "POST",
            url: base_url+"home/getSubCategory",
            data: {id:id},
            success: function(data) {
                $("#sub_category").html(data);
            }
        });
    });

    $('#state').change(function(){
        var id = $('#state').val();
        $.ajax({
            type: "POST",
            url: base_url+"home/getCities",
            data: {id:id},
            success: function(data) {
                $("#city").html(data);
            }
        });
    });

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
            return false;
        return true;
    }
</script>