<div class="container">
    <div class="info-tab">
        <ul class="nav nav-tabs">
            <li class="active"><a href="<?php echo base_url().'user/profile'?>">Ads</a></li>
            <li><a href="<?php echo base_url().'user/messages'?>">Messages</a></li>
            <li><a href="<?php echo base_url().'user/settings'?>">Settings</a></li>
        </ul>
        <div class="tab-content user-ads">
            <div class="tab-pane active" id="ads">
                <div class="tabbable-panel">
                    <div class="tabbable-line">
                        <div class="col-lg-3 col-md-3 col-sm-3 search_table">
                            <label>
                                <input type="search" class="form-control input-sm " placeholder="Ad Title..." aria-controls="example1">
                                <i class="fa fa-search seach_icon"></i>
                            </label>
                        </div>
                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#active" data-toggle="tab">
                                    Active ads </a>
                            </li>

                            <li>
                                <a href="#inactive" data-toggle="tab">
                                    Inactive ads </a>
                            </li>

                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="active">
                                <?php if(count($active_ads)>0){?>

                                    <div id="no-more-tables">
                                        <table class="col-md-12 table-bordered table-hover table-striped table-condensed cf">
                                            <thead class="cf">
                                            <tr>
                                                <th>Image</th>
                                                <th>Ad Title</th>
                                                <th>Date Posted</th>
                                                <th class="numeric">Price</th>
                                                <th class="numeric">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php for($i=0;$i<count($active_ads);$i++){?>
                                                <?php
                                                $images=explode(',',$active_ads[$i]['my_images']);
                                                ?>
                                            <tr>
                                                <td align="center">
                                                    <img style="height:100px;width:100px; border-radius: 50px;" src="<?php echo base_url().'img/'.$images[0]?>" alt="">
                                                </td>
                                                <td data-title="Ad Title">
                                                    <p><?php echo $active_ads[$i]['title']?></p>
                                                    <i class="fa fa-print icn"></i><a href="<?php echo base_url().'item/itemDetail/'.$active_ads[$i]['id']?>">View</a>
                                                    <i class="fa fa-edit icn"></i><a href="<?php echo base_url().'postad/edit/'.$active_ads[$i]['id']?>">Edit</a>
                                                    <i class="fa fa-trash icn"></i><a href="<?php echo base_url().'postad/delItem/'.$active_ads[$i]['id']?>">Delete</a>
                                                </td>
                                                <td data-title="Date"><?php echo date('M, d Y',strtotime($active_ads[$i]['date']))?></td>
                                                <td data-title="Price" class="numeric"><i class="fa fa-inr"></i> <?php echo $active_ads[$i]['amount']?></td>
                                                <td data-title="Action" class="numeric">
                                                    <a class="btn btn-danger" href="<?php echo base_url().'user/deactivate_ad/'.$active_ads[$i]['id']?>" data="<?php echo $active_ads[$i]['id']; ?>" id="myLinkToConfirm" data-confirm-title="Confirmation" data-confirm-message="Are you sure you want to perform this action ?"><i class="fa fa-arrow-down"></i> Pause Ad</a>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php }else{?>
                                    <center>
                                        <div class="thumb-box">
                                            <i class="fa fa-thumb-tack cssAnimation"></i>
                                        </div>
                                        <h4> You Don't have active Ads. Place an ad now!</h4>
                                        <a href="<?php echo base_url().'postad'?>" class=" btn btn-success"><i class="fa fa-plus"></i> Submit an Ad!</a>
                                    </center>
                                <?php }?>

                            </div>
                            <div class="tab-pane inactive-box" id="inactive">
                                <div id="no-more-tables">
                                    <table class="col-md-12 table-bordered table-hover table-striped table-condensed cf">
                                        <thead class="cf">
                                        <tr>
                                            <th>Image</th>
                                            <th>Ad Title</th>
                                            <th>Date Posted</th>
                                            <th class="numeric">Price</th>
                                            <th class="numeric">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i=0;$i<count($Inactive_ads);$i++){?>
                                            <?php
                                            $images=explode(',',$Inactive_ads[$i]['my_images']);
                                            ?>
                                            <tr>
                                                <td align="center">
                                                    <img style="height:100px;width:100px; border-radius: 50px;" src="<?php echo base_url().'img/'.$images[0]?>" alt="">
                                                </td>
                                                <td data-title="Ad Title">
                                                    <p><?php echo $Inactive_ads[$i]['title']?></p>
                                                    <i class="fa fa-print icn"></i><a href="<?php echo base_url().'item/itemDetail/'.$Inactive_ads[$i]['id']?>">View</a>
                                                    <i class="fa fa-edit icn"></i><a href="#">Edit</a>
                                                    <i class="fa fa-trash icn"></i><a href="#">Delete</a>
                                                </td>
                                                <td data-title="Date"><?php echo date('M, d Y',strtotime($Inactive_ads[$i]['date']))?></td>
                                                <td data-title="Price" class="numeric"><i class="fa fa-inr"></i> <?php echo $Inactive_ads[$i]['amount']?></td>
                                                <td data-title="Action" class="numeric">
                                                    <a class="btn btn-success" href="<?php echo base_url().'user/activate_ad/'.$Inactive_ads[$i]['id']?>"><i class="fa fa-arrow-up"></i> Resume Ad </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url().'js/confirm-bootstrap.js'?>"></script>
<script>
    /*$('#myLinkToConfirm').confirmModal();*/
</script>