<div class="container">
    <div class="tab-content">
        <div class="tab-pane active" id="inbox">

            <div class="col-md-2"></div>
            <div class="col-md-8" style="margin-bottom: 25px;">
                <h4 >Subject: <?php echo $message[0]['subject']?> (<a class="btn btn-link" href="<?php echo base_url().'user/messages'?>">Inbox</a>)</h4>
                <div id="no-more-tables" style="margin-bottom: 30px">
                    <?php
                    if(isset($errors)){
                        ?>
                        <div class="alert alert-danger fade in" style="overflow: hidden">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?php echo($errors);?>
                        </div>
                    <?php }?>
                    <form action="" method="post">
                        <textarea name="content" class="form-control" rows="3"></textarea>
                        <input type="hidden" name="comm_id" value="<?php echo $message[0]['comm_id']?>">
                        <input type="hidden" name="sender" value="<?php echo $this->session->userdata['id']?>">
                        <input type="hidden" name="receiver" value="<?php echo $message[0]['sender_id']?>">
                        <div style="float: right; margin-top: 10px; ">
                            <button type="submit" class="btn btn-primary">Reply</button>
                        </div>
                    </form>
                    <div style="margin-top: 50px;">
                    <?php for($i=0;$i<count($message);$i++){?>
                        <span><i class="fa fa-user"> </i> <?php echo $message[$i]['sender']?></span> wrote;
                        <div style="float: right;">
                            <?php echo date('M d Y',strtotime($message[$i]['date']));?>
                        </div>
                        <div class="no-more-tables" style="border-radius: 5px; padding: 20px; margin-bottom: 20px; background-color: #f7ecb5; color: #000;">

                                <?php echo $message[$i]['content']?>

                        </div>
                    <?php }?>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
