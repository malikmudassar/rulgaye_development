<div class="container">
    <div class="info-tab">
        <ul class="nav nav-tabs">
            <li><a href="<?php echo base_url().'user/profile'?>">Ads</a></li>
            <li><a href="<?php echo base_url().'user/messages'?>">Messages</a></li>
            <li class="active"><a href="<?php echo base_url().'user/settings'?>">Settings</a></li>
        </ul>
        <div class="tab-content user-ads">
            <div class="tab-pane active" id="ads">
                <div class="setting-info">
                    <div id="accordion" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Personal Details</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <form action="" id="personal_details" method="post" enctype="multipart/form-data" accept-charset="utf-8">
                                <div class="panel-body">
                                    <input type="hidden" name="cmd" value="1">
                                    <div class="col-xs-12 col-lg-4">
                                        <select class="form-control" name="state" id="state" required>
                                            <option value="">Choose State</option>
                                            <?php
                                            for($i=0;$i<count($cities);$i++) {
                                                ?>
                                                <option value="<?php echo $cities[$i]['id'] ?>" <?php if($user['state']==$cities[$i]['id']){echo 'Selected';}?>><?php echo $cities[$i]['name'] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-12 col-lg-4">
                                        <select class="form-control" name="city" id="city" required >
                                            <option value="">Choose City</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-12 col-lg-4">
                                        <label>Full Name</label>
                                        <input name="fullname" type="text" class="form-control input-sm " value="<?php echo $user['fullname']?>" placeholder="Rajev Sinha" aria-controls="example1" required>
                                        <br>
                                        <label>Mobile </label>
                                        <input name="mobile" type="text" class="form-control input-sm " placeholder="913315566887" value="<?php echo $user['mobile']?>" aria-controls="example1" required>
                                        <br>
                                        <label>Image </label>(Max Res 350 x 350)
                                        <?php
                                        if(isset($user['image']))
                                        {
                                            $src=base_url().'img/user/'.$user['image'];
                                        }
                                        else
                                        {
                                            $src=base_url().'img/avatar-2.png';
                                        }
                                        ?>
                                        <img src="<?php echo $src?>" class="img-responsive img-thumbnail"/>
                                        <input type="file" name="userfile" class="form-control"/>
                                        <br>
                                        <label>Website URL </label>
                                        <input name="website" type="text" class="form-control input-sm " placeholder="Website" aria-controls="example1" value="<?php echo $user['website']?>" required>
                                        <label>About me </label>
                                        <textarea name="about" rows="3" class="form-control" required><?php echo $user['about']?></textarea>
                                    </div>

                                    <div class="col-xs-12">
                                        <button id="pd" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Change Password?</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <form action="" id="change_password" method="post">
                                    <input type="hidden" name="cmd" value="2">
                                <div class="panel-body">
                                    <div class="panel-body change-pwd">
                                        <div class="col-xs-12 col-lg-4">
                                            <label>New Password</label>
                                            <input name="password" type="password" class="form-control input-sm " aria-controls="example1" requried>
                                        </div>
                                        <div class="col-xs-12 col-lg-4">
                                            <label>Confirm Password</label>
                                            <input type="password" name="conf_password" class="form-control input-sm " aria-controls="example1" required>
                                        </div>
                                        <div class="col-xs-12">
                                            <button id="pwd" class="btn btn-primary">Update Password</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Social Media Profiles</a></h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <form action="" id="social_media" method="post">
                                    <input type="hidden" name="cmd" value="3">
                                <div class="panel-body">
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-lg-4">
                                            <label>Facebook</label>
                                            <input type="text" class="form-control input-sm " name="facebook" placeholder="Facebook URL">
                                            <br>
                                            <label>LinkedIn</label>
                                            <input type="text" class="form-control input-sm " name="linkedin" placeholder="LinkedIn Profile URL">
                                            <br>
                                            <label>Google+</label>
                                            <input type="text" class="form-control input-sm " name="googleplus" placeholder="Google Plus Profile URL">
                                            <br>
                                            <label>Twitter</label>
                                            <input type="text" class="form-control input-sm " name="twitter" placeholder="Twitter Profile URL">

                                        </div>
                                        <div class="col-xs-12">
                                            <button id="social" class="btn btn-primary">Save</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">Email Notifications</a></h4>
                            </div>
                            <div id="collapsefour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="panel-body change-email">
                                        <div class="col-xs-12">
                                            <input type="checkbox">
                                            <label>Yes, I want to receive newsletter.</label></div>
                                        <div class="col-xs-12">
                                            <input type="checkbox">
                                            <label>Yes, I want to receive email notifications of messages.</label></div>
                                        <div class="col-xs-12">
                                            <input type="checkbox">
                                            <label>Yes,I want to receive email alerts about new listings.</label></div>
                                        <a href="#" class="btn btn-primary">Save</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsefive">Delete Account</a></h4>
                            </div>
                            <div id="collapsefive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="panel-body">
                                        <center>
                                            <a href="#" class="btn btn-primary" >Delete Account</a>
                                        </center>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url().'js/confirm-bootstrap.js'?>"></script>
<script>
    /*$('#myLinkToConfirm').confirmModal();*/
    $('#state').change(function(){
        var id = $('#state').val();
        $.ajax({
            type: "POST",
            url: base_url+"home/getCities",
            data: {id:id},
            success: function(data) {
                $("#city").html(data);
            }
        });
    });

    $('#pd').click(function(){
        $('#personal_detail').submit();
    });
    $('#pwd').click(function(){
        $('#change_password').submit();
    });
    $('#social').click(function(){
        $('#social_media').submit();
    });
</script>