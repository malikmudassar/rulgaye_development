<div class="container">
    <!-- left side -->
    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 list-grid userdetai">
        <?php
        if(isset($user['image']))
        {
            $src=base_url().'img/user/'.$user['image'];
        }
        else
        {
            $src=base_url().'img/avatar-2.png';
        }
        ?>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
            <img src="<?php echo $src?>" class="img-responsive img-thumbnail" width="150px"/>
        </div>
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <h3><?php echo $user['fullname']?></h3>
                <ul class="user_info">
                    <li>
                        <b class="col-xs-3">Website:</b>
                        <p><a href="#"><?php echo $user['website']?></a></p>
                    </li>
                     <li>
                         <b class="col-xs-3">Phone:</b>
                         <p><?php echo $user['mobile']?></p>
                     </li>
                    <li>
                        <b class="col-xs-3">About Me:</b>
                        <p class="col-xs-9 bio">
                            <?php echo $user['about']?>
                        </p>
                    </li>
                    <li class="mobile-info">
                        <b class="col-xs-3">Active Listings:</b>
                        <p><b><?php echo count($user_items)?></b></p>
                    </li>
                </ul>

                <center class="contactuser">
                    <ul>
                        <li class="contact-fb">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="contact-twitter">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="contact-gplus">
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li class="contact-linkedin">
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li class="contact-gplus">
                            <a href="#"><i class="fa fa-envelope"></i></a>
                        </li>
                    </ul>
                </center>
        </div>
        <div class="info-tab">
            <ul class="nav nav-tabs" id="myTab">

                <li class="active"><a data-toggle="tab" href="#classifieds">Active Listings</a></li>


            </ul>
            <div class="tab-content">
                <div id="classifieds" class="tab-pane fade in active">
                    <div class="row">
                        <?php
                        for ($i = 0; $i < count($user_items); $i++) {
                            ?>
                            <div class="col-xs-12 item-listbox">
                                <div class="item-box">
                                    <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">
                                        <?php
                                        $images = explode(',', $user_items[$i]['my_images']);
                                        if($items[$i]['cat_id']==4)
                                        {
                                            $images[0]='avatar_job.png';
                                        }
                                        ?>
                                        <a href="<?php echo base_url() . 'item/itemDetail/' . $user_items[$i]['id'].'/'.implode('-',explode(' ',$user_items[$i]['title'])) ?>">
                                            <img src="<?php echo base_url() . 'img/' . $images[0] ?>"
                                                 class="img-thumbnail img-responsive" width="300px"/>
                                        </a>
                                        <center>
                                            <div class="item-author">
                                                By <span><?php echo $user_items[$i]['fname'] . ' ' . $user_items[$i]['lname'] ?></span>
                                            </div>
                                            <div class="item-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>

                                        </center>

                                    </div>
                                    <div class="col-xs-8 col-sm-7 col-md-7 col-lg-7">
                                        <div class="item_details">
                                            <div class="item-title">
                                                <h5>
                                                    <b><a href="<?php echo base_url() . 'item/itemDetail/' . $user_items[$i]['id'].'/'.implode('-',explode(' ',$user_items[$i]['title'])) ?>"><?php echo $user_items[$i]['title'] ?>
                                                    </b></a></h5>
                                            </div>

                                            <div class="item-cat">
                                                <?php echo $user_items[$i]['category'] ?> - <?php echo $user_items[$i]['sub_category'] ?>
                                            </div>

                                            <div class="item-bio">
                                                <?php echo substr($user_items[$i]['description'], 0, 200); ?>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        <div class="pull-right">
                                            <div class="item-price">
                                                <b><i class="fa fa-inr"></i> <?php echo $user_items[$i]['amount'] ?>.00</b>
                                            </div>
                                            <div class="item-date">
                                                <?php echo date('M, d Y', strtotime($user_items[$i]['date'])) ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        <?php
                        }
                        if($pagination){
                        ?>
                            <div style="text-align: center">
                                <?php echo $pagination;?>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- left side end -->
    <!-- right side -->
    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 userdetai-right">
        <img src="<?php echo base_url().'img/adv.jpg'?>" />
        <?php
        if(count($favorites)>0) {
            if(count($favorites)>5)
            {
                $fav_count=5;
            }
            else
            {
                $fav_count=count($favorites);
            }
            ?>
            <h3 class="person-heading">My Favourites</h3>
            <div class="similar_posts">
                <ul>
                    <?php
                    for ($i = 0; $i < $fav_count; $i++) {
                        $img = explode(',', $favorites[$i]['my_images']);
                        if($favorites[$i]['cat_id']==4)
                        {
                            $img[0]='avatar_job.png';
                        }
                        ?>
                        <li>
                            <img src="<?php echo base_url() . 'img/' . $img[0] ?>"
                                 class="img-responsive img-thumbnail" width="100px">
                            <a href="<?php echo base_url() . 'item/itemDetail/' . $favorites[$i]['id'].'/'.implode('-',explode(' ',$favorites[$i]['title'])) ?>"
                               class="post_title">
                                <?php echo $favorites[$i]['title'] ?>
                            </a>

                            <p><?php echo substr($favorites[$i]['description'], 0, 25) . '...'; ?></p>
                            <b><i class="fa fa-inr"> </i> <?php echo $favorites[$i]['amount'] ?></b>
                        </li>
                    <?php
                    }
                    ?>

                </ul>
            </div>
        <?php
        }else{
        ?>

                <div class="row" style="height: 35px;"></div>
                <div class="side-box">
                    <div >
                        <a href="#"><img style="width: 245px;" src="<?php echo base_url()?>img/indian.jpg" alt="Used Cars for Sale"></a>
                    </div>
                    <div class="row" style="height: 35px;"></div>
                    <div >
                        <a href="#"><img style="width: 245px;" src="<?php echo base_url()?>img/re.jpg" alt="Used Tata Cars for Sale"></a>
                    </div>
                    <div class="row" style="height: 35px;"></div>
                    <div >
                        <a href="#"><img style="width: 245px;" src="<?php echo base_url()?>img/3.jpg" alt="Find Hotels in India"></a>
                    </div>
                    <div class="row" style="height: 35px;"></div>
                    <div >
                        <a href="#"><img style="width: 245px;" src="<?php echo base_url()?>img/4.jpg" alt="Find tourism services in India"></a>
                    </div>
                    <div class="row" style="height: 35px;"></div>
                    <div >
                        <a href="#"><img style="width: 245px;" src="<?php echo base_url()?>img/5.jpg" alt="Best restaurants in India"></a>
                    </div>
                    <div class="row" style="height: 35px;"></div>
                    <div>
                        <a href="#"><img style="width: 245px;" src="<?php echo base_url()?>img/6.jpg" alt="Sell Cars online"></a>
                    </div>
                </div>

        <?php }?>
        <div class="row" style="height: 35px;"></div>
        <h3 >Connect to RulGaye</h3>
        <div class="connectus">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <a href="#">
                        <div class="find-fb">
                            <i class="fa fa-facebook"></i>
                        </div>
                        <span>Find us on facebook</span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <a href="#">
                        <div class="find-gplus">
                            <i class="fa fa-google-plus"></i>
                        </div>
                        <span>Find us on Google+</span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <a href="#">
                        <div class="find-rss">
                            <i class="fa fa-rss"></i>
                        </div>
                        <span>Find us on Rss</span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <a href="#">
                        <div class="find-twitter">
                            <i class="fa fa-twitter"></i>
                        </div>
                        <span>Find us on Twitter</span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <a href="#">
                        <div class="find-youtube">
                            <i class="fa fa-youtube"></i>
                        </div>
                        <span>Find us on Youtube</span>
                    </a>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <a href="#">
                        <div class="find-pin">
                            <i class="fa fa-pinterest"></i>
                        </div>
                        <span>Find us on Pinterest</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--right side end -->
</div>