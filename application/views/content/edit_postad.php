<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/8/2015
 * Time: 2:20 PM
 */
?>
<script src="<?php echo base_url()?>js/home.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#mytextarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: false,
        height : 200,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });
</script>
<div class="wrapper">
    <div class="container">
        <!-- post adform -->
        <div class="postad_wrapper">
            <h3>Edit Your Ad Details</h3>

            <?php
            if(isset($errors)){
                ?>
                <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong> <?php echo ($errors);?>
                </div>
            <?php }?>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            Edit Item Details <span class="pull-right"></span>
                        </h4>
                    </div>
                    <div class="panel-collapse">
                        <?php echo form_open_multipart('postad/index');?>
                        <div class="panel-body">
                            <div class="user-detail">
                                <div class="main-cat col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label class="control-label">Main Category</label>
                                    <select class="form-control" id="searchCategory" name="category">
                                        <option value="">Select Main Category</option>
                                        <?php
                                        for($i=0;$i<count($menu_cats);$i++) {
                                            ?>
                                            <option value="<?php echo $menu_cats[$i]['id']?>" <?php if($menu_cats[$i]['id']==$item['category']){echo 'selected';}?>><?php echo $menu_cats[$i]['name']?></option>

                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="main-cat col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label class="control-label">Sub Category</label>
                                    <select class="form-control" id="sub_category" name="sub_category">
                                        <option value="">Select Sub Category</option>
                                    </select>
                                </div>
                                <div class="Ad-title col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label class="control-label">Title</label>
                                    <input type="text" class="form-control" name="title" value="<?php echo $item['title']?>"/>
                                </div>
                                <div class="price-info col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <label class="control-label">Amount </label> <i class="fa fa-inr"></i>
                                    <input type="text" class="form-control" name="amount" placeholder='Rs. 10,000 /-' value="<?php echo $item['amount']?>" />
                                </div>
                                <div class="classified-desc col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label class="control-label">Description</label>
                                    <div class="desc-box" style="border: 1px solid #eee;min-height: 100px;">
                                        <textarea name="description" id="mytextarea" placeholder="Paste your description here"><?php echo $item['description']?>

                                        </textarea>
                                    </div>
                                </div>
                                <h3>Location</h3>
                                <div class="location-map col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                    <div class="country-map">
                                        <label class="control-label">Country</label>

                                        <input name="country" type="text" value="India" disabled class="form-control">

                                    </div>
                                    <div class="country-map">
                                        <label class="control-label">State</label>
                                        <select class="form-control" name="state" id="state">
                                            <option value="">Select State</option>
                                            <?php
                                            for($i=0;$i<count($cities);$i++) {
                                                ?>

                                                <option value="<?php echo $cities[$i]['id'] ?>" <?php if($cities[$i]['id']==$item['state']){echo 'selected';}?>><?php echo $cities[$i]['name'] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="country-map">
                                        <label class="control-label">City</label>
                                        <select class="form-control" name="city" id="city">
                                            <option value="">Select City</option>
                                        </select>
                                    </div>
                                    <div class="loc-map">
                                        <label class="control-label">Location</label>
                                        <input type="text" class="form-control" name="location" value="<?php echo $item['location']?>"/>
                                    </div>
                                    <div class="googlemap">

                                    </div>
                                </div>

                                <h3>Seller Contact Information</h3>
                                <div class="seller-info col-xs-12">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <label class="control-label">Contact Phone</label>
                                        <input type="text" name="contact_phone" class="form-control" placeholder="+9133355445568" value="<?php echo $item['contact_phone']?>"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <label class="control-label">Contact Name</label>
                                        <input type="text" name="contact_name" class="form-control" placeholder="Radhay Kishan" value="<?php echo $item['contact_name']?>"/>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <label class="control-label">Contact Email</label>
                                        <input type="text" name="contact_email" class="form-control" placeholder="radhay@domain.com" value="<?php echo $item['contact_email']?>"/>
                                    </div>
                                </div>
                                <h3>Upload Item Images</h3>
                                <div class="add-image col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class='upload-img' style="border: 1px solid #eee;min-height: 100px;">
                                        <div id="formdiv" class="form_t">

                                            Only JPEG,PNG,JPG Type Images Allowed. Image Size Should Be Less Than 100KB.
                                            <div id="filediv"><input name="file[]" type="file" id="file" /></div>
                                            <input type="button" id="add_more" id="upload" class="upload" value="Add More Files"/>

                                        </div>
                                    </div>
                                </div>
                                <div class="check_list col-xs-12 col-sm-4 col-md-4 col-lg-4">

                                    <div class="accept-term">
                                        <input type="checkbox" name="terms" value="1"><span>Accept terms and conditions.</span>
                                    </div>
                                </div>
                                <div class="btn-action">
                                    <button class="btn btn-cont" type="submit">Submit</button>
                                    <a href="<?php echo base_url().'user/profile'?>" class="btn btn-default">Cancel</a>
                                    <!--<a href="#" class="btn btn-preview" type="submit">Preview</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="popular_ads">
            <div class="ads-heading">
                <h3>
                    Popular Classified


                </h3>
            </div>
            <div class="popular-body">

                <div class="container">
                    <div class="col-xs-12" >
                        <div class="carousel slide" id="myCarousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <ul class="thumbnails">
                                        <?php
                                        if(count($items)>12)
                                        {
                                            $count_items=12;
                                        }
                                        else
                                        {
                                            $count_items=count($items);
                                        }
                                        ?>
                                        <?php for($i=0;$i<$count_items;$i++){?>
                                            <?php
                                            $images=explode(',',$items[$i]['my_images']);

                                            ?>

                                            <li class="col-sm-2">
                                                <div class="fff">

                                                    <div class="thumbnail">

                                                        <a href="<?php echo base_url().'item/itemDetail/'.$items[$i]['id'];?>"><img style="height:175px" src="<?php echo base_url().'img/'.$images[0]?>" alt=""></a>
                                                    </div>
                                                    <div class="caption">
                                                        <p><a href="<?php echo base_url().'item/itemDetail/'.$items[$i]['id'];?>"><?php echo $items[$i]['title']?></a></p>
                                                        <b><i class="fa fa-inr"> </i> <?php echo $items[$i]['amount']?></b>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php }?>


                                    </ul>
                                </div><!-- /Slide1 -->
                                <!-- /Slide2 -->
                            </div>
                            <!-- /.control-box -->
                        </div><!-- /#myCarousel -->
                    </div><!-- /.col-xs-12 -->
                </div><!-- /.container -->
            </div>
        </div>
    </div>


    <!--end-->
</div>
<script>
    $('#searchCategory').change(function(){
        var id = $('#searchCategory').val();
        $.ajax({
            type: "POST",
            url: base_url+"home/getSubCategory",
            data: {id:id},
            success: function(data) {
                $("#sub_category").html(data);
            }
        });
    });

    $('#state').change(function(){
        var id = $('#state').val();
        $.ajax({
            type: "POST",
            url: base_url+"home/getCities",
            data: {id:id},
            success: function(data) {
                $("#city").html(data);
            }
        });
    });



    var abc = 0;      // Declaring and defining global increment variable.
    $(document).ready(function() {
//  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
        $('#add_more').click(function() {
            $(this).before($("<div/>", {
                id: 'filediv'
            }).fadeIn('slow').append($("<input/>", {
                name: 'file[]',
                type: 'file',
                id: 'file'
            }), $("<br/><br/>")));
        });
// Following function will executes on change event of file input to select different file.
        $('body').on('change', '#file', function() {
            if (this.files && this.files[0]) {
                abc += 1; // Incrementing global variable by 1.
                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='abcd" + abc + "' class='abcd'><img id='previewimg" + abc + "' src=''/></div>");
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
                $(this).hide();
                $("#abcd" + abc).append($("<img/>", {
                    id: 'img',
                    src: 'x.png',
                    alt: 'delete'
                }).click(function() {
                    $(this).parent().parent().remove();
                }));
            }
        });
// To Preview Image
        function imageIsLoaded(e) {
            $('#previewimg' + abc).attr('src', e.target.result);
        };
        $('#upload').click(function(e) {
            var name = $(":file").val();
            if (!name) {
                alert("First Image Must Be Selected");
                e.preventDefault();
            }
        });
    });


</script>