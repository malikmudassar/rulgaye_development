<div class="container">
    <div class="search-main">
        <form action="<?php echo base_url().'search'?>" method="get">
            <div class="search_container">
                <h4>What are you looking for?</h4>
                <div class="container-1">
                    <input type="search" id="search" placeholder="E.g Jobs, Cars etc.." name="term" />
                    <button type="submit" class="icon"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="postbtn">
<center>
        <a href="<?php echo base_url()?>postad" class="btn btn-post">Post an Ad</a>
</center>
</div>
<div class="container" style="margin-top: 80px; ">
    <?php
    if(isset($_REQUEST['pa'])=='true'){
        ?>
        <div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Congratulations!</strong> <?php echo ('Your Ad has been Posted, It will published as soon as it is Approved.');?>
        </div>
    <?php }?>
</div>
<div class="container">
    <!-- popular ads-->
    <div class="popular_ads">
        <div class="ads-heading">
            <h1 class="landing_h1">
                MOST VIEWED PRODUCTS
            </h1>
        </div>
        <div class="popular-body">

            <div class="container">
                <div class="col-xs-12" >
                                <div class="thumbnails">
                                    <?php
                                    if(count($items)>12)
                                    {
                                        $count_items=12;
                                    }
                                    else
                                    {
                                        $count_items=count($items);
                                    }
                                    ?>
                                    <?php for($i=0;$i<$count_items;$i++){?>
                                        <?php
                                        $images=explode(',',$items[$i]['my_images']);
                                        if(empty($images[0]))
                                        {
                                            if($items[$i]['category']==4)
                                            {
                                                $images[0]='avatar_job.png';
                                            }
                                            elseif($items[$i]['category']==3)
                                            {
                                                $images[0]='avatar_job.png';
                                            }
                                            elseif($items[$i]['category']==2)
                                            {
                                                $images[0]='avatar_motor.png';
                                            }
                                        }
                                        //print_r($images);
                                        ?>

                                    <div class="col-xs-12 col-sm-2">
                                        <div class="fff">

                                            <div class="thumbnail">

                                                <a href="<?php echo base_url().'item/itemDetail/'.$items[$i]['id'].'/'.implode('-',explode(' ',$items[$i]['title']));?>"><img style="height:175px" src="<?php echo base_url().'img/'.$images[0]?>" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p><a href="<?php echo base_url().'item/itemDetail/'.$items[$i]['id'].'/'.implode('-',explode(' ',$items[$i]['title']));?>"><?php echo substr($items[$i]['title'],0,15)?></a></p>
                                                <b><i class="fa fa-inr"> </i> <?php echo $items[$i]['amount']?></b>
                                            </div>
                                        </div>
                                    </div>
                                    <?php }?>

                    </div><!-- /#myCarousel -->
                </div><!-- /.col-xs-12 -->
            </div><!-- /.container -->
        </div>
    </div>
    <!-- popular ads end-->
    <!-- category start-->
    <div class="home-category">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
            <h3 class="landing_h1">BUY/SELL PRODUCTS</h3>
            <div class="row">
                <?php
                for($j=0;$j<count($categories[1]['sub']);$j++) {
                    ?>
                    <div class="col-xs-5 col-sm-2 col-md-2 col-lg-2 category-bg">
                            <i class="fa <?php echo $categories[1]['sub'][$j]['icon']?>"></i>
                            <h5><a href="<?php echo base_url().'item/catListing/'.$categories[1]['sub'][$j]['id']?>"><?php echo $categories[1]['sub'][$j]['name']?></a></h5>
                    </div>
                <?php
                }
                ?>


            </div>
            <div class="row"></div>
            <h3>BUY/SELL AUTOMOBILES</h3>
            <div class="row">
                <?php
                for($j=0;$j<count($categories[0]['sub'])-1;$j++) {
                    ?>
                    <div class="col-xs-5 col-sm-2 col-md-2 col-lg-2 category-bg">
                            <i class="fa <?php echo $categories[0]['sub'][$j]['icon']?>"></i>
                            <h5><a href="<?php echo base_url().'item/catListing/'.$categories[0]['sub'][$j]['id']?>"><?php echo $categories[0]['sub'][$j]['name']?></a></h5>
                    </div>
                <?php
                }
                ?>
            </div>
            <h3>BUY/SELL PROPERTIES</h3>
            <div class="row">
                <?php
                for($j=0;$j<count($categories[2]['sub']);$j++) {
                    ?>
                    <div class="col-xs-5 col-sm-2 col-md-2 col-lg-2 category-bg">
                            <i class="fa <?php echo $categories[2]['sub'][$j]['icon']?>"></i>
                            <h5><a href="<?php echo base_url().'item/catListing/'.$categories[2]['sub'][$j]['id']?>"><?php echo $categories[2]['sub'][$j]['name']?></a></h5>
                    </div>
                <?php
                }
                ?>
            </div>
            <h3>LATEST JOBS</h3>
            <div class="row">
                <?php
                for($j=0;$j<count($categories[3]['sub']);$j++) {
                    ?>
                    <div class="col-xs-5 col-sm-2 col-md-2 col-lg-2 category-bg">
                            <i class="fa <?php echo $categories[3]['sub'][1]['icon']?>"></i>
                            <h5>
                                <a href="<?php echo base_url().'item/catListing/'.$categories[3]['sub'][$j]['id']?>">
                                    <?php echo $categories[3]['sub'][$j]['name']?>
                                </a>
                            </h5>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2 col-sm-offset-1 col-md-2 col-md-offset-1 col-lg-2 col-lg-offset-1">
            <div class="row" style="height: 35px;"></div>
            <div class="side-box">
                <div class="thumbnail">
                    <a href="<?php echo base_url().'item/catListing/10'?>">
                        <img src="<?php echo base_url()?>img/iphone3.jpg" alt="Buy/Sell Used Mobiles">
                    </a>
                </div>
                <div class="thumbnail">
                    <a href="#"><img src="<?php echo base_url()?>img/1.jpg" alt="Used Cars for Sale"></a>
                </div>
                <div class="thumbnail">
                    <a href="#"><img src="<?php echo base_url()?>img/2.jpg" alt="Used Tata Cars for Sale"></a>
                </div>
                <div class="thumbnail">
                    <a href="#"><img src="<?php echo base_url()?>img/3.jpg" alt="Find Hotels in India"></a>
                </div>
                <div class="thumbnail">
                    <a href="#"><img src="<?php echo base_url()?>img/4.jpg" alt="Find tourism services in India"></a>
                </div>
                <div class="thumbnail">
                    <a href="#"><img src="<?php echo base_url()?>img/5.jpg" alt="Best restaurants in India"></a>
                </div>
                <div class="thumbnail">
                    <a href="#"><img src="<?php echo base_url()?>img/6.jpg" alt="Sell Cars online"></a>
                </div>
            </div>
        </div>
    </div>
    <!--end-->
</div>
<script>
    //$(document).ready(function(){
        //var idval = $('#searchCategory').val();
        //alert (idval);
        $('#searchCategory').change(function(){
            var id = $('#searchCategory').val();
            $.ajax({
                type: "POST",
                url: base_url+"home/getSubCategory",
                data: {id:id},
                success: function(data) {
                    $("#sub_category").html(data);
                }
            });
        });

    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
            return false;
        return true;
    }


    //});
</script>