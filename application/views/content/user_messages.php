<div class="container">
    <div class="info-tab">
        <ul class="nav nav-tabs">
            <li ><a href="<?php echo base_url().'user/profile'?>">Ads</a></li>
            <li class="active"><a href="<?php echo base_url().'user/messages'?>">Messages</a></li>
            <li><a href="<?php echo base_url().'user/settings'?>">Settings</a></li>
        </ul>
        <div class="tab-content user-ads">
            <div class="tab-pane active" id="ads">
                <div class="tabbable-panel">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#inbox" data-toggle="tab">
                                    Inbox </a>
                            </li>
                            <li>
                                <a href="#trash" data-toggle="tab">
                                    Trash </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="inbox">
                                <h4>Messages</h4>

                                <div id="no-more-tables">
                                    <?php
                                    if(count($messages)>0) {
                                        ?>
                                        <table class="col-md-12 table-bordered table-striped table-condensed cf">
                                            <thead class="cf">

                                            <tr>
                                                <th>Sender</th>
                                                <th>Ad Title</th>
                                                <th>Message Subject</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php for ($i = 0; $i < count($messages); $i++) { ?>
                                                <tr>

                                                    <td data-title="Ad Title">
                                                        <p><?php echo $messages[$i]['sender'] ?></p>
                                                    </td>
                                                    <td data-title="Ad Title">
                                                        <?php echo $messages[$i]['title'] ?>
                                                    </td>
                                                    <td data-title="Message" class="numeric">
                                                        <p>
                                                            <a href="<?php echo base_url() . 'user/message_detail/' . $messages[$i]['id'] ?>"> <?php echo $messages[$i]['subject'] ?></a>
                                                        </p>
                                                    </td>
                                                    <td data-title="Date"><?php echo date('M d, Y', strtotime($messages[$i]['date'])) ?></td>
                                                    <td data-title="Action" class="numeric">
                                                        <i class="fa fa-star icn"></i>
                                                        <i class="fa fa-trash icn"></i>
                                                    </td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                        <div class="alert alert-info">
                                            <p>There are no messages in your inbox right now.</p>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="tab-pane" id="trash">
                                <h4>Deleted Messages</h4>
                                <div id="no-more-tables">
                                    <table class="col-md-12 table-bordered table-striped table-condensed cf">
                                        <thead class="cf">
                                        <tr>
                                            <th>User</th>
                                            <th class="numeric">Message</th>
                                            <th>Date</th>
                                            <th class="numeric">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td data-title="Ad Title">
                                                <p>John (2)</p>
                                            </td>
                                            <td data-title="Message" class="numeric">
                                                <p>Fingerprint Time and Attendance Mac...</p>
                                            </td>
                                            <td data-title="Date">12 Septmeber</td>
                                            <td data-title="Action" class="numeric">
                                                <i class="fa fa-star icn"></i>
                                                <i class="fa fa-trash icn"></i>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url().'js/confirm-bootstrap.js'?>"></script>
<script>
    /*$('#myLinkToConfirm').confirmModal();*/
</script>