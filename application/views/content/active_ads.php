<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Active ads</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/all-krajee.css" rel="stylesheet">
    <link href="css/fileinput.min.css" rel="stylesheet">
  </head>
  <body>
  	<!--Active & inactive section -->
    <div class="container">
    	<div clsss="row">
        	<h2>Your Rulgaye Ads</h2>
            <p >You can manage your Active and Inactive Ads here</p>
            <br><br>
        </div>
    </div>
    <div class="container">
            <div class="info-tab">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#ads">Ads</a></li>
                <li><a data-toggle="tab" href="#message">message</a></li>
                <li><a data-toggle="tab" href="#settings">settings</a></li>
              </ul>
              <div class="tab-content user-ads">
              <div class="tab-pane active" id="ads">
              <div class="tabbable-panel">
                <div class="tabbable-line">
                    <div class="col-lg-3 col-md-3 col-sm-3 search_table">
                        <label>
                           <input type="search" class="form-control input-sm " placeholder="Ad Title..." aria-controls="example1">
                           <i class="fa fa-search seach_icon"></i>
                        </label>
                    </div>
                        <ul class="nav nav-tabs ">
                                <li class="active">
                                        <a href="#active" data-toggle="tab">
                                        Active ads </a>
                                </li>

                                <li>
                                        <a href="#inactive" data-toggle="tab">
                                        Inactive ads </a>
                                </li>

                        </ul>
                    
            <div class="tab-content">
                <div class="tab-pane active" id="active">
                <center>
                    <div class="thumb-box">
                        <i class="fa fa-thumb-tack cssAnimation"></i>
                    </div>
                        <h4> You Dont have active Ads. Place an ad now!</h4>	
                        <a href="#" class=" btn btn-success"><i class="fa fa-plus"></i> Submit an Ad!</a>
                </center>
                </div>
                <div class="tab-pane inactive-box" id="inactive">
                    <div id="no-more-tables">
                    <table class="col-md-12 table-bordered table-striped table-condensed cf">
        		<thead class="cf">
        			<tr>
        				<th>Date</th>
        				<th>Ad Title</th>
        				<th class="numeric">Price</th>
        				<th class="numeric">Message</th>
        				<th class="numeric">Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<tr>
        				<td data-title="Date">12 Septmeber</td>
                                        <td data-title="Ad Title">
                                            <p>Fingerprint time and Attendence Machine...</p>
                                            <i class="fa fa-print icn"></i><a href="#">Preview</a> 
                                            <i class="fa fa-edit icn"></i><a href="#">Edit</a>
                                            <i class="fa fa-trash icn"></i>
                                        </td>
        				<td data-title="Price" class="numeric">$1.38</td>
                                        <td data-title="Message" class="numeric">
                                            <label> <i class="fa fa-envelope "></i>&nbsp; 10</label>
                                        </td>
                                        <td data-title="Action" class="numeric">
                                        <button type="button" class="btn btn-success"><i class="fa fa-arrow-up"></i> Active Ad</button>
                                        </td>
        			</tr>
                                <tr>
        				<td data-title="Date">12 Septmeber</td>
                                        <td data-title="Ad Title">
                                            <p>Fingerprint time and Attendence Machine...</p>
                                            <i class="fa fa-print icn"></i><a href="#">Preview</a> 
                                            <i class="fa fa-edit icn"></i><a href="#">Edit</a>
                                            <i class="fa fa-trash icn"></i>
                                            <a href="#" id="myLinkToConfirm" class="confirModal" data-confirm-title="Confirmation" data-confirm-message="Are you sure you want to perform this action ?">Delete</a>
                                        </td>
        				<td data-title="Price" class="numeric">$1.38</td>
                                        <td data-title="Message" class="numeric">
                                            <label> <i class="fa fa-envelope "></i>&nbsp; 10</label>
                                        </td>
                                        <td data-title="Action" class="numeric">
                                        <button type="button" class="btn btn-success"><i class="fa fa-arrow-up"></i> Active Ad</button>
                                        </td>
        			</tr>
        		</tbody>
        	</table>
        </div>
              </div> 
            </div>
               </div>

              </div>
              </div>
              <div id="message" class="tab-pane fade">
                  <div class="tabbable-panel">
				<div class="tabbable-line">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#inbox" data-toggle="tab">
							Inbox </a>
						</li>
						<li>
							<a href="#sent" data-toggle="tab">
							Sent </a>
						</li>
						<li>
							<a href="#Archive" data-toggle="tab">
							Archive </a>
						</li>
					</ul>
                                    
            <div class="tab-content">
                <div class="tab-pane active" id="inbox">
                    <h4>Inbox Messages</h4>
                            <div class="col-xs-12 header-search">
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <p>Show Incoming messages for:</p>
                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                    <select class="form-control" placeholder="All Recieve messages">
                                        <option>Disabled select</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                    <p>Show:</p>
                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                    <input type="search" class="form-control input-sm " placeholder="Name" aria-controls="example1" style="float:right; margin-left:10px">
                                    </div>
                                    </div>
                            </div>
                    <div id="no-more-tables">
                    <table class="col-md-12 table-bordered table-striped table-condensed cf">
        		<thead class="cf">
        			<tr>
        				<th>Date</th>
        				<th>User</th>
        				<th class="numeric">Message</th>
                                        <th class="numeric">Price</th>
        				<th class="numeric">Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<tr>
        				<td data-title="Date">12 Septmeber</td>
                                        <td data-title="Ad Title">
                                            <p>John (2)</p>
                                        </td>
                                        <td data-title="Message" class="numeric">
                                            <p>Fingerprint Time and Attendance Mac...</p>
                                        </td>
                                        <td data-title="Price" class="numeric">$1.38</td>
                                        <td data-title="Action" class="numeric">
                                            <i class="fa fa-star icn"></i>
                                            <i class="fa fa-trash icn"></i>
                                        </td>
        			</tr>
                                <tr>
        				<td data-title="Date">12 Septmeber</td>
                                        <td data-title="Ad Title">
                                            <p>John (2)</p>
                                        </td>
                                        <td data-title="Message" class="numeric">
                                            <p>Fingerprint Time and Attendance Mac...</p>
                                        </td>
                                        <td data-title="Price" class="numeric">$1.38</td>
                                        <td data-title="Action" class="numeric">
                                            <i class="fa fa-star icn"></i>
                                            <i class="fa fa-trash icn"></i>
                                        </td>
        			</tr>
        		</tbody>
        	</table>
        </div>
                </div>
            <div class="tab-pane" id="sent">
                <h4>Sent Messages</h4>
            <div id="no-more-tables">
                    <table class="col-md-12 table-bordered table-striped table-condensed cf">
        		<thead class="cf">
        			<tr>
        				<th>Date</th>
        				<th>User</th>
        				<th class="numeric">Message</th>
                                        <th class="numeric">Price</th>
        				<th class="numeric">Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<tr>
        				<td data-title="Date">12 Septmeber</td>
                                        <td data-title="Ad Title">
                                            <p>John (2)</p>
                                        </td>
                                        <td data-title="Message" class="numeric">
                                            <p>Fingerprint Time and Attendance Mac...</p>
                                        </td>
                                        <td data-title="Price" class="numeric">$1.38</td>
                                        <td data-title="Action" class="numeric">
                                            <i class="fa fa-star icn"></i>
                                            <i class="fa fa-trash icn"></i>
                                        </td>
        			</tr>
                                <tr>
        				<td data-title="Date">12 Septmeber</td>
                                        <td data-title="Ad Title">
                                            <p>John (2)</p>
                                        </td>
                                        <td data-title="Message" class="numeric">
                                            <p>Fingerprint Time and Attendance Mac...</p>
                                        </td>
                                        <td data-title="Price" class="numeric">$1.38</td>
                                        <td data-title="Action" class="numeric">
                                            <i class="fa fa-star icn"></i>
                                            <i class="fa fa-trash icn"></i>
                                        </td>
        			</tr>
        		</tbody>
        	</table>
        </div>
            </div>
            <div class="tab-pane" id="Archive">
                    <center>
                        <i class="fa fa-envelope"></i>
                        <h4> No Messages!</h4>	
                    </center>                    
            </div>
					</div>
				</div>
			</div>

              </div>
              <div id="settings" class="tab-pane fade">
                  <div class="setting-info">
                        <div id="accordion" class="panel-group">
                            <div class="panel panel-default">
                                 <div class="panel-heading">
                                    <h4 class="panel-title">
                                         <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Change Contact Details</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                     <div class="panel-body">
                                        <div class="col-xs-12 col-lg-4">
                                            <select class="form-control" >
                                                 <option>Choose State</option>
                                            </select>
                                        </div>
                                     </div>
                                     <div class="panel-body">
                                         <div class="col-xs-12 col-lg-4">
                                         <label>Name</label>
                                          <input type="search" class="form-control input-sm " placeholder="Name" aria-controls="example1">
                                          </div>
                                         <div class="col-xs-12 col-lg-4">
                                          <label>Phone</label>
                                          <input type="search" class="form-control input-sm " placeholder="Phone" aria-controls="example1">
                                         </div>
                                         <div class="checkbox-area">
                                          <div class="col-xs-12">
                                            <input type="checkbox"><label>Do not use this data when adding Ads</label>
                                          </div>
                                          <div class="col-xs-12">
                                            <input type="checkbox"><label>Hide the link to 'All my Ads' </label>
                                          </div>
                                         </div>
                                         <div class="col-xs-12">                       
                                          <a href="#" class="btn btn-primary">Save</a>
                                          </div>
    
                                     </div>
                                </div>
                         </div>
                            <div class="panel panel-default">
                                 <div class="panel-heading">
                                    <h4 class="panel-title">
                                         <a data-toggle="collapse" data-parent="#accordion" href="#collapsesix">Upload Photo</a>
                                    </h4>
                                </div>
                                <div id="collapsesix" class="panel-collapse collapse">
                                     <div class="panel-body">
                                         <div id="kv-avatar-errors" class="center-block" style="width:800px;display:none"></div>
                                            <form class="text-center" action="#" method="post" enctype="multipart/form-data">
                                                <div class="kv-avatar center-block" style="width:180px">
                                                    <div class="file-input"><div class="file-preview ">
                                                        <div class="file-drop-disabled">
                                                        <div class="file-preview-thumbnails">
                                                            <div class="file-default-preview"><img src="img/default_avatar_male.jpg" alt="Your Avatar" style="width:160px">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>    
                                                        <div class="file-preview-status text-center text-success"></div>
                                                        <div class="kv-fileinput-error"></div>
                                                        </div>
                                                    </div> 
                                                    <button type="button" tabindex="500" title="Cancel or reset changes" class="btn btn-default fileinput-remove fileinput-remove-button"><i class="glyphicon glyphicon-remove"></i></button> 
                                                    <div tabindex="500" class="btn btn-primary btn-file"><i class="glyphicon glyphicon-folder-open"></i>
                                                        <input id="avatar" name="avatar" type="file" class=""></div>
                                                    </div>
                                                    </div>
                                               
                                                <!-- include other inputs if needed and include a form submit (save) button -->
                                            </form>
                                     </div>
                                    
                                </div>
                         </div>
                         <div class="panel panel-default">
                             <div class="panel-heading">
                                  <h4 class="panel-title">
                                     <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Change Password?</a>
                                  </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                  <div class="panel-body">
                                         <div class="panel-body change-pwd">
                                         <div class="col-xs-12 col-lg-4">
                                          <label> New password</label>
                                          <input type="password" class="form-control input-sm " aria-controls="example1">
                                         </div>
                                           <div class="col-xs-12 col-lg-4">
                                          <label>Repeat new password</label>
                                          <input type="password" class="form-control input-sm " aria-controls="example1"> 
                                             </div>
                                             <div class="col-xs-12">
                                            <a href="#" class="btn btn-primary">Change Password</a>
                                             </div>
                                        </div>
                                     </div>
                                 </div>
                             </div>
                       <div class="panel panel-default">
                             <div class="panel-heading">
                                    <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Change Email</a></h4>
                             </div>
                     <div id="collapseThree" class="panel-collapse collapse">
                             <div class="panel-body">
                                 <div class="panel-body">
                                        <div class="col-xs-12 col-lg-4">
                                            <label>New Email</label>
                                          <input type="password" class="form-control input-sm " aria-controls="example1" placeholder="abc@example.com">
                                          </div>
                                     <div class="col-xs-12">
                                            <a href="#" class="btn btn-primary">Save</a>
                                     </div>
                                        
                               </div>
                             </div>
                     </div>
                     
                 </div>
                 <div class="panel panel-default">
                             <div class="panel-heading">
                                    <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">Email Notifications</a></h4>
                             </div>
                   <div id="collapsefour" class="panel-collapse collapse">
                             <div class="panel-body">
                                 <div class="panel-body change-email">
                                            <div class="col-xs-12">
                                            <input type="checkbox"> 
                                            <label>Yes, I want to recieve newsletter.</label></div>
                                            <div class="col-xs-12">
                                            <input type="checkbox"> <label>Yes,
                                            I want to recieve email notifications of messages.</label></div>
                                            <div class="col-xs-12">
                                            <input type="checkbox"><label> Yes,
                                            I want to recieve email alerts about new listings.</label></div>
                                            <a href="#" class="btn btn-primary">Save</a>
                                         </div>
                             </div>
                     </div>
                     
                 </div>
                 <div class="panel panel-default">
                             <div class="panel-heading">
                                    <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapsefive">Delete Account</a></h4>
                             </div>
                   <div id="collapsefive" class="panel-collapse collapse">
                             <div class="panel-body">
                                 <div class="panel-body">
                                     <center>
                                          <a href="#" class="btn btn-primary" >Delete Account</a>    
                                     </center>
                                     </div>
                             </div>
                     </div>
                     
                 </div>
            </div>
                    </div>
              </div>
              </div>                
            </div>
    </div>  
    <!--Ends Active & inactive section -->
       
    <!-- footer section -->
  <footer class="footer">
      
      <div class="footer_container">
          <div class="container">
              <div class="footer_menu">
              <ul>
                  <li><a href="#">
                      Home
                      </a>
                  </li>
                  <li><a href="#">
                      Home
                      </a>
                  </li>
                  <li><a href="#">
                      Home
                      </a>
                  </li>
              </ul>
              </div>
              <div class="social_media">
                  <ul>
                      <li>
                          <a href="#"><i class="fa fa-google"></i></a>
                      </li>
                      <li>
                          <a href="#"><i class="fa fa-twitter"></i></a>
                      </li>
                      <li>
                          <a href="#"><i class="fa fa-facebook"></i></a>
                      </li>
                  </ul>
              </div>
              <div class="divider">
                  <hr>
              </div>
              <div class="copy_right">
                  <p>Copyright &copy; 2015 Rulgaye.com. All rights Reserved.</p>
              </div>
          </div>
      </div>
  </footer>
    <!-- footer end-->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/confirm-bootstrap.js"></script>
    <script>
        $('#myLinkToConfirm').confirmModal();
var btnCust = '<button type="button" class="btn btn-default" title="Add picture tags" ' + 
    'onclick="alert(\'Call your custom code here.\')">' +
    '<i class="glyphicon glyphicon-tag"></i>' +
    '</button>'; 
$("#avatar").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="/uploads/default_avatar_male.jpg" alt="Your Avatar" style="width:160px">',
    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
    allowedFileExtensions: ["jpg", "png", "gif"]
});
        </script>
  </body>
</html>
