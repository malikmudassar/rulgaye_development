<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/10/2015
 * Time: 10:13 AM
 */
if(count($reviews)>0)
{
    $sum=0;
    for($i=0;$i<count($reviews);$i++){
        $sum=$sum+$reviews[$i]['rating'];
    }
    $avg_rating=intval($sum/count($reviews));
}
?>
<!--<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmnLxiLgqYdGRKA_u5TWoR85rvIzOz_Io&callback=initMap">
</script>-->
<script type="text/javascript" src="<?php echo base_url()?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#mytextarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: false,
        height : 200,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=698675913599902";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="container">
    <!-- Ad's detail-->
    <div class="ads-detail">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-9">
            <div class="Ads-title">
                <h3><?php echo $item['title']?></h3>
                <div class="prod_meta">

                    <span class="meta_id"><?php echo $item['page_views']?> Views</span>
                    <span><?php echo count($reviews)?> Reviews </span>
                          <span class="rating">
                              <div class="rating-stars">
                                  <input type="radio" <?php if($avg_rating==5){echo 'checked';}?> name="group-2" id="group-2-0" value="5" />
                                  <label for="group-2-0"></label>
                                  <input type="radio" <?php if($avg_rating==4){echo 'checked';}?> name="group-2" id="group-2-1" value="4" />
                                  <label for="group-2-1"></label>
                                  <input type="radio" <?php if($avg_rating==3){echo 'checked';}?> name="group-2" id="group-2-2" value="3" />
                                  <label for="group-2-2"></label>
                                  <input type="radio" <?php if($avg_rating==2){echo 'checked';}?> name="group-2" id="group-2-3" value="2" />
                                  <label for="group-2-3"></label>
                                  <input type="radio" <?php if($avg_rating==1){echo 'checked';}?> name="group-2" id="group-2-4"  value="1" />
                                  <label for="group-2-4"></label>
                              </div>

                </div>
            </div>

            <div class="pricebox">
                <h4 class="pricetag"><i class="fa fa-inr"></i> <?php echo $item['amount']?>.00</h4>
            </div>
            <div class="row"></div>
            <?php if($item['category']!=4) { ?>
                <div class="gallery-slider col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div id="slider">
                        <!-- Top part of the slider -->
                        <?php
                        $images = explode(',', $item['my_images']);
                        ?>
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
                                <!-- Indicators -->


                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" id="mycoursal">

                                    <?php
                                    for($i=0;$i<count($images);$i++) {
                                        ?>
                                        <div class="item <?php if($i==0){echo 'active';}?>">
                                            <img src="<?php echo base_url().'img/'.$images[$i];?>" alt="1.jpg"
                                                 class="img-responsive">

                                        </div>
                                    <?php
                                    }
                                    ?>

                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>

                                <!-- Thumbnails -->
                                <ul class="hide-bullets col-xs-12">
                                    <?php
                                    for($i=0;$i<count($images);$i++) {
                                        ?>
                                        <li class="col-xs-4 col-sm-4">
                                            <a class="thumbnail" id="carousel-selector-<?php echo $i;?>">
                                                <img src="<?php echo base_url().'img/'.$images[$i]?>">
                                            </a>
                                        </li>
                                    <?php
                                    }
                                    ?>

                                </ul>
                            </div>

                    </div>



                </div>
            <?php
            }
            ?>
            

            <div class="Ads-info col-xs-12 col-sm-7 col-md-12 col-lg-6">
                <div class="row">
                          <span>
                              <b class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Location:</b>
                              <p class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><?php echo $item['location']?></p>
                          </span>
                          <span>
                              <b class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Country:</b>
                              <p class="col-xs-6 col-sm-6 col-md-6 col-lg-6">India</p>
                          </span>
                          <span>
                              <b class="col-xs-3 col-sm-3 col-md-3 col-lg-3">State:</b>
                              <p class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><?php echo $item['state_name']?></p>
                          </span>
                          <span>
                              <b class="col-xs-3 col-sm-3 col-md-3 col-lg-3">City:</b>
                              <p class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><?php echo $item['city_name']?></p>
                          </span>
                          <span>
                              <b class="col-xs-3 col-sm-3 col-md-3 col-lg-3">Listed On:</b>
                              <p class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><?php echo date('F, d Y', strtotime($item['date']))?></p>
                          </span>



                    <div class="fb-like" data-href="http://rulgaye.in/item/itemDetail/<?php echo $item['id']?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true" width="100%"></div>

                    <div class="contactseller">
                        <?php if(isset($this->session->userdata['id'])){?>
                            <?php if($item['category']==4){?>
                                <a data-target="#contactModal" data-toggle="modal" href="#" class="btn btn-seller">Apply Here</a>
                            <?php }else{?>
                                <a data-target="#contactModal" data-toggle="modal" href="#" class="btn btn-seller">Contact Seller</a>
                            <?php }}?>
                    </div>
                    <div id="contactModal" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <?php if($item['category']==4){?>
                                    <h4>Apply to this Job
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </h4>
                                    <?php }else{?>
                                        <h4>Contact Seller
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </h4>
                                    <?php }?>

                                </div>
                                <div class="modal-body">
                                    <form class="form-signin" action="<?php echo base_url().'user/create_communication/'.$item['id'].'/'.$item['user_id']?>" method="post">
                                        <label>Subject:</label>
                                        <input name="subject" type="text" class="form-control"/>
                                        <?php if($item['category']==4){?>
                                        <label>Paste Your Resume:</label>
                                        <?php }else{?>
                                        <label>Message:</label>
                                        <?php }?>
                                        <textarea name="content" id="mytextarea" class="form-control"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-success pull-right">Submit</button>
                                </div>
                                </form>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                    <?php if(isset($this->session->userdata['id'])){?>
                    <div class="contactseller">
                        <a data-target="#reviewModal" data-toggle="modal" href="#" class="btn btn-seller">Add Reviews</a>
                    </div>
                    <?php }?>
                    <div id="reviewModal" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4>Add Reviews
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <form class="form-signin" action="<?php echo base_url().'item/add_rating/'.$item['id']?>" method="post">
                                        <label>Rating:</label><br/>
                                        <div class="rating-stars">
                                            <input type="radio" name="rating" id="group-3-0" value="5"   />
                                            <label for="group-3-0"></label>
                                            <input type="radio" name="rating" id="group-3-1"  value="4"  />
                                            <label for="group-3-1"></label>
                                            <input type="radio"  name="rating" id="group-3-2"  value="3"  />
                                            <label for="group-3-2"></label>
                                            <input type="radio" name="rating" id="group-3-3"  value="2"  />
                                            <label for="group-3-3"></label>
                                            <input type="radio" name="rating"  id="group-3-4" value="1" />
                                            <label for="group-3-4"></label>
                                        </div><br/>
                                        <label>Comment:</label>
                                        <textarea class="form-control" name="content" required></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-success pull-right">Submit</button>
                                    <button class="btn btn-default pull-right" data-dismiss="modal">Close</button>

                                </div>
                                </form>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-tab">
                <ul class="nav nav-tabs" id="myTab">

                    <li class="active"><a data-toggle="tab" href="#info">Info</a></li>
                    <li><a data-toggle="tab" href="#review">Reviews</a></li>

                </ul>
                <div class="tab-content">
                    <div id="info" class="tab-pane fade in active">
                        <h3>Description</h3>
                        <?php echo $item['description']?>
                        <?php if(!empty($this->session->userdata['id'])){?>
                        <div class="seller_social">
                            <ul>
                                <li>
                                    <a href="#" class="btn btn-send" data-target="#emailModal" data-toggle="modal">
                                        <i class="fa fa-envelope"></i>
                                        Send to friend
                                    </a>
                                </li>
                                <li>
                                    <?php if($is_favorite==false){?>
                                    <a href="<?php echo base_url().'item/addtofavorite/'.$item['id']?>" class="btn btn-send">
                                        <i class="fa fa-heart"></i>
                                        Add to Favorites
                                    </a>
                                    <?php }else{?>
                                    <span class="btn btn-success">
                                        <i class="fa fa-heart"></i>
                                        Added to Favorites
                                    </span>
                                    <?php }?>

                                </li>
                            </ul>
                        </div>
                        <?php }?>
                        <div id="emailModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <h4>Send to Friend</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-signin" action="" method="post" id="friendForm">
                                            <label>Friend's Email:</label>
                                            <input name="email" type="email" class="form-control"/>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success pull-right">Send</button>
                                    </div>
                                    </form>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <div class="fb-comments" data-href="http://rulgaye.in/item/itemDetail<?php echo $item['id']?>" data-width="100%" data-numposts="5"></div>
                    </div>

                    <div id="review" class="tab-pane fade">
                        <?php
                        if(count($reviews)>0){
                        for($i=0;$i<count($reviews);$i++) {
                            ?>
                            <div class="row" style="padding: 10px;">
                            <div class="col-md-3">
                                <div class="rating-stars">
                                   <?php if ($reviews[$i]['rating'] == 5) { ?>

                                       <i class="fa fa-star"></i>
                                       <i class="fa fa-star"></i>
                                       <i class="fa fa-star"></i>
                                       <i class="fa fa-star"></i>
                                       <i class="fa fa-star"></i>
                                   <?php
                                   }
                                   ?>
                                    <?php if ($reviews[$i]['rating'] == 4) { ?>

                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    <?php
                                    }
                                    ?>
                                    <?php if ($reviews[$i]['rating'] == 3) { ?>

                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    <?php
                                    }
                                    ?>
                                    <?php if ($reviews[$i]['rating'] == 2) { ?>

                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    <?php
                                    }
                                    ?>
                                    <?php if ($reviews[$i]['rating'] == 1) { ?>

                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star"></i>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-5" style="padding: 5px">
                                <?php echo $reviews[$i]['content']?>
                            </div>
                            <div class="col-md-2">
                                <?php echo $reviews[$i]['fname'].' '.$reviews[$i]['lname']?>
                            </div>
                            <div class="col-md-2">
                                <?php echo date('M, d Y',strtotime($reviews[$i]['date']));?>
                            </div>
                            </div>
                        <?php
                        }}else {
                            ?>
                        <center>
                            <i class="fa fa-commenting-o fa-2x"></i>
                            <h5>No review for this item. Be the first to add your review.</h5>
                        </center>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>



        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-3 user-side">
            <h3 class="person-heading">Seller Details</h3>
            <div class="seller_pic">
                <?php
                if(isset($user['image']))
                {
                    $src=base_url().'img/user/'.$user['image'];
                }
                else
                {
                    $src=base_url().'img/avatar-2.png';
                }
                ?>
                <img src="<?php echo $src?>" class="img-responsive img-thumbnail" width="100px" height="100px"/>
            </div>
            <div class="seller-info">
                <h4><b><?php echo $item['fname'].' '.$item['lname']?></b></h4>
                <h5>Joined On: <?php echo date('d, M',strtotime($item['joining_date']));?></h5>
                <a href="<?php echo base_url().'user/public_profile/'.$item['user_id']?>" class="btn btn-owner">Owner Listings</a>
            </div>
            <div class="seller_btn">
                <ul>
                    <li>
                        <a href="#" class="btn btn-fb">
                            <i class="fa fa-facebook"></i>
                            facebook
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn  btn-tw">
                            <i class="fa fa-twitter"></i>
                            twitter
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-gplus">
                            <i class="fa fa-google-plus"></i>
                            google
                        </a>
                    </li>
                </ul>
            </div>

            <div class="seller-website">
                <a href="#">
                    <i class="fa fa-globe"></i>
                    <a href="<?php echo $user['website']?>">Visit Seller's Website</a>
                </a>
            </div>
            <?php
            if(count($favorites)>0) {
                if(count($favorites)>5)
                {
                    $fav_count=5;
                }
                else
                {
                    $fav_count=count($favorites);
                }
                ?>
                <h3 class="person-heading">My Favourites</h3>
                <div class="similar_posts">
                    <ul>
                        <?php
                        for ($i = 0; $i < $fav_count; $i++) {
                            $img = explode(',', $favorites[$i]['my_images']);
                            if($favorites[$i]['cat_id']==4)
                            {
                                $img[0]='avatar_job.png';
                            }
                            ?>
                            <li>
                                <img src="<?php echo base_url() . 'img/' . $img[0] ?>"
                                     class="img-responsive img-thumbnail" width="100px">
                                <a href="<?php echo base_url() . 'item/itemDetail/' . $favorites[$i]['id'].'/'.implode('-',explode(' ',$favorites[$i]['title'])) ?>"
                                   class="post_title">
                                    <?php echo $favorites[$i]['title'] ?>
                                </a>

                                <p><?php echo substr($favorites[$i]['description'], 0, 25) . '...'; ?></p>
                                <b><i class="fa fa-inr"> </i> <?php echo $favorites[$i]['amount'] ?></b>
                            </li>
                        <?php
                        }
                        ?>

                    </ul>
                </div>
            <?php
            }
            ?>
            <h3 class="person-heading">Similar Ads</h3>
            <div class="similar_posts">
                <ul>
                    <?php
                    if(count($ads)>5)
                    {
                        $ads_count=5;
                    }
                    else
                    {
                        $ads_count=count($ads);
                    }
                    for($i=0;$i<$ads_count;$i++) {
                        $img=explode(',',$ads[$i]['my_images']);

                        ?>
                        <li>
                            <?php if($ads[$i]['category']!=4){?>
                            <img src="<?php echo base_url().'img/'.$img[0]?>" class="img-responsive img-thumbnail" width="100px" >
                            <?php
                            }
                            ?>
                            <a href="<?php echo base_url().'item/itemDetail/'.$ads[$i]['id'].'/'.implode('-',explode(' ',$ads[$i]['title']))?>" class="post_title">
                                <?php echo $ads[$i]['title']?>
                            </a>
                            <p><?php echo substr($ads[$i]['description'],0,25).'...';?></p>
                            <b><i class="fa fa-inr"> </i> <?php echo $ads[$i]['amount']?></b>
                        </li>
                    <?php
                    }
                    ?>

                </ul>
            </div>
        </div>

        <!--end-->
    </div>
</div>

