<?php ?>
<section class="site-content">
    <div class="container" style="padding-top: 20px; padding-bottom: 200px;">
        <div class="row">
            <div class="col-md-12">
                <div class="success-outer">

                    <div class="alert alert-success" role="alert">
                        <div style="font-weight: bolder; padding-bottom: 20px;">Thank you for registration!</div>
                        Congratulations! Your account has been created and an email has been sent to you at the email address you provided.
                        Please check your inbox to verify the email address. You might want to look at your spam folder in case you haven't got the email.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
