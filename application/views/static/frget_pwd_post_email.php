<?php
/**
 * Created by PhpStorm.
 * User: Malik Mudassar
 * Date: 8/28/2015
 * Time: 11:06 AM
 */
?>
<div class="container" style="height:300px;">
    <div class="col-md-4"></div>
    <div class="col-md-4 alert alert-success" >


            <p>Dear User!.</p>

            <p>
                Your password reset link is sent to your email address. Please click on the link to reset your password.<br>
                Thank you
            </p>
    </div>
</div>