<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="google-site-verification" content="KxCp76piaInC97TaH_QLHxY2aMFBKiOptwmHhJ93tco" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!--<meta name="description" content="<?php /*echo $meta_description*/?>">-->
    <meta name="title" content="<?php echo $meta_title?>">
    <!--<meta name="keyword" content="<?php /*echo $meta_keywords*/?>">-->

    <?php if(isset($item))
    {
    $images=explode(',',$item['my_images']);
        ?>
        <link href="<?php echo base_url().'item/itemDetail/'.$item['id'].'/'.implode('-',explode(' ',$items['title']))?>" rel="canonical" />

        <meta content="<?php echo $meta_title?>" property="og:title" />
        <!--<meta content="<?php /*echo $meta_description*/?>" property="og:description" />-->
        <meta content="<?php echo base_url().'item/itemDetail/'.$item['id'].'/'.implode('-',explode(' ',$items['title']))?>" property="og:url" />
        <meta content="<?php echo base_url().'img/'.$images[0]?>" property="og:image" />
    <?php }?>
    <link rel="icon" href="../../favicon.ico">

    <title><?php echo $title?></title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>css/bootstrap-social.css" rel="stylesheet">
    <link href="<?php echo base_url()?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>css/bootstrap-slider.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url()?>css/style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap-slider.js"></script>

    <?php if(isset($map)){ echo $map['js'];} ?>
</head>