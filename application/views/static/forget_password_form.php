<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 10/13/2015
 * Time: 12:36 PM
 */
?>
<div class="container" style="height:300px;">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <form action="" method="post">
            <label>
                Password
            </label>
            <input type="password" name="password" class="form-control"><br>
            <label>
                Confirm Password
            </label>
            <input type="password" name="conf_password" class="form-control"><br>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <?php
        if(isset($errors)){
            ?>
            <div class="alert alert-danger fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> <?php echo ($errors);?>
            </div>
        <?php }?>
    </div>
</div>