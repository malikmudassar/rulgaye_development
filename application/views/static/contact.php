      <div class="container">
          <center>
              <h2 class="main-heading">Contact Us</h2>
          </center>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="contact-container">
                  <div class="col-lg-12">
                      <div class="form-area">
                          <?php
                          if(isset($errors)){
                              ?>
                              <div class="alert alert-danger fade in">
                                  <a href="#" class="close" data-dismiss="alert">&times;</a>
                                  <strong>Error!</strong> <?php echo ($errors);?>
                              </div>
                          <?php }?>
                          <?php
                          if(isset($success)){
                              ?>
                              <div class="alert alert-success fade in ">
                                  <a href="#" class="close" data-dismiss="alert">&times;</a>
                                  <strong>Congratulations!</strong> <?php echo ($success);?>
                              </div>
                          <?php }?>
                          <form role="form" action="" method="post">
                              <div class="form-group">
                                  <input type="text" class="form-control" id="name" name="name" placeholder="Name" >
                              </div>
                              <div class="form-group">
                                  <input type="text" class="form-control" id="email" name="email" placeholder="Email" >
                              </div>
                              <div class="form-group">
                                  <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" >
                              </div>
                              <div class="form-group">
                                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" >
                              </div>
                              <div class="form-group">
                                  <textarea name="message" class="form-control" type="textarea" placeholder="Message" maxlength="140" rows="7"></textarea>
                                  <span class="help-block"><p id="characterLeft" class="help-block "></p></span>
                              </div>

                              <button type="submit" class="btn btn-submit btn-seller pull-right">Send</button>
                          </form>

                      </div>

                  </div>
              </div>
          </div>
      </div>