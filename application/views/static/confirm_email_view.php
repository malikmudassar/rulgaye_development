<?php
/**
 * Created by PhpStorm.
 * User: Malik Mudassar
 * Date: 4/16/15
 * Time: 12:38 PM
 */
?>
<section class="site-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="success-outer">

                    <div class="alert alert-success" role="alert">
                        <div ><span><b>Thank you!</b></span> <?php echo $username;?></div>
                        You email address has been verified and your account has been activated.
                        Please <a href="<?php echo base_url()?>user/signin">Click Here</a> to login to your account.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <!-- popular ads-->
    <div class="popular_ads">
        <div class="ads-heading">
            <h3>
                Popular Classified

                <div class="ads_icon">
                    <nav>
                        <ul class="control-box pager">
                            <li><a data-slide="prev" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
                            <li><a data-slide="next" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-right"></i></li>
                        </ul>
                    </nav>
                </div>
            </h3>
        </div>
        <div class="popular-body">
            <div class="container">
                <div class="col-xs-12" >
                    <div class="carousel slide" id="myCarousel">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div><!-- /Slide1 -->
                            <div class="item">
                                <ul class="thumbnails">
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-sm-2">
                                        <div class="fff">
                                            <div class="thumbnail">
                                                <a href="#"><img src="http://placehold.it/360x240" alt=""></a>
                                            </div>
                                            <div class="caption">
                                                <p>Nullam Condimentum Nibh Etiam Sem</p>
                                                <b>$200</b>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div><!-- /Slide2 -->
                        </div>
                        <!-- /.control-box -->
                    </div><!-- /#myCarousel -->
                </div><!-- /.col-xs-12 -->
            </div><!-- /.container -->
        </div>
    </div>
    <!-- popular ads end-->
    <!-- category start-->

    <!--end-->
</div>
