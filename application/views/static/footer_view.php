<footer class="footer">

    <div class="footer_container">
        <div class="container">
            <div class="footer_menu">
                <ul>
                    <li><a href="<?php echo base_url()?>">
                            Home
                        </a>
                    </li>

                    <li><a href="<?php echo base_url().'user/signup'?>">
                            Register
                        </a>
                    </li>
                    <li><a href="<?php echo base_url().'user/signin'?>">
                            Login
                        </a>
                    </li>
                </ul>
            </div>
            <div class="social_media">
                <ul>
                    <li>
                        <a href="#"><i class="fa fa-google"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                </ul>
            </div>
            <div class="divider">
                <hr>
            </div>
            <div class="col-xs-12 footer-links">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" style="padding-right: 30px; text-align: justify">
                    <h4>About Us</h4>
                    <p>RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online, Sell your car online or find your dream job in India and start your career RulGaye.In is your one stop Solution to all your queries.</p>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <h4>Our Conduct</h4>
                    <ul>
                        <li>
                            <a href="<?php echo base_url().'page/careers'?>">Careers at Rulgaye</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'page/affiliates'?>">Rulgaye Affiliates</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'page/terms_conditions'?>">Terms & Conditions</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'page/privacy_policy'?>">Privacy Policy</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'page/faq'?>">FAQ</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 support">
                    <h4>Rulgaye</h4>
                    <ul>
                        <li>
                            <a href="<?php echo base_url().'contact'?>">Contact Us</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'page/support'?>">Support</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'page/feedback'?>">Feedback</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'page/site_map'?>">Site Map</a>
                        </li>

                    </ul>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <h4>Community</h4>
                    <ul>
                        <li>
                            <a href="#">Rulgaye</a>
                        </li>
                        <li>
                            <a href="#">Everything</a>
                        </li>
                        <li>
                            <a href="#">World of Rulgaye</a>
                        </li>
                    </ul>

                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" >
                    <h4>Blog</h4>
                    <ul>
                        <li>
                            <a href="#">Rulgaye Mania</a>
                        </li>
                        <li>
                            <a href="#">News</a>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="copy_right">
                <p>Copyright &copy; 2015 Rulgaye.com. All rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>
