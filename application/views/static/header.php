<script>
    base_url="<?php echo base_url()?>";
</script>
<meta http-equiv="Expires" content="Tue, 01 Jan 1995 12:12:12 GMT">
<meta http-equiv="Pragma" content="no-cache">
<div class="header-container">

    <div class="header_top">

        <div class="container">
            <?php
/*            if((basename($_SERVER['PHP_SELF'])=='index.php') || (basename($_SERVER['PHP_SELF'])=='welcome')) {
                */?><!--
                <button id="map_show" type="button" style="margin-top: 5px;" class="btn btn-xs btn-danger"><i
                        class="fa fa-map"></i> Find on Map
                </button>
            --><?php
/*            }
            */?>
            <div class="top_menu">
                <?php
                if(!isset($this->session->userdata['id'])) {
                    ?>
                    <ul>
                        <li>
                            <a href="<?php echo base_url()?>user/signin">Login </a>
                        </li>
                        <li><span> | </span></li>
                        <li>
                            <a  href="<?php echo base_url()?>user/signup">Register</a>
                        </li>
                    </ul>
                <?php
                }else {
                    ?>
                Hi! <span class="mini-submenu login-link"><?php echo $this->session->userdata['fname'].' '.$this->session->userdata['lname']?><span class="caret"></span> </span>
                    <div class="list-group">

                        <a href="<?php echo base_url().'user/profile'?>" class="list-group-item">
                            <i class="fa fa-user"></i><span>My Ads</span>
                        </a>
                        <a href="<?php echo base_url().'user/messages'?>" class="list-group-item">
                            <i class="fa fa-user"></i><span>Inbox</span>
                        </a>
                        <a href="<?php echo base_url().'user/settings'?>" class="list-group-item">
                            <i class="fa fa-cog"></i><span>Settings</span>
                        </a>
                        <a href="<?php echo base_url().'user/logout'?>" class="list-group-item">
                            <span class="glyphicon glyphicon-off"></span><span>Logout</span>
                        </a>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="map" id="map_canvas">

    </div>
    <div class="header_bg"></div>
    <div class="header-mob"></div>
    <div class="container" >
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div>
                    </div>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo base_url();?>"><i class="fa fa-home"> </i> Home</a></li>
                        <?php
                        for($i=0;$i<count($menu_cats);$i++) {
                            ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $menu_cats[$i]['name']?> <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <?php
                                    for($j=0;$j<count($menu_cats[$i]['sub']);$j++) {
                                    ?>
                                    <li><a href="<?php echo base_url().'item/catListing/'.$menu_cats[$i]['sub'][$j]['id']?>"><?php echo $menu_cats[$i]['sub'][$j]['name']?></a> </li>
                                    <?php
                                    }
                                    ?>

                                </ul>
                            </li>
                        <?php
                        }
                        ?>
                        <li><a href="<?php echo base_url().'contact'?>">Contact Us</a> </li>

                    </ul>

                </div>

            </div>

        </nav>
    </div>
</div>
<script>
    jQuery(function(){
        jQuery('#slide-submenu').on('click',function() {
            jQuery(this).closest('.list-group').fadeOut('slide',function(){
                jQuery('.mini-submenu').fadeIn();
            });

        });
        jQuery('.mini-submenu').on('click',function(){
            jQuery(this).next('.list-group').toggle('slide');
            //jQuery('.mini-submenu').hide();
        });

    });

    jQuery('#map_show').click(function(){
        //alert('I am clicked');
        jQuery('.map').slideToggle(function(){
            google.maps.event.trigger(document.getElementById("map_canvas"), 'resize')
        });

    });
</script>
<style>
    #map_canvas { height: 700px; width:100%; align-content: center }
    .map{display: none;}
</style>
<script>

    /*var map;
    function initialize() {
        var mapOptions = {

            center: new google.maps.LatLng(20.593684, 78.962880),
            zoom:10,
            mapTypeId: google.maps.MapTypeId.ROADMAP

        };
        map = new  google.maps.Map(document.getElementById("map_canvas"),mapOptions);

    }
    google.maps.event.addDomListener(window, 'load', initialize);*/

</script>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '698675913599902',
            xfbml      : true,
            version    : 'v2.5'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=698675913599902";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>