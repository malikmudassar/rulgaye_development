<?php ?>
<div class="container">

    <div >
        <p>
        <h2>Thankyou <?php echo $user_data['username']?> for registration</h2>
        </p>
    </div>
    <div class="col-lg-12" style="border-radius: 10px; background-color: #f5f5f5; padding: 15px;">

        <p style="color: #000">
            Please copy and paste the below URL in your browser or just  <a href="<?php echo base_url().'user/activate/'.$user_data['id'].'/'.$user_data['session']?>">Click Here</a>
            to activate your account.
        </p>
        <b>URL:</b><span><?php echo base_url().'user/activate/'.$user_data['id'].'/'.$user_data['session']?></span>

    </div>
</div>