<?php
/**
 * Created by PhpStorm.
 * User: Malik Mudassar
 * Date: 5/12/15
 * Time: 3:35 PM
 */
?>
<section class="site-content">
    <div class="container">
        <div class="account-block">
            <div class="heading">
                <h1>Contact Us</h1>
                <h2><span>We'd love to hear from you</span></h2>
            </div>
            <div class="contact-info">
                <div class="row">
                    <form class="text-left clearfix">


                        <div class="col-md-8 col-sm-8">
                            <h3>DROP US A LINE</h3>
                            <div class="form-group">
                                <label for="name" class="custom-label">Your name <span class="required">*</span></label>
                                <input type="text" class="form-control custom-input" id="name" placeholder="Mathew" required>
                            </div>
                            <div class="form-group">
                                <label for="username" class="custom-label">Blue Lizard Audio Username <span class="required">*</span></label>
                                <input type="text" class="form-control custom-input" id="username" placeholder="john Mathew" required>
                            </div>
                            <div class="form-group">
                                <label for="email" class="custom-label">Your email address <span class="required">*</span></label>
                                <input type="email" class="form-control custom-input" id="email" placeholder="john@bluelizardaudio.com" required>
                            </div>
                            <div class="form-group">
                                <label for="subject" class="custom-label">Subject <span class="required">*</span></label>
                                <input type="text" class="form-control custom-input" id="subject" placeholder="Type here..." required>
                            </div>
                            <h4>Is this request about a specific listing?</h4>
                            <div class="option-block">
                                <input id="checkbox12" type="checkbox" name="checkbox" value="1">
                                <label for="checkbox12"><span></span>Yes</label>
                            </div>
                            <div class="hidden-section">
                                <div class="form-group">
                                    <label for="listingId" class="custom-label">Please provide the Listing ID:</label>
                                    <input type="text" class="form-control custom-input" id="listingId" placeholder="LIS583EI" required>
                                </div>
                                <div class="form-group">
                                    <label for="listingTitle" class="custom-label">Or provide the Listing title:</label>
                                    <input type="text" class="form-control custom-input" id="listingTitle" placeholder="Bookshelf Speakers" required>
                                </div>
                            </div>
                            <div class="form-group clearfix padd-both-none">
                                <label for="category" class="custom-label label-height">What is your inquiry regarding?</label>

                                <select class="basic" data-placeholder="Accessories">
                                    <option value="one">One</option>
                                    <option value="two">Two</option>
                                    <option value="three">Three</option>
                                    <option value="four">Four</option>
                                    <option value="five">Five</option>
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="message" class="custom-label">Message <span class="required">*</span></label>
                                <textarea class="form-control custom-input" rows="7" placeholder="Type here..." required></textarea>
                            </div>

                            <button type="submit" class="btn signin-btn blue-btn btn-common">Send Email</button>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <h3>GIVE US A CALL</h3>

                            <div class="contact"><i class="fa fa-phone-square"></i><?php echo $contact['phone_1']?></div>
                            <div class="contact"><i class="fa fa-phone-square"></i><?php echo $contact['phone_2']?></div>
                            <div class="contact"><i class="fa fa-phone-square"></i><?php echo $contact['uan']?></div>
                            <div class="contact"><i class="fa fa-envelope"></i><a href="mailto:<?php echo $contact['email']?>"><?php echo $contact['email']?></a></div>
                            <div class="contact"><i class="fa fa-map-marker" style="font-size:20px;"></i><?php echo $contact['address']?></div>

                        </div>
                        <div class="clearfix"></div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

