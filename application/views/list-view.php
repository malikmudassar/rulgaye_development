<?php
/**
 * Created by PhpStorm.
 * User: Malik Mudassar
 * Date: 4/9/15
 * Time: 3:47 PM
 */
if(!isset($address_dropdown))
{
    for ($i=0;$i<count($models);$i++)
    {
?>
      <option value="<?php echo $models[$i]['id']?>" <?php
      if($i==0)
      {?>
          selected="selected"
      <?php
      }
      ?>><?php echo $models[$i]['name'];?></option>
    <?php
    }
}
else
{
    foreach ($address as $row){
    ?>
        <option value="<?php echo $row['id']?>" <?php
        if($selected == $row['id'])
        {?>
            selected="selected"
        <?php
        }
        ?> ><?php echo $row['street1'].' '.$row['street2'].' '.$row['city'].' '.$row['state']?></option>
    <?php
    }
}
?>
