<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 10/9/2015
 * Time: 4:47 PM
 */
?>
<div class="container">
    <p>
        <a href="<?php echo base_url().'admin/add_page'?>" class="btn btn-success">- New Page Content -</a>
    </p>
    <h3>List of Pages</h3>
    <hr>
    <div class="col-md-9">
        <table class="table">
        <tr>
            <th>Page Title</th>
            <th>Page Type</th>
            <th>Action</th>
        </tr>
        <?php
        for($i=0;$i<count($pages);$i++) {
        ?>
            <tr>
                <td><?php echo $pages[$i]['title']?></td>
                <td><?php echo $pages[$i]['page_type']?></td>
                <td><a href="" class="btn btn-default">Edit</a> <a href="" class="btn btn-danger">Delete</a> </td>
            </tr>
        <?php
        }
        ?>
        </table>
    </div>
</div>