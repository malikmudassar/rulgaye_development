<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 10/9/2015
 * Time: 4:49 PM
 */
?>
<script type="text/javascript" src="<?php echo base_url()?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#mytextarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: false,
        height : 200,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });
</script>
<div class="container">
    <h3>Add Content to Page</h3>

    <div class="col-md-6">
        <?php
        if(isset($errors)){
            ?>
            <div class="alert alert-danger fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> <?php echo ($errors);?>
            </div>
        <?php }?>
        <form action="" method="post">
            <table class="table table-stripped">
                <tr>
                    <td>
                        <select name="page_type" class="form-control">
                            <option value="">-Select Page Type-</option>
                            <?php for($i=0;$i<count($page_types);$i++){?>
                                <option value="<?php echo $page_types[$i]['id']?>"><?php echo $page_types[$i]['name']?></option>
                            <?php }?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="title" placeholder="Page Title" class="form-control">
                    </td>
                </tr>
                <tr>
                    <td>
                        <textarea name="content" id="mytextarea"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="<?php echo base_url().'admin/static_pages'?>" class="btn btn-danger">Cancel</a>
                    </td>
                </tr>
            </table>
        </form>

    </div>
</div>