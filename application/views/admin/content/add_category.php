<div id="page-container" class="sidebar-full">
    <div id="sidebar">
        <div class="sidebar-scroll">
            <div class="sidebar-content">
                <a href="<?php echo base_url().'admin'?>" class="sidebar-brand">
                    <strong>Admin </strong>Panel
                </a>

                <ul class="sidebar-nav">
                    <li>
                        <a href="<?php echo base_url().'admin'?>" class=" active"><i class="gi gi-stopwatch sidebar-nav-icon"></i>Dashboard</a>
                    </li>

                    <li class="sidebar-header">
                        <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip"
                                                                         title="Quick Settings"><i
                                    class="gi gi-settings"></i></a></span>
                        <span class="sidebar-header-title">Management</span>
                    </li>
                    <li>
                        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i
                                class="gi gi-certificate sidebar-nav-icon"></i>Products</a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url().'admin/new_products'?>">New Products</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'admin/active_products'?>">Active Products</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'admin/categories'?>">List Categories</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'admin/add_category'?>">Add Category</a>
                            </li>


                        </ul>
                    </li>
                    <li>
                        <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i
                                class="gi gi-notes_2 sidebar-nav-icon"></i>Users</a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url().'admin/newUsers'?>">New Users</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'admin/activeUsers'?>">Active Users</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'admin/suspendedUsers'?>">Suspended Users</a>
                            </li>

                        </ul>
                    </li>

                </ul>

            </div>
        </div>
    </div>
    <div id="main-container">
        <header class="navbar navbar-default">
            <ul class="nav navbar-nav-custom">
                <li class="hidden-xs hidden-sm">
                    <a href="javascript:void(0)" id="sidebar-toggle-lg">
                        <i class="fa fa-list-ul fa-fw"></i>
                    </a>
                </li>
                <li class="hidden-md hidden-lg">
                    <a href="javascript:void(0)" id="sidebar-toggle-sm">
                        <i class="fa fa-bars fa-fw"></i>
                    </a>
                </li>
                <li class="hidden-md hidden-lg">
                    <a href="./index.php.html">
                        <i class="gi gi-stopwatch fa-fw"></i>
                    </a>
                </li>
            </ul>
            <form action="page_ready_search_results.php" method="post" class="navbar-form-custom" role="search"/>
            <div class="form-group">
                <input type="text" id="top-search" name="top-search" class="form-control" placeholder="Search.."/>
            </div>
            </form>

        </header>
        <div id="page-content">

            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="widget">
                        <div class="widget-simple">
                            <a href="./page_ready_article.php.html"
                               class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                                <i class="fa fa-file-text"></i>
                            </a>

                            <h3 class="widget-content text-right animation-pullDown">
                                Total <strong>Ads</strong><br/>
                                <small><?php echo $total_ads?></small>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="widget">
                        <div class="widget-simple">
                            <a href="./page_comp_charts.php.html"
                               class="widget-icon pull-left themed-background-spring animation-fadeIn">
                                <i class="gi gi-user"></i>
                            </a>

                            <h3 class="widget-content text-right animation-pullDown">
                                Total <strong>Users</strong><br/>
                                <small><?php echo $total_users?></small>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="widget">
                        <div class="widget-simple">
                            <a href="./page_ready_inbox.php.html"
                               class="widget-icon pull-left themed-background-fire animation-fadeIn">
                                <i class="gi gi-briefcase"></i>
                            </a>

                            <h3 class="widget-content text-right animation-pullDown">
                                Total <strong>Jobs</strong>
                                <small><?php echo $total_jobs?></small>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="widget">
                        <div class="widget-simple">
                            <a href="./page_comp_gallery.php.html"
                               class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                                <i class="gi gi-car"></i>
                            </a>

                            <h3 class="widget-content text-right animation-pullDown">
                                Total <strong>Autos</strong>
                                <small><?php echo $total_autos?></small>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="widget">
                        <div class="widget-simple">
                            <a href="./page_comp_charts.php.html"
                               class="widget-icon pull-left themed-background animation-fadeIn">
                                <i class="gi gi-home"></i>
                            </a>

                            <div class="pull-right">
                                <span id="mini-chart-sales"></span>
                            </div>
                            <h3 class="widget-content animation-pullDown visible-lg">
                                Total <strong>Properties</strong>
                                <small><?php echo $total_property?></small>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="widget">
                        <div class="widget-simple">
                            <a href="./page_widgets_stats.php.html"
                               class="widget-icon pull-left themed-background animation-fadeIn">
                                <i class="gi gi-crown"></i>
                            </a>

                            <div class="pull-right">
                                <span id="mini-chart-brand"></span>
                            </div>
                            <h3 class="widget-content animation-pullDown visible-lg">
                                Total <strong>Classifieds</strong>
                                <small><?php echo $total_classifieds?></small>
                            </h3>
                        </div>
                    </div>
                </div>

            </div>
            <div class="block">

                <div class="block-title">
                    <h2><strong>ADD</strong> CATEGORY</h2>
                </div>
                <form action="page_forms_general.php" method="post" enctype="multipart/form-data"
                      class="form-horizontal form-bordered" onsubmit="return false;"/>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Name</label>

                    <div class="col-md-9">
                        <input type="text" id="example-text-input" name="category"
                               class="form-control" placeholder=""/>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-file-input">Image</label>

                    <div class="col-md-9">
                        <input type="file" id="example-file-input" name="userfile"/>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i>
                            Submit
                        </button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset
                        </button>
                    </div>
                </div>
                </form>
            </div>
                </div>
            </div>
        </div>

    </div>
</div>
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
