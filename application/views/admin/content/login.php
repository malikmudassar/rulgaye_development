<div id="login-container" class="animation-fadeIn">
    <div class="login-title text-center">
        <h1><img src="<?php echo base_url().'assets/images/logo-sm.png'?>" class="gi gi-flash"/></i></br> <small>Please <strong>Login</strong></small></h1>
    </div>
    <div class="block remove-margin">
        <?php
        if(isset($errors)){
            ?>
            <div style="width: 75%; margin: 0 auto;">
                <div class="alert alert-danger fade in">
                    <strong>Error!</strong>
                    <?php echo ($errors);?>
                </div>
            </div>
        <?php }?>
        <form action="" method="post" id="form-login" class="form-horizontal form-bordered form-control-borderless" />
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                    <input type="text" id="login-email" name="username" class="form-control input-lg" placeholder="User Name" />
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                    <input type="password" id="login-password" name="password" class="form-control input-lg" placeholder="Password" />
                </div>
            </div>
        </div>
        <div class="form-group form-actions">
            <div class="col-xs-4">
                <label class="switch switch-primary" data-toggle="tooltip" title="Remember Me">
                    <input type="checkbox" id="login-remember-me" name="rememberme" checked="" />
                    <span></span>
                </label>
            </div>
            <div class="col-xs-8 text-right">
                <button type="submit" name="login" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login</button>
                <button type="reset" name="reset" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Reset</button>

            </div>
        </div>
        </form>
    </div>
</div>
