<div class="container">
    <header style="">
        <h2>RulGaye - Watch Tower</h2>
        <div class="row">
            <a style="float: right" href="<?php echo base_url().'admin/logout'?>" class="btn btn-link"> <i class="fa fa-user"></i>Logout</a>
        </div>
        <div  style="">
            <nav class="navbar nabvar-inverse">
                <ul class="nav nav-tabs ">
                    <?php if((basename($_SERVER['PHP_SELF'])=='admin')||(basename($_SERVER['PHP_SELF'])=='')){?>
                        <li class="active"><a href="<?php echo base_url()?>admin/">Dashboard</a></li>
                    <?php }else{?>
                        <li><a role="presentation" href="<?php echo base_url()?>admin/">Dashboard</a></li>
                    <?php }?>

                    <?php if((basename($_SERVER['PHP_SELF'])=='categories')){?>
                        <li class="active"><a href="<?php echo base_url()?>admin/categories">Categories</a></li>
                    <?php }else{?>
                        <li><a role="presentation" href="<?php echo base_url()?>admin/categories">Categories</a></li>
                    <?php }?>

                    <?php if((basename($_SERVER['PHP_SELF'])=='dashboard')||(basename($_SERVER['PHP_SELF'])=='users')){?>
                        <li class="active"><a href="<?php echo base_url()?>admin/users">Users</a></li>
                    <?php }else{?>
                        <li><a role="presentation" href="<?php echo base_url()?>admin/users">Users</a></li>
                    <?php }?>

                    <?php if((basename($_SERVER['PHP_SELF'])=='ads')){?>
                        <li class="active"><a href="<?php echo base_url()?>admin/ads">Ads</a></li>
                    <?php }else{?>
                        <li><a role="presentation" href="<?php echo base_url()?>admin/ads">Ads</a></li>
                    <?php }?>

                    <?php if((basename($_SERVER['PHP_SELF'])=='dashboard')||(basename($_SERVER['PHP_SELF'])=='web_configs')){?>
                        <li class="active"><a href="<?php echo base_url()?>admin/web_configs">Web Configurations</a></li>
                    <?php }else{?>
                        <li><a role="presentation" href="<?php echo base_url()?>admin/web_configs">Web Configurations</a></li>
                    <?php }?>

                    <?php if((basename($_SERVER['PHP_SELF'])=='static_pages')||(basename($_SERVER['PHP_SELF'])=='static_pages')){?>
                        <li class="active"><a href="<?php echo base_url()?>admin/static_pages">Static Pages</a></li>
                    <?php }else{?>
                        <li><a role="presentation" href="<?php echo base_url()?>admin/static_pages">Static Pages</a></li>
                    <?php }?>

                    <?php if((basename($_SERVER['PHP_SELF'])=='jobs')||(basename($_SERVER['PHP_SELF'])=='jobs')){?>
                        <li class="active"><a href="<?php echo base_url()?>admin/jobs">Jobs</a></li>
                    <?php }else{?>
                        <li><a role="presentation" href="<?php echo base_url()?>admin/jobs">Jobs</a></li>
                    <?php }?>
                </ul>
            </nav>
        </div>

    </header>
</div>