<?php
?>
<div class="container">
    <h2>List of Ads</h2>
    <hr>
    <span>Total Ads : <?php echo count($ads)?></span>
    <hr>
    <table class="table">
        <tr>
            <th>Image</th>
            <th>Title</th>
            <th>Date Posted</th>
            <th>Posted By</th>
            <th>Action</th>
        </tr>
        <?php
        for($i=0;$i<count($ads);$i++){
        ?>
        <tr>
            <td>
                <?php
                $image=explode(',',$ads[$i]['my_images']);
                ?>
                <img src="<?php echo base_url().'img/'.$image[0]?>" style="height: 80px;">
            </td>
            <td>
                <?php echo $ads[$i]['title']?>
            </td>
            <td>
                <?php echo date('M d Y',strtotime($ads[$i]['date']))?>
            </td>
            <td>
                <?php
                echo $ads[$i]['fname'].' '.$ads[$i]['lname']
                ?>
            </td>
            <td>
                <?php if($ads[$i]['status']<2){?>
                <i class="fa fa-trash">-</i><a href="<?php echo base_url().'admin/del_item/'.$ads[$i]['id']?>">Delete</a>  <i class="fa fa-check">-</i> <a href="">Approve</a>
                <?php }else{ ?>
                <span style="color:red; font-weight: bolder">Item Deleted</span>
                <?php }?>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>