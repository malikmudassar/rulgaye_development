<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/8/2015
 * Time: 10:11 AM
 */
/*echo '<pre>';
print_r($categories);
echo '</pre>';*/
?>
<div class="container">
    <div class="row ">

            <?php
            for($i=0;$i<count($categories);$i++) {
                ?>
                <div class="col-md-3 ll">
                        <i class="fa <?php echo $categories[$i]['icon']?>"></i><span><?php echo $categories[$i]['name'];?></span>
                        <ul>
                            <?php
                            for($j=0;$j<count($categories[$i]['sub']);$j++) {
                                ?>
                                <li><i class="fa <?php echo $categories[$i]['sub'][$j]['icon']?>">.- </i><a href=""><?php echo $categories[$i]['sub'][$j]['name']?></a> </li>
                            <?php
                            }
                            ?>

                        </ul>
                </div>
            <?php
            }
            ?>

    </div>
    <div class="row">
        <div class="col-md-4">
            <?php
            if(isset($errors)){
                ?>
                <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong> <?php echo ($errors);?>
                </div>
            <?php }?>
            <form action="" method="post">
                <table class="table">
                    <tr>
                        <td>
                            <h3>Add Category</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select name="parent_category" class="form-control">
                                <option value="">- Parent Category -</option>
                                <?php
                                for($i=0;$i<count($categories);$i++) {
                                    ?>

                                    <option value="<?php echo $categories[$i]['id'] ?>"><?php echo $categories[$i]['name'] ?></option>

                                    <?php
                                    for($j=0;$j<count($categories[$i]['sub']);$j++) {
                                        ?>
                                        <option value="<?php echo $categories[$i]['sub'][$j]['id']?>"> - <?php echo $categories[$i]['sub'][$j]['name']?></option>
                                    <?php
                                    }

                                }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td><input name="cat_name" type="text" class="form-control" placeholder="Category Name"> </td>
                    </tr>
                    <tr>
                        <td>
                            <button type="submit" class="btn btn-primary">Add Category</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-md-4">
            <?php
            if(isset($errors_icon)){
                ?>
                <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong> <?php echo ($errors_icon);?>
                </div>
            <?php }?>
            <form action="" method="post" id="icon_form">
                <table class="table">
                    <tr>
                        <td>
                            <h3>Assign Icon to Category</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select name="category" class="form-control">
                                <option value="">- Select Category -</option>
                                <?php
                                for($i=0;$i<count($categories);$i++) {
                                    ?>

                                        <option value="<?php echo $categories[$i]['id'] ?>"><?php echo $categories[$i]['name'] ?></option>

                                            <?php
                                            for($j=0;$j<count($categories[$i]['sub']);$j++) {
                                                ?>
                                                <option value="<?php echo $categories[$i]['sub'][$j]['id']?>"> - <?php echo $categories[$i]['sub'][$j]['name']?></option>
                                            <?php
                                            }

                                    }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td><input name="icon_name" type="text" class="form-control" placeholder="Icon Name">
                        <input type="hidden" name="icon_form" value="1"> </td>
                    </tr>
                    <tr>
                        <td>
                            <button type="submit" class="btn btn-primary" onclick="javascript:icon_form_submit()">- Assign -</button>
                        </td>
                    </tr>
                </table>
                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Note!</strong> <p>The Icon names are from Fontawesome Icon List found at
                    <a href="https://fortawesome.github.io/Font-Awesome/icons/">This Link</a>. Only provide the icon name as fa-icon </p>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function icon_form_submit()
    {
        $('#icon_form').submit();
    }
</script>
<style>
    .ll:hover
    {
        background-color: #f3f3f3;
    }
    ul
    {
        list-style: none;
        padding-left: 5px;
    }
</style>