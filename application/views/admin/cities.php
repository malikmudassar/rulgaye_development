<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/8/2015
 * Time: 10:11 AM
 */
/*echo '<pre>';
print_r($categories);
echo '</pre>';*/
?>
<div class="container">
    <div class="row ">

        <?php
        for($i=0;$i<count($cities);$i++) {
            ?>
            <div class="col-md-2 ll" style="height: 200px; overflow-x: auto; padding: 10px; ">
                <i class="fa <?php echo $cities[$i]['icon']?>"></i><span><?php echo $cities[$i]['name'];?></span>
                <ul>
                    <?php
                    for($j=0;$j<count($cities[$i]['sub']);$j++) {
                        ?>
                        <li><i class="fa <?php echo $cities[$i]['sub'][$j]['icon']?>">.- </i><a href=""><?php echo $cities[$i]['sub'][$j]['name']?></a> </li>
                    <?php
                    }
                    ?>

                </ul>
            </div>

        <?php
        }
        ?>

    </div>
    <div class="row">

        <div class="col-md-4">
            <?php
            if(isset($errors)){
                ?>
                <div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong> <?php echo ($errors);?>
                </div>
            <?php }?>
            <form action="" method="post" id="icon_form">
                <table class="table">
                    <tr>
                        <td>
                            <h3>Add City in State</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select name="state" class="form-control">
                                <option value="">- Select State -</option>
                                <?php
                                for($i=0;$i<count($cities);$i++) {
                                    ?>

                                    <option value="<?php echo $cities[$i]['id'] ?>"><?php echo $cities[$i]['name'] ?></option>
                                    <?php
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td><input name="city_name" type="text" class="form-control" placeholder="City Name">
                            <input type="hidden" name="icon_form" value="1"> </td>
                    </tr>
                    <tr>
                        <td>
                            <button type="submit" class="btn btn-primary" onclick="javascript:icon_form_submit()">- Add City -</button>
                        </td>
                    </tr>
                </table>

            </form>
        </div>

    </div>

</div>
<script>
    function icon_form_submit()
    {
        $('#icon_form').submit();
    }
</script>
<style>
    .ll:hover
    {
        background-color: #f3f3f3;
    }
    ul
    {
        list-style: none;
        padding-left: 5px;
    }
</style>