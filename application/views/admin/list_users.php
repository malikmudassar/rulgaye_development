<?php
?>
<div class="container">
    <div class="col-md-9">
        <table class="table">
            <tr>
                <th>
                    Email
                </th>
                <th>
                    Full Name
                </th>
                <th>
                    Date Registered
                </th>
                <th>
                    Action
                </th>
            </tr>
            <?php
            for($i=0;$i<count($user);$i++){
            ?>
            <tr>
                <td>
                    <?php echo $user[$i]['email']?>
                </td>
                <td>
                    <?php echo $user[$i]['fname'].' '.$user[$i]['lname']?>
                </td>
                <td>
                    <?php echo date('M d Y',strtotime($user[$i]['date']))?>
                </td>
                <td>
                    <a href="<?php echo $user[$i]['id']?>" class="btn btn-danger">Suspend</a>
                    <span class="btn btn-success">Approved</span>
                </td>
            </tr>
            <?php
            }
            ?>
        </table>
    </div>
</div>