<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> </html><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

    <meta charset="utf-8" />
    <title>Amore Admin Panel </title>
    <meta name="description" content="ProUI is a Responsive Admin Dashboard Template created by pixelcave and published on Themeforest. This is the demo of ProUI! You need to purchase a license for legal use!" />
    <meta name="author" content="pixelcave" />
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0" />
    <link rel="shortcut icon" href="<?php echo base_url().'assets_admin/'?>/img/favicon.ico" />
    <link rel="apple-touch-icon" href="<?php echo base_url().'assets_admin/'?>/img/icon57.png" sizes="57x57" />
    <link rel="apple-touch-icon" href="<?php echo base_url().'assets_admin/'?>/img/icon72.png" sizes="72x72" />
    <link rel="apple-touch-icon" href="<?php echo base_url().'assets_admin/'?>/img/icon76.png" sizes="76x76" />
    <link rel="apple-touch-icon" href="<?php echo base_url().'assets_admin/'?>/img/icon114.png" sizes="114x114" />
    <link rel="apple-touch-icon" href="<?php echo base_url().'assets_admin/'?>/img/icon120.png" sizes="120x120" />
    <link rel="apple-touch-icon" href="<?php echo base_url().'assets_admin/'?>/img/icon144.png" sizes="144x144" />
    <link rel="apple-touch-icon" href="<?php echo base_url().'assets_admin/'?>/img/icon152.png" sizes="152x152" />
    <link rel="stylesheet" href="<?php echo base_url().'assets_admin/'?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets_admin/'?>/css/plugins.css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets_admin/'?>/css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url().'assets_admin/'?>/css/themes.css" />
    <script src="<?php echo base_url().'assets_admin/'?>/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>