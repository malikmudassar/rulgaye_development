<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Get_BLA_ListingImage
 *
 * Lets you determine whether an array index is set and whether it has a value.
 * If the element is empty it returns FALSE (or whatever you specify as the default value.)
 *
 * @access	public
 * @param	Integer
 * @param	Integer
 * @param	string
 * @param	mixed
 * @return	mixed	depends on what the array contains
 */
if ( ! function_exists('Get_BLA_ListingImage'))
{
    function Get_BLA_ListingImage($ListingID,$UserID,$ImageName, $default = FALSE)
    {
        $ImageURL='';
        if(file_exists(HEAD_ROOTURL."resources/{$UserID}/{$ListingID}/{$ImageName}"))
        {
            $ImageURL = HEAD_URL."resources/{$UserID}/{$ListingID}/{$ImageName}";
        }
        else
        {
            $ImageURL = HEAD_URL."images/default-item.jpg";
        }
        return $ImageURL;
    }
}

if ( ! function_exists('Get_User_Profile_Image'))
{
    function Get_User_Profile_Image($ImageName,$default = FALSE)
    {
        $ImageURL='';
        if($ImageName!='')
        {
            if(file_exists(HEAD_ROOTURL."images/profile/{$ImageName}"))
            {
                $ImageURL = HEAD_URL."images/profile/{$ImageName}";
            }
            else
            {
                $ImageURL = HEAD_URL."images/default-profile.jpg";
            }
        }
        else
        {
            $ImageURL = HEAD_URL."images/default-profile.jpg";
        }
        return $ImageURL;
    }
}
// ------------------------------------------------------------------------

/**
 *
/**
 * Created by PhpStorm.
 * User: Given by Saira Saeed
 * Date: 4/8/15
 * Time: 4:07 PM
 * This function get Coordinates against an address
 * from Google Map API and insert the coordinates
 * in Database in Item table
 *
 *
 * @access	public
 * @param	string
 * @return	mixed	depends on what the array contains
// */
if ( ! function_exists('GetAllLatLong'))
{
    function GetAllLatLong($Address, $UserProxy = false) {
    $UserProxy = false;
//$ProxyInfo = $this->GetCurrentProxy();

    $result = array();
    $result['Status'] = "Fail";
    $Address = urlencode($Address);
    //$FileContent = file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=$Address&sensor=true");
    $Address = "http://maps.googleapis.com/maps/api/geocode/json?address=$Address&sensor=true";


    //IF ($UserProxy)
    // $proxy = $ProxyInfo['ip'];
    //$proxyauth = "{$ProxyInfo['username']}:{$ProxyInfo['password']}";

    $CURL = curl_init();
    curl_setopt($CURL, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($CURL, CURLOPT_URL, $Address);

    // IF ($UserProxy)
    // curl_setopt($CURL, CURLOPT_PROXY, $proxy);
    //curl_setopt($CURL, CURLOPT_PROXYUSERPWD, $proxyauth);

    $FileContent = curl_exec($CURL);

    $ResponseHeader = curl_getinfo($CURL);
    $err_no = curl_errno($CURL);
    $err_msg = curl_error($CURL);
    curl_close($CURL);

    $ParsedFileContent = json_decode($FileContent, true);

    $CountryName = 0;
    $gCountry = NULL;
    $gAdminArea1 = NULL;
    $gAdminArea2 = NULL;
    $gAdminArea3 = NULL;
    $gLocality = NULL;
    $gSubLocality = NULL;
    $gRoute = NULL;

    if ($ParsedFileContent['status'] == 'OK') {

        foreach ($ParsedFileContent['results'][0]['address_components'] as $AC) {
            if (array_search("country", $AC['types']) > -1) {

                $CountryName = $AC['long_name'];
                $gCountry = substr($AC['short_name'], 0, 60);
            }
            if (array_search("route", $AC['types']) > -1) {

                $gRoute = substr($AC['short_name'], 0, 60);
            }
            if (array_search("sublocality", $AC['types']) > -1) {

                $gSubLocality = substr($AC['short_name'], 0, 60);
            }
            if (array_search("locality", $AC['types']) > -1) {

                $gLocality = substr($AC['short_name'], 0, 60);
            }
            if (array_search("administrative_area_level_3", $AC['types']) > -1) {

                $gAdminArea3 = substr($AC['short_name'], 0, 60);
            }
            if (array_search("administrative_area_level_2", $AC['types']) > -1) {

                $gAdminArea2 = substr($AC['short_name'], 0, 60);
            }
            if (array_search("administrative_area_level_1", $AC['types']) > -1) {

                $gAdminArea1 = substr($AC['short_name'], 0, 60);
            }
        }

        if ($CountryName !== 0) {

            //$CountryName = $this->CountryIDFromCountryName($CountryName);
        }

        if (array_search("country", $ParsedFileContent['results'][0]['types']) > -1) {
            $IsCountry = 1;
        } else {
            $IsCountry = 0;
        }

        $result['NELat'] = $ParsedFileContent['results'][0]['geometry']['viewport']['northeast']['lat'];
        $result['NELong'] = $ParsedFileContent['results'][0]['geometry']['viewport']['northeast']['lng'];
        $result['SWLat'] = $ParsedFileContent['results'][0]['geometry']['viewport']['southwest']['lat'];
        $result['SWLong'] = $ParsedFileContent['results'][0]['geometry']['viewport']['southwest']['lng'];
        $result['Lat'] = $ParsedFileContent['results'][0]['geometry']['location']['lat'];
        $result['Long'] = $ParsedFileContent['results'][0]['geometry']['location']['lng'];
        $result['Country'] = $CountryName;
        $result['IsCountry'] = $IsCountry;
        $result['gRoute'] = $gRoute;
        $result['gSubLocality'] = $gSubLocality;
        $result['gLocality'] = $gLocality;
        $result['gAdminArea3'] = $gAdminArea3;
        $result['gAdminArea2'] = $gAdminArea2;
        $result['gAdminArea1'] = $gAdminArea1;
        $result['gCountry'] = $gCountry;
        $result['Status'] = "OK";
    }
    return $result;
}
}