<?php
/* Autho: Rizwan Abbas rizwan@zeropoint.it
 * Organization: zeropoint.it
 * This script crawls the data from OLX only
 * Currently it targets cars category and all its html elements
 * 
 */
include_once('simple_html_dom.php');
error_reporting(E_ALL);
ini_set('display_errors', 1);

function getBetween($var1="",$var2="",$pool){
	$temp1 = strpos($pool,$var1)+strlen($var1);
	$result = substr($pool,$temp1,strlen($pool));
	$dd=strpos($result,$var2);
	if($dd == 0){
		$dd = strlen($result);
	}
}
	
function getDetails($url) {
	 
	   $html = file_get_html($url);
        
	    
	   $attributes	=	array();
	  
		$attributes['attributes']['Ref No']	=	trim($html ->find('span[class=rel inlblk]',0)->innertext); 
		if($attributes['attributes']['Ref No']==''){

			return array();
		}
										
		$attributes['carinfo']['price'] =	trim(ltrim(str_replace(',','',@$html ->find('strong[class=xxxx-large margintop7 block not-arranged]',0)->innertext),' Rs'));
			
			if($attributes['carinfo']['price']==''){
								$attributes['carinfo']['price'] = trim(ltrim(str_replace(',','',@$html ->find('strong[class=xxxx-large margintop7 block arranged]',0)->innertext),' Rs'));
			}
		
	 	$attributes['carinfo']['Title'] 	=	trim($html ->find('h1[class=brkword lheight28]',0)->innertext); 
		if($attributes['carinfo']['Title']==''){
			$attributes['carinfo']['Title'] 	=	trim($html ->find('div[class=clr offerheadinner pding15 pdingright20]>h1',0)->innertext); 
		}
		
		$attributes['carinfo']['Title'] = trim('0000',$attributes['carinfo']['Title']);
		
		$attributes['attributes']['Year']	=	trim($html ->find('strong[class=block]',1)->innertext); 
						//	details fixed marginbott20 margintop5	
		$attributes['attributes']['Engine type']	=	trim(str_replace('Fuel:','',strip_tags(trim(@$html ->find('table[class=details fixed marginbott20 margintop5] >td',2)->innertext))));
		
		$attributes['carinfo']['Manufacturer'] = trim(str_replace('Brand:','',strip_tags(trim(@$html ->find('table[class=details fixed marginbott20 margintop5] >td',0)->innertext))));
		//$attributes['carinfo']['Model']	=	trim($html ->find('a[title=Other Brands - Muzaffarabad]',0)->innertext); 							
									//$html-> find('div[class=pding5_10]');
		if($html ->find('div[class=pding5_10]',3)){
			$attributes['attributes']['Mileage'] =	trim(rtrim(trim(str_replace(',','',$html ->find('div[class=pding5_10]',3)->find('strong[class=block]',0)->innertext)),'km'));
		}
		$attributes['attributes']['Description']	=	nl2br(strip_tags(trim($html ->find('p[class=pding10 lheight20 large]',0)->innertext))); 
		$attributes['carinfo']['Description'] =trim($attributes['attributes']['Description']);
	  	$attributes['carinfo']['url'] = $url;	
	  	$attributes['carinfo']['portal_ad_id'] = trim($attributes['attributes']['Ref No']);
	  	$attributes['adinfo']['source'] = 'olx';
	  	$attributes['adinfo']['url'] = $url;

	  	$attributes['contactinfo']['Cell Number'] =trim(str_replace(' ','', trim($html ->find('strong[class=xx-large lheight20 fnormal]',0)->innertext)));
	  	$attributes['contactinfo']['Contact Name'] = trim($html ->find('span[class=block color-5 brkword xx-large]',0)->innertext);
	  	
	  	$i=0;
        $image ='';
        $imageTag= str_replace(' ','-',strtolower($attributes['carinfo']['Title']));
        $images	=	$html ->find('.fleft>a');
        $attributes['carinfo']['images']	=	array();
        foreach($images as $img){
            if(!@strpos($img,$imageTag)){
            	continue;
            }
            $imgurl=str_replace('94x72','1000x700',$img->rel);
            if($i==0){
                 $image  = $imgurl;
            }
            $attributes['carinfo']['images'][]  = $imgurl;
            $i++;
        }
       
        if(sizeof($attributes['carinfo']['images'])>1){
        	unset($attributes['carinfo']['images'][sizeof($attributes['carinfo']['images'])-1]);
    	}
    	
    	$attributes['carinfo']['image'] = $image;
		return $attributes;
}

//dump single url
if(isset($_GET['url'])){
	echo $url=$_GET['url'];
	$attributes	=	getDetails($url);
	print"<pre>";
	print_r($attributes);
}

?>
