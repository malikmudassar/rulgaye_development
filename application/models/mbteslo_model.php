<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/21/2015
 * Time: 9:23 AM
 */

class Mbteslo_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    public function getSubCat($catId)
    {
        $st=$this->db->query('SELECT * from categories WHERE categories.parent='.$catId);
        return $st->result_array();
    }

    public function getItemsInSubCat($catId)
    {
        $st2=$this->db->query('SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\') FROM item_images
                            where item_images.item_id = item.id) my_images, categories.name as category, c.name as sub_category,
                            user.fname, user.lname, cities.name as city_name, states.name as state_name from item
                            inner join categories on categories.id=item.category
                            inner join categories c on c.id=item.sub_category
                            inner join user on user.id=item.user_id
                            inner join cities on cities.id=item.city
                            inner join states on states.id=item.state
                            WHERE (item.sub_category='.$catId.')
                            ORDER BY item.page_views desc');
        return $st2->result();
    }

    public function getMenuCategories()
    {
        $query=$this->db->select('categories.id,categories.name')->from('categories')->where('parent',0)->get();
        if ($query->num_rows() > 0)
        {
            $result =$query->result();
            for($i=0;$i<count($result);$i++)
            {
                $query1=$this->db->select('categories.id,categories.name')->from('categories')->where('parent',$result[$i]->id)->get();
                if($query1->num_rows() > 0)
                {
                    $result[$i]->sub=$query1->result();
                }
                else
                {
                    $result[$i]->sub=array();
                }
            }
            //print_r($result);
            return $result;
        }
        else
        {
            return $query->result();
        }

    }

    public function checkUser($data)
    {
        $st=$this->db->query('SELECT * from user where user.email = \''.$data['email'].'\' AND user.password = \''.sha1(md5($data['password'])).'\' and user.status=1');
        $data=$st->result_array();
        if($st->num_rows()>0){
            return $data[0];
        }
        else
        {
            return false;
        }

    }

    public function addUser($data)
    {
        $user=array(
            'email'=>$data['email'],
            'fname'=>$data['fname'],
            'lname'=>$data['lname'],
            'password'=>sha1(md5($data['password'])),
            'session'=>$this->session->userdata['session_id']
        );

        $check=$this->db->insert('user',$user);
        if($check)
        {
            return true;
        }
        else
        {
            return FALSE;
        }
    }
}