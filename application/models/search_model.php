<?php
/**
 * Created by PhpStorm.
 * User: Malik Mudassar
 * Date: 5/8/15
 * Time: 1:02 PM
 */
class Search_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function search_item($data,$per_page,$offset=0)
    {
        $term_q='';
        $state_q='';
        $cat_q='';
        $min_max_q='';

        if($per_page==0)
        {
            $limit_q='';
        }
        else
        {
            $limit_q='LIMIT '.$offset.','.$per_page;
        }

        if(isset($data['state']))
        {
            if(!empty($data['state'])){
            $state_q=' AND item.state='.$data['state'].' AND item.city='.$data['city'].' ';
        }}

        if(isset($data['price_from'])) {
            if(!empty($data['price_from'])){
            $min_max_q = 'AND item.amount BETWEEN ' . $data['price_from'] . ' AND ' . $data['price_to'];
        }}

        if(isset($data['term'])) {
            if(!empty($data['term'])){
            $term_q = 'AND (item.title LIKE "%' . addslashes($data['term']) . '%" OR item.description LIKE "%' . addslashes($data['term']) . '%")';
        }}

        if(isset($data['category'])) {
            if(!empty($data['category'])){
            $cat_q = 'AND item.category=' . $data['category'] . ' AND item.sub_category=' . $data['sub_category'];
        }}
        $query=$this->db->query("SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR ',') FROM item_images
                                        where item_images.item_id = item.id) my_images, user.fname,user.lname,
                                        categories.name as category, categories.id as cat_id, c.name as sub_category from item
                                        inner join user on user.id=item.user_id
                                        inner join categories on categories.id=item.category
                                        inner join categories c on c.id=item.sub_category
                                        WHERE
                                        item.status=0
                                        {$term_q}
                                        {$cat_q}
                                        {$min_max_q}
                                        {$state_q}
                                        {$limit_q}");
        return $query->result_array();
    }
    // Search item count, using same query

}
