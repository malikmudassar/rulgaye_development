<?php
/**
 * Created by PhpStorm.
 * User: Malik Mudassar
 * Date: 8/22/2015
 * Time: 10:49 AM
 */
class User_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    public function getUsers()
    {
        $st=$this->db->select('user.*')->from('user')->get();
        return $st->result_array();
    }
    public function getUserById($id)
    {
        $st=$this->db->select('user.*')->from('user')->where('id',$id)->get();
        $data=$st->result_array();
        return $data[0];
    }
    public function addUser($data)
    {
        $user=array(
            'email'=>$data['user']['email'],
            'fname'=>$data['user']['fname'],
            'lname'=>$data['user']['lname'],
            'password'=>sha1(md5($data['user']['password'])),
            'session'=>$this->session->userdata['session_id']
        );

        $this->db->insert('user',$user);
        return $this->db->insert_id();
    }
    public function checkUser($data)
    {
        $st=$this->db->query('SELECT * from user where user.email = \''.$data['email'].'\' AND user.password = \''.sha1(md5($data['password'])).'\' and user.status=1');
        $data=$st->result_array();
        if($st->num_rows()>0){
            return $data[0];
        }
        else
        {
            return false;
        }

    }

    public function activateUser($data)
    {
        $st=$this->db->query('UPDATE user set user.status=1 WHERE user.id='.$data['id'].' AND user.session LIKE \''.$data['session'].'\'');
        return true;
    }
    public function getUserProfileById()
    {
        $st=$this->db->query('SELECT user.*,user_attribute.name, user_profile.value from user_profile left join user on user.id=user_profile.user_id
                              left join user_attribute on user_attribute.id=user_profile.attribute_id');
        $data=$st->resilt_array();
        return $data[0];
    }
    public function suspendUserById($id)
    {
        $this->db->query('UPDATE user set user.status=2 WHERE user.id='.$id);
        return true;
    }

    public function getUserTypes()
    {
        $st=$this->db->select('user_types.*')->from('user_types')->get();
        return $st->result_array();
    }
    public function checkEmailExist($email)
    {
        $st=$this->db->query('select * from user where user.email like \''.$email['email'].'\'');
        $data=$st->result_array();
        if($st->num_rows()==0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public function getUserIdByEmail($email)
    {
        $st=$this->db->query('SELECT user.id from user where user.email like \''.$email['email'].'\'');
        $data=$st->result_array();
        return $data[0]['id'];
    }
    public function createPasswordHash($user_id)
    {
        $hash=sha1(md5($this->session->userdata['session_id']));
        $this->db->query('UPDATE user set user.session=\''.$hash.'\' WHERE user.id='.$user_id);
        return $hash;
    }
    public function changePassword($data,$user_id)
    {
        $this->db->query('UPDATE user SET user.password=\''.sha1(md5($data['password'])).'\' WHERE user.id='.$user_id);
        return true;
    }
    public function getActiveAds($user_id)
    {
        $st=$this->db->query('SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\') FROM item_images where item_images.item_id = item.id) my_images, categories.name as category, c.name as sub_category,user.fname, user.lname, cities.name as city_name, states.name as state_name from item
                            inner join categories on categories.id=item.category
                            inner join categories c on c.id=item.sub_category
                            inner join user on user.id=item.user_id
                            inner join cities on cities.id=item.city
                            inner join states on states.id=item.state
                            WHERE item.user_id='.$user_id.'
                            AND item.status=0');
        return $st->result_array();
    }
    public function getInActiveAds($user_id)
    {
        $st=$this->db->query('SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\') FROM item_images where item_images.item_id = item.id) my_images, categories.name as category, c.name as sub_category,user.fname, user.lname, cities.name as city_name, states.name as state_name from item
                            inner join categories on categories.id=item.category
                            inner join categories c on c.id=item.sub_category
                            inner join user on user.id=item.user_id
                            inner join cities on cities.id=item.city
                            inner join states on states.id=item.state
                            WHERE item.user_id='.$user_id.'
                            AND item.status=1');
        return $st->result_array();
    }
    public function save_message($message)
    {
        $data=array(
            'sender' => $message['sender'],
            'receiver' => $message['receiver'],
            'content' => $message['content'],
            'comm_id'=>$message['comm_id']
        );
        $this->db->insert('messages',$data);
        return true;
    }
    public function get_message_contents($comm_id)
    {
        $st=$this->db->query('SELECT CONCAT(user.fname,\' \',user.lname) as sender,user.id as sender_id, messages.content, messages.date_modified as date, communication.subject, communication.id as comm_id
                              from messages
                              inner join user on user.id=messages.sender
                              inner join communication on communication.id=messages.comm_id
                              where messages.comm_id='.$comm_id.' order by date desc');
        return $st->result_array();
    }
    public function create_comm($item_id,$receiver_id,$sender,$data)
    {
        $comm=array(
            'sender'=>$sender,
            'receiver'=>$receiver_id,
            'item_id'=>$item_id,
            'subject'=>$data['subject']
        );
        $this->db->insert('communication',$comm);
        $comm_id=$this->db->insert_id();
        $message=array(
            'comm_id'=>$comm_id,
            'sender'=>$sender,
            'receiver'=>$receiver_id,
            'content'=>$data['content']
        );
        $this->db->insert('messages',$message);
        return true;
    }
    public function getMessages($user_id)
    {
        $st=$this->db->query('SELECT communication.id, CONCAT(user.fname ,\' \',user.lname) sender,
                              CONCAT(u.fname,\' \',u.lname) as receiver, communication.subject, communication.date_created as date, item.title
                              from communication
                              inner join user on user.id=communication.sender
                              inner join user u on u.id=communication.receiver
                              inner join item on item.id=communication.item_id
                              WHERE communication.receiver='.$user_id.' OR communication.sender='.$user_id);
        return $st->result_array();
    }

    public function update_personal_details($data,$user_id)
    {
        $user_details=array(
            'state'=>$data['state'],
            'city'=>$data['city'],
            'mobile'=>$data['mobile'],
            'fullname'=>$data['fullname'],
            'website'=>$data['website'],
            'about'=>$data['about']
        );
        $this->db->where('id',$user_id)->update('user',$user_details);
        return true;
    }

    public function add_user_image($image_name)
    {
        $image=array(
            'image'=>$image_name
        );
        $this->db->where('id',$this->session->userdata['id'])->update('user',$image);
        return true;
    }

    public function get_user_by_id($id)
    {
        $this->db->where('id',$id);
        $st=$this->db->get('user');
        $data=$st->result_array();
        return $data[0];
    }

    public function get_user_items($per_page,$start,$user_id)
    {
        $limit_q='';
        if($per_page!=0)
        {
            $limit_q=' LIMIT '.$start.','.$per_page;
        }

        $st=$this->db->query('SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\') FROM item_images where item_images.item_id = item.id) my_images, categories.name as category,categories.id as cat_id, c.name as sub_category,user.fname, user.lname, cities.name as city_name, states.name as state_name from item
                            inner join categories on categories.id=item.category
                            inner join categories c on c.id=item.sub_category
                            inner join user on user.id=item.user_id
                            inner join cities on cities.id=item.city
                            inner join states on states.id=item.state
                            WHERE item.user_id='.$user_id.'
                            AND item.status=0'.$limit_q);
        return $st->result_array();
    }

    public function get_sellers()
    {
        $st=$this->db->query('SELECT user.*, (SELECT count(*) from item where item.user_id=user.id) as total_items from user
                              inner join item on item.user_id=user.id
                              group by user.id order by total_items desc');
        return $st->result_array();
    }


}

?>