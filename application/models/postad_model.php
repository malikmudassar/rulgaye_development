<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/8/2015
 * Time: 2:27 PM
 */

class Postad_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    public function addItem($data)
    {
        $state_name=$this->getStateById($data['item']['state']);
        $city_name=$this->getCityById($data['item']['state']);
        $addressString=$data['item']['location'].','.$city_name.','.$state_name.', India';
        $coordinates=GetAllLatLong($addressString);
        $lat=$coordinates['Lat'];
        $long=$coordinates['Long'];
        $item=array(
            'user_id'=>$this->session->userdata['id'],
            'title'=>$data['item']['title'],
            'description'=>$data['item']['description'],
            'category'=>$data['item']['category'],
            'sub_category'=>$data['item']['sub_category'],
            'amount'=>$data['item']['amount'],
            'state'=>$data['item']['state'],
            'city'=>$data['item']['city'],
            'location'=>$data['item']['location'],
            'lat'=>$lat,
            'long'=>$long,
            'status'=>1,
            'ip_address'=>$data['ip_address'],
            'browser_agent'=>$data['browser_agent'],
            'host'=>$data['host']
        );
        $this->db->insert('item',$item);
        $item_id=$this->db->insert_id();
        $contact=array(
            'item_id'=>$item_id,
            'contact_name'=>$data['item']['contact_name'],
            'contact_phone'=>$data['item']['contact_phone'],
            'contact_email'=>$data['item']['contact_email']
        );
        $this->db->insert('item_contact_info',$contact);
        return $item_id;
    }
    public function updateItem($data,$item_id)
    {
        $state_name=$this->getStateById($data['item']['state']);
        $city_name=$this->getCityById($data['item']['state']);
        $addressString=$data['item']['location'].','.$city_name.','.$state_name.', India';
        $coordinates=GetAllLatLong($addressString);
        $lat=$coordinates['Lat'];
        $long=$coordinates['Long'];
        $item=array(
            'user_id'=>$this->session->userdata['id'],
            'title'=>$data['item']['title'],
            'description'=>$data['item']['description'],
            'category'=>$data['item']['category'],
            'sub_category'=>$data['item']['sub_category'],
            'amount'=>$data['item']['amount'],
            'state'=>$data['item']['state'],
            'city'=>$data['item']['city'],
            'location'=>$data['item']['location'],
            'lat'=>$lat,
            'long'=>$long
        );
        $this->db->insert('item',$item);
        $this->db->query('UPDATE item set item.title='.$data['item']['title'].',item.description');
        $item_id=$this->db->insert_id();
        $contact=array(
            'item_id'=>$item_id,
            'contact_name'=>$data['item']['contact_name'],
            'contact_phone'=>$data['item']['contact_phone'],
            'contact_email'=>$data['item']['contact_email']
        );
        $this->db->insert('item_contact_info',$contact);
        return $item_id;
    }

    public function addItemImage($item_id,$image)
    {
        $image_data=array(
            'item_id'=>$item_id,
            'name'=>$image['file_name']
        );
        $this->db->insert('item_images',$image_data);
        return true;
    }

    public function getStateById($id)
    {
        $st=$this->db->query('select states.name from states where states.id='.$id);
        $data=$st->result_array();
        return $data[0]['name'];
    }
    public function getCityById($id)
    {
        $st=$this->db->query('select cities.name from cities where cities.id='.$id);
        $data=$st->result_array();
        return $data[0]['name'];
    }
    public function del_Item($item_id)
    {
        $this->db->query('DELETE from item where item.id='.$item_id);
        return true;
    }
}
?>