<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/5/2015
 * Time: 10:47 AM
 */

class Home_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getPopularItems()
    {
        $st=$this->db->query('SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\') FROM item_images
                            where item_images.item_id = item.id) my_images from item
                            WHERE item.status=0
                            ORDER BY item.page_views desc ');
        return $st->result_array();
    }

    public function get_all_items()
    {
        $st=$this->db->query('SELECT * from item');
        return $st->result_array();
    }

    public function getMenuCategories()
    {
        $query=$this->db->select('categories.*')->from('categories')->where('parent',0)->get();
        if ($query->num_rows() > 0)
        {
            $result =$query->result_array();
            for($i=0;$i<count($result);$i++)
            {
                $query1=$this->db->query("select * from categories where parent='".$result[$i]['id']."'");
                if($query1->num_rows() > 0)
                {
                    $result[$i]['sub']=$query1->result_array();
                }
                else
                {
                    $result[$i]['sub']=array();
                }
            }
            //print_r($result);
            return $result;
        }
        else
        {
            return $query->result_array();
        }

    }

    public function getSubCat($id)
    {
        $st=$this->db->select('categories.*')->from('categories')->where('parent',$id)->get();
        return $st->result_array();
    }

    public function getCitiesById($id)
    {
        $st=$this->db->select('cities.*')->from('cities')->where('state',$id)->get();
        return $st->result_array();
    }

   
}


?>