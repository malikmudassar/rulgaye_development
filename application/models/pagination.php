<?php

class Pagination extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }
    public function GetPagination($Total,$Limit,$pageurl,$Page,$SearchPage='')
    {
//        echo "<pre>";
//        print_r(array($Total,$Limit,$pageurl,$Page,$SearchPage));
//        echo "</pre>";
        $pagination = '';
        $start=0;
        $next='';
        $nextPageClass = 'next';
        $previousPageClass = 'previous';
        $last = ceil($Total / $Limit);
        if ($last > 1) {
            if (isset($Page)) {
                $page = $Page;
            }
            if (!(isset($page))) {
                $page = 1;
            }
            if ($page < 1) {
                $page = 1;
            } elseif ($page > $last) {
                $page = $last;
            }

            $prev = $page - 1;
            if($page != $last)
            {
                $next = $page + 1;
            }
            $start = abs(($page - 1) * $Limit);

            $targetpage = $pageurl;

            if($SearchPage!='')
            {
                if($SearchPage=='with_request')
                {
                    $lastpagelink = $targetpage."&page=". $last ;
                    $page1 = $page - 2;
                    $page1link = $targetpage ."&page=". $page1;
                    $page2 = $page - 1;
                    $page2link = $targetpage ."&page=". $page2;
                    $page3 = $page;
                    $page3link = $targetpage ."&page=". $page3;
                    $page4 = $page + 1;
                    $page4link = $targetpage ."&page=". $page4;
                    $page5 = $page + 2;
                    $page5link = $targetpage ."&page=".  $page5;

                    $previouspagelink = $targetpage ."&page=". $prev;

                    if($next!='')
                    {
                        $nextpagelink = $targetpage ."&page=".  $next ;
                    }
                    else
                    {
                        $nextpagelink='';
                    }
                }
                elseif($SearchPage=='dashboard_with_request')
                {
                    $lastpagelink = $targetpage."&page=". $last ;
                    $page1 = $page - 2;
                    $page1link = $targetpage ."&page=". $page1;
                    $page2 = $page - 1;
                    $page2link = $targetpage ."&page=". $page2;
                    $page3 = $page;
                    $page3link = $targetpage ."&page=". $page3;
                    $page4 = $page + 1;
                    $page4link = $targetpage ."&page=". $page4;
                    $page5 = $page + 2;
                    $page5link = $targetpage ."&page=".  $page5;

                    $previouspagelink = $targetpage ."&page=". $prev;
                    if($next!='')
                    {
                        $nextpagelink = $targetpage ."&page=".  $next ;
                    }
                    else
                    {
                        $nextpagelink='';
                    }
                }
                else
                {
                    $lastpagelink = $targetpage."?page=". $last ;
                    $page1 = $page - 2;
                    $page1link = $targetpage ."?page=". $page1;
                    $page2 = $page - 1;
                    $page2link = $targetpage ."?page=". $page2;
                    $page3 = $page;
                    $page3link = $targetpage ."?page=". $page3;
                    $page4 = $page + 1;
                    $page4link = $targetpage ."?page=". $page4;
                    $page5 = $page + 2;
                    $page5link = $targetpage ."?page=".  $page5;

                    $previouspagelink = $targetpage ."?page=". $prev;

                    if($next!='')
                    {
                        $nextpagelink = $targetpage ."?page=".  $next ;
                    }
                    else
                    {
                        $nextpagelink='';
                    }
                }
            }
            else
            {
                $lastpagelink = $targetpage  . $last ;
                $page1 = $page - 2;
                $page1link = $targetpage . $page1;
                $page2 = $page - 1;
                $page2link = $targetpage . $page2;
                $page3 = $page;
                $page3link = $targetpage . $page3;
                $page4 = $page + 1;
                $page4link = $targetpage . $page4;
                $page5 = $page + 2;
                $page5link = $targetpage .  $page5;

                $previouspagelink = $targetpage . $prev;

                if($next!='')
                {
                    $nextpagelink = $targetpage .  $next ;
                }
                else
                {
                    $nextpagelink='';
                }
            }


            if ($page1 < 1 || $page1 > $last) {
                $page1 = '';
                $page1link = "";
            }
            if ($page2 < 1 || $page2 > $last) {
                $page2 = '';
                $page2link = "";
                $previousPageClass = 'c-gray-light';
            }
            if ($page3 < 1 || $page3 > $last) {
                $page3 = '';
                $page3link = "#";
            }
            if ($page4 < 1 || $page4 > $last) {
                $page4 = '';
                $page4link = "";
                $nextPageClass = 'c-gray-light';
            }
            if ($page5 < 1 || $page5 > $last) {
                $page5 = '';
                $page5link = "#";
            }
            $DotDot = '<li><span>...</span></li>';
            if ($last <= 5) {
                $last = '';
                $DotDot = '';
            }

            if ($page1 != '') {
                if($page == $page1)
                {
                    $class='class="active"';
                    $page1Line = '<li '.$class.' ><span>' . $page1 . '</span></li>';
                }
                else
                {
                    $page1Line = '<li><a href="' . $page1link . '">' . $page1 . '</a></li>';
                }

            } else {
                $page1Line = '';
            }
            if ($page2 != '') {
                if($page == $page2)
                {
                    $class='class="active"';
                    $page2Line = '<li '.$class.'><span>' . $page2 . '</span></li>';
                }
                else
                {
                    $class='';
                    $page2Line = '<li><a href="' . $page2link . '">' . $page2 . '</a></li>';
                }

            } else {
                $page2Line = '';
            }
            if ($page3 != '') {
                if($page == $page3)
                {
                    $class='class="active"';
                    $page3Line = '<li '.$class.'><span>' . $page3 . '</span></li>';
                }
                else
                {
                    $class='';
                    $page3Line = '<li><a  href="' . $page3link . '" >' . $page3 . '</a></li>';
                }

            } else {
                $page3Line = '';
            }
            if ($page4 != '') {
                if($page == $page4)
                {
                    $class='class="active"';
                    $page4Line = '<li '.$class.'><span>' . $page4 . '</span></li>';
                }
                else
                {
                    $class='';
                    $page4Line = '<li><a  href="' . $page4link . '">' . $page4 . '</a></li>';
                }

            } else {
                $page4Line = '';
            }
            if ($page5 != '') {
                if($page == $page4)
                {
                    $class='class="active"';
                    $page5Line = '  <li class="active"><span>'. $page5 . '</span></li>';
                }
                else
                {
                    $class='';
                    $page5Line = '<li><a  href="' . $page5link . '">' . $page5 . '</a></li>';
                }

            } else {
                $page5Line = '';
            }
            if ($last != '') {
                //$lastPageLine = '<li><a href="' . $lastpagelink . '">Last' . $last . '</a></li> ';
            }

            if($page==1)
            {
                if($SearchPage!='')
                {
                    $previousPageButton = '';
                }
                else
                {
                    $previousPageButton = '<span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>';
                }

            }
            else
            {
                if($SearchPage!='')
                {
                    $previousPageButton = '<li><a href="' . $previouspagelink . '"><span><i class="fa fa-angle-double-left"></i></span></a></li>';
                }
                else
                {
                    $previousPageButton = '<li><a href="' . $previouspagelink . '"> <span><i class="fa fa-angle-double-left"></i></span></a></li>';
                }
            }


            if ($last != '') {
                if($page==$last)
                {
                    if($SearchPage!='')
                    {
                        if($SearchPage=='with_request')
                        {
                            $nextPageButton = '<li><a><span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span></a></li>';
                        }
                        else
                        {
                            $nextPageButton = ' <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>';
                        }

                    }
                    else
                    {
                        $nextPageButton = ' <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>';
                    }
                }
                else
                {
                    if($SearchPage!='')
                    {
                        if($SearchPage=='with_request')
                        {
                            $nextPageButton = '<span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>';
                        }
                        else
                        {
                            $nextPageButton = ' <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>';
                        }

                    }
                    else
                    {
                        $nextPageButton = ' <li><span><i class="fa fa-angle-right c-gray-light"></i></span></li>';
                    }
                }
            }
            else
            {
                if($SearchPage!='')
                {
                    if($SearchPage=='with_request')
                    {
                        $nextPageButton = '<li><a><span><i class="fa fa-angle-right c-gray-light"></i></span></a></li>';
                    }
                    else
                    {
                        $nextPageButton = ' <li><span><i class="fa fa-angle-right c-gray-light"></i></span></li>';
                    }

                }
                else
                {
                    $nextPageButton = ' <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>';
                }
            }

            if($page==$last)
            {
                if($SearchPage!='')
                {
                    //$nextPageButton = '<span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>';
                    $nextPageButton = '';
                }
                else
                {
                    //$nextPageButton = ' <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>';
                    $nextPageButton = '';
                }
            }
            else
            {
                if($SearchPage!='')
                {
                    $nextPageButton = '<li><a id="nextpagebutton" href="' . $nextpagelink . '"><i class="fa fa-angle-double-right"></i></a></li>';
                }
                else
                {
                    $nextPageButton = '<li><a id="nextpagebutton" href="' . $nextpagelink . '"><i class="fa fa-angle-double-right"></i></a></li>';
                }
            }


            if ($page == $last) {
                $DotDot = '';
                $lastPageLine = '';
            }
            if ($page5 == $last) {
                $DotDot = '';
                $lastPageLine = '';
            }
            if ($page4 == $last) {
                $DotDot = '';
                $lastPageLine = '';
            }

            if($SearchPage=='')
            {
                $pagination = '
                    <ul class="pagination custom-style">
                    ' . $previousPageButton . '
                    ' . $page1Line . '
                    ' . $page2Line . '
                    ' . $page3Line . '
                    ' . $page4Line . '
                    ' . $page5Line . '
                    ' . $DotDot . '
                    ' . $nextPageButton . '
                    </ul>';
                //                ' . @$lastPageLine . '
            }
            else
            {
                if($SearchPage=='dashboard_with_request')
                {
                    $pagination = '
                    <ul class="pagination custom-style">
                    ' . $previousPageButton . '
                    ' . $page1Line . '
                    ' . $page2Line . '
                    ' . $page3Line . '
                    ' . $page4Line . '
                    ' . $page5Line . '
                    ' . $DotDot . '
                    ' . $nextPageButton . '
                    </ul>';
                    //' . @$lastPageLine . '
                }else
                {
                    $pagination = '
                    <ul class="pagination custom-style">
                    ' . $previousPageButton . '
                    ' . $page1Line . '
                    ' . $page2Line . '
                    ' . $page3Line . '
                    ' . $page4Line . '
                    ' . $page5Line . '
                    ' . $DotDot . '
                    ' . $nextPageButton . '
                     </ul>';
                    //' . @$lastPageLine . '
                }
            }

        }

        $result['start'] = $start;
        $result['pagination'] = $pagination;

        return $result;
    }
}

?>