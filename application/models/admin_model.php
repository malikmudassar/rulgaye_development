<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/8/2015
 * Time: 9:37 AM
 */

class Admin_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }
    public function getCategories()
    {
        $query=$this->db->query('SELECT categories.*, category_icon.name as icon from categories left join category_icon on
                                    category_icon.category=categories.id
                                    WHERE
                                    categories.parent=0');
        if ($query->num_rows() > 0)
        {
            $result =$query->result_array();
            for($i=0;$i<count($result);$i++)
            {
                $query1=$this->db->query("SELECT categories.*, category_icon.name as icon from categories left join category_icon on
                                    category_icon.category=categories.id
                                    WHERE
                                    categories.parent='".$result[$i]['id']."'");
                if($query1->num_rows() > 0)
                {
                    $result[$i]['sub']=$query1->result_array();
                }
                else
                {
                    $result[$i]['sub']=array();
                }
            }
            return $result;
        }
        else
        {
            return $query->result_array();
        }
    }
    public function addCategory($data)
    {
        $parent=0;
        if(!empty($data['category']['parent_category']))
        {
            $parent=$data['category']['parent_category'];
        }
        $cat=array(
            'parent'=>$parent,
            'name'=>$data['category']['cat_name']
        );

        $this->db->insert('categories',$cat);
        return true;
    }
    public function assignIcon($data)
    {
        $query=$this->db->query('SELECT * from category_icon WHERE category_icon.category='.$data['category']['category']);
        if($query->num_rows()>0)
        {
            $this->db->query('UPDATE category_icon SET category_icon.name=\''.$data['category']['icon_name'].'\'
                                WHERE category_icon.category='.$data['category']['category']);

        }
        else
        {
            $icon=array(
                'category'=>$data['category']['category'],
                'name'=>$data['category']['icon_name']
            );
            $this->db->insert('category_icon',$icon);
        }
        return true;
    }
    public function getCities()
    {
        $query=$this->db->query('select * from states');
        if ($query->num_rows() > 0)
        {
            $result =$query->result_array();
            for($i=0;$i<count($result);$i++)
            {
                $query1=$this->db->query("select cities.* from cities where cities.state=".$result[$i]['id']);
                if($query1->num_rows() > 0)
                {
                    $result[$i]['sub']=$query1->result_array();
                }
                else
                {
                    $result[$i]['sub']=array();
                }
            }
            return $result;
        }
        else
        {
            return $query->result_array();
        }
    }
    public function addCity($data)
    {
        $query=$this->db->query('SELECT * from cities WHERE cities.state='.$data['city']['state']);
        if($query->num_rows()>0)
        {
            $this->db->query('UPDATE cities SET cities.name=\''.$data['city']['city_name'].'\'
                                WHERE cities.state='.$data['city']['state']);

        }
        else
        {
            $icon=array(
                'category'=>$data['category']['category'],
                'name'=>$data['category']['icon_name']
            );
            $this->db->insert('category_icon',$icon);
        }
        return true;
    }
    public function getUsers()
    {
        $st=$this->db->select('user.*')->from('user')->get();
        return $st->result_array();
    }
    public function getActiveAds($per_page,$offset)
    {

        $limit_q=' LIMIT '.$offset.','.$per_page;

        $st=$this->db->query('SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\') FROM item_images where item_images.item_id = item.id) my_images, CONCAT(user.fname,\' \',user.lname) username,
                            categories.name as subcategory from item
                            inner join user on user.id=item.user_id
                            inner join categories on categories.id=item.sub_category
                            WHERE item.status=0
                            order by item.date desc '.$limit_q);
        return $st->result_array();
    }
    public function countActiveAds()
    {
        $st=$this->db->query('SELECT count(*) as count from item
                            inner join user on user.id=item.user_id
                            inner join categories on categories.id=item.sub_category
                            WHERE item.status=0');
        $data=$st->result_array();
        return $data[0]['count'];
    }
    public function countInActiveAds()
    {
        $st=$this->db->query('SELECT count(*) as count from item
                            inner join user on user.id=item.user_id
                            inner join categories on categories.id=item.sub_category
                            WHERE item.status=1');
        $data=$st->result_array();
        return $data[0]['count'];
    }

    public function getNewAds()
    {
        $st=$this->db->query('SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\') FROM item_images where item_images.item_id = item.id) my_images, CONCAT(user.fname,\' \',user.lname) username,
                            categories.name as subcategory from item
                            inner join user on user.id=item.user_id
                            inner join categories on categories.id=item.sub_category
                            WHERE item.status=1
                            order by item.date desc');
        return $st->result_array();
    }
    public function del_item_by_Id($item_id)
    {
        $this->db->query('UPDATE item SET item.status=3 WHERE item.id='.$item_id);
        return true;
    }

    public function getStaticPageTypes()
    {
        $st=$this->db->query('SELECT * from page_types');
        return $st->result_array();
    }

    public function add_page_data($page_data)
    {
        $page=array(
            'page_type'=>$page_data['page_type'],
            'title'=>$page_data['title'],
            'content'=>$page_data['content']
        );
        $this->db->insert('pages',$page_data);
        return true;
    }

    public function getStaticPages()
    {
        $st=$this->db->query('SELECT pages.id, pages.title, page_types.name as page_type from pages inner join page_types on page_types.id=pages.page_type');
        return $st->result_array();
    }
    public function getPageContentByTypeId($id)
    {
        $st=$this->db->query('SELECT * from pages WHERE pages.page_type='.$id);
        return $st->result_array();
    }
    public function getNewUsers()
    {
        $st=$this->db->query('SELECT * from user WHERE user.status=0 order by user.date desc');
        return $st->result_array();
    }

    public function getActiveUsers()
    {
        $st=$this->db->query('SELECT * from user WHERE user.status=1 order by user.date desc');
        return $st->result_array();
    }

    public function getSuspendedUsers()
    {
        $st=$this->db->query('SELECT * from user WHERE user.status=2');
        return $st->result_array();
    }

    public function approveUser($user_id)
    {
        $this->db->query('UPDATE user SET user.status=1 WHERE user.id='.$user_id);
        return true;
    }
    public function approveItem($item_id)
    {
        $this->db->query('UPDATE item SET item.status=0 WHERE item.id='.$item_id);
        return true;
    }
    public function suspendItem($item_id)
    {
        $this->db->query('UPDATE item SET item.status=2 WHERE item.id='.$item_id);
        return true;
    }
    public function suspendUser($user_id)
    {
        $this->db->query('UPDATE user SET user.status=2 WHERE user.id='.$user_id);
        $this->db->query('UPDATE item set item.status=2 WHERE item.user_id='.$user_id);
        return true;
    }
    public function deleteUser($user_id)
    {
        $this->db->query('DELETE from item WHERE item.user_id='.$user_id);
        $this->db->query('DELETE from user WHERE user.id='.$user_id);
        return true;
    }
    public function deleteItem($item_id)
    {
        $this->db->query('DELETE from item WHERE item.id='.$item_id);
        return true;
    }
    /**
     * Count Totals
     * Code by Malik Mudassar
    */

    public function getTotalAds()
    {
        $st=$this->db->query('SELECT COUNT(*) as total_ads from item where item.status=0');
        $data=$st->result_array();
        return $data[0]['total_ads'];
    }

    public function getTotalUsers()
    {
        $st=$this->db->query('SELECT COUNT(*) as total_users from user WHERE user.status=1');
        $data=$st->result_array();
        return $data[0]['total_users'];
    }

    public function getTotalJobs()
    {
        $st=$this->db->query('SELECT COUNT(*) as total_jobs from item WHERE item.status=0 AND item.category=4');
        $data=$st->result_array();
        return $data[0]['total_jobs'];
    }

    public function getTotalAutos()
    {
        $st=$this->db->query('SELECT COUNT(*) as total_autos from item WHERE item.status=0 AND item.category=1');
        $data=$st->result_array();
        return $data[0]['total_autos'];
    }

    public function getTotalProperty()
    {
        $st=$this->db->query('SELECT COUNT(*) as total_property from item where item.category=3');
        $data=$st->result_array();
        return $data[0]['total_property'];
    }

    public function getTotalClassifieds()
    {
        $st=$this->db->query('SELECT COUNT(*) as total_classifieds from item where item.category=2');
        $data=$st->result_array();
        return $data[0]['total_classifieds'];
    }

}