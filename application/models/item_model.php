<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/10/2015
 * Time: 10:14 AM
 */

class Item_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $item_id
     * @return mixed
     */
    public function getItemDetails($item_id)
    {

        $st=$this->db->query('SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\') FROM item_images where item_images.item_id = item.id) my_images, states.name as state_name, cities.name as city_name, user.fname, user.lname, user.date as joining_date from item
                            inner join states on states.id=item.state
                            inner join cities on cities.id=item.city
                            inner join user on user.id=item.user_id
                            WHERE item.id='.$item_id);
        $data=$st->result_array();
        return $data[0];
    }

    public function getItemById($item_id)
    {
        $st=$this->db->query('SELECT item.*, iti.* from item inner join
                              item_contact_info iti on iti.item_id=item.id
                              WHERE item.id='.$item_id);
        $data=$st->result_array();
        return $data[0];
    }

    /**
     * @param $item_id
     * @return mixed
     */
    public function getSimilarAds($item_id)
    {
        $cat=$this->getCatId($item_id);
        $st2=$this->db->query('SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\') FROM item_images
                            where item_images.item_id = item.id) my_images from item
                            WHERE item.category='.$cat.'
                            AND item.status=0
                            ORDER BY item.page_views desc');
        return $st2->result_array();
    }

    /**
     * @param $item_id
     * @return mixed
     */
    public function getCatId($item_id)
    {
        $st=$this->db->query('select item.category from item where item.id='.$item_id);
        $data1=$st->result_array();
        return $data1[0]['category'];
    }

    /**
     * @param $item_id
     * @return bool
     */
    public function incPageViews($item_id)
    {
        $this->db->query('UPDATE item SET item.page_views=item.page_views+1 where item.id='.$item_id);
        return true;
    }

    /**
     * @param $catId
     * @return mixed
     */
    public function getItemsByCat($per_page,$offset,$catId)
    {
        $limit_q='LIMIT '.$offset.','.$per_page;
        $st2=$this->db->query('SELECT item.*, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\') FROM item_images
                            where item_images.item_id = item.id) my_images, categories.id as cat_id,categories.name as category, c.name as sub_category,
                            user.fname, user.lname, cities.name as city_name, states.name as state_name from item
                            inner join categories on categories.id=item.category
                            inner join categories c on c.id=item.sub_category
                            left join user on user.id=item.user_id
                            inner join cities on cities.id=item.city
                            inner join states on states.id=item.state
                            WHERE (item.category='.$catId.' or item.sub_category='.$catId.' AND item.status=0)
                            ORDER BY item.page_views desc
                            '.$limit_q);
        return $st2->result_array();
    }

    public function get_parent_cat($catId)
    {
        $st=$this->db->query('SELECT categories.parent from categories where categories.id='.$catId);
        $data=$st->result_array();
        return $data[0]['parent'];
    }

    public function count_items_by_cat($catId)
    {
        $st2=$this->db->query('SELECT count(*) as count from item
                            inner join categories on categories.id=item.category
                            inner join categories c on c.id=item.sub_category
                            left join user on user.id=item.user_id
                            inner join cities on cities.id=item.city
                            inner join states on states.id=item.state
                            WHERE (item.category='.$catId.' or item.sub_category='.$catId.' AND item.status=0)
                            ORDER BY item.page_views desc');
        $data=$st2->result_array();
        return $data[0]['count'];
    }

    /**
     * @param $catId
     * @return mixed
     */
    public function getCatName($catId)
    {
        $st=$this->db->query('SELECT categories.name as category, c.name as subcategory from
                              categories left join categories c on c.parent=categories.id
                              where c.id='.$catId);
        $data=$st->result_array();
        return $data[0];
    }

    /**
     * @param $catId
     * @return mixed
     */
    public function getSimilarCategories($catId)
    {
        $query=$this->db->query('SELECT categories.parent from categories where categories.id='.$catId);
        $data=$query->result_array();
        $parentId=$data[0]['parent'];
        if($parentId==0)
        {
            $st=$this->db->query('select a.id,a.name as category,count(item.id) as items
                                  from categories a left join item on a.id=item.category
                                  where a.parent=0 Group by a.name ');
            return $st->result_array();
        }
        else
        {
            $st=$this->db->query('select a.id,a.name as category,count(item.id) as items
                                  from categories a left join item on a.id=item.sub_category
                                  where a.parent='.$parentId.' Group by a.name ');
            return $st->result_array();
        }


    }

    /**
     * @param $item_id
     * @return bool
     */
    public function add_user_favorite($item_id)
    {
        $st=$this->db->query('SELECT * from favorites WHERE favorites.item_id='.$item_id.' AND favorites.user_id='.$this->session->userdata['id']);
        if($st->num_rows()>0)
        {
            return true;
        }
        else
        {
            $fav=array(
                'item_id'=>$item_id,
                'user_id'=>$this->session->userdata['id']
            );
            $this->db->insert('favorites    ',$fav);
            return true;
        }

    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function get_favorites($user_id)
    {
        $st=$this->db->query('SELECT item.id,item.title,item.category as cat_id,item.amount,item.description, (SELECT GROUP_CONCAT(name ORDER BY item_images.id DESC SEPARATOR \',\')
                                FROM item_images where item_images.item_id = item.id) my_images, user.fname,user.lname
                                from favorites
                                inner join item on item.id=favorites.item_id
                                inner join user on user.id=item.user_id
                                WHERE favorites.user_id='.$user_id);
        return $st->result_array();
    }

    /**
     * @param $data
     * @param $item_id
     * @return bool
     */
    public function add_item_rating($data,$item_id)
    {
        $st=$this->db->query('SELECT * from reviews WHERE reviews.item_id='.$item_id.' AND reviews.user_id='.$this->session->userdata['id']);
        if($st->num_rows()>0)
        {
            return true;
        }
        else
        {
            $rating=array(
                'item_id'=>$item_id,
                'user_id'=>$this->session->userdata['id'],
                'rating'=>$data['rating'],
                'content'=>$data['content']
            );
            $this->db->insert('reviews',$rating);
            return true;
        }
    }

    /**
     * @param $item_id
     */
    public function getReviews($item_id)
    {
        $st=$this->db->query('SELECT reviews.*, user.lname, user.fname from reviews
                              inner join user on user.id=reviews.user_id
                              WHERE reviews.item_id='.$item_id);
        return $st->result_array();
    }

    public function deactivate_item($item_id)
    {
        $this->db->query('UPDATE item SET item.status=1 WHERE item.id='.$item_id);
        return true;
    }
    public function activate_item($item_id)
    {
        $this->db->query('UPDATE item SET item.status=0 WHERE item.id='.$item_id);
        return true;
    }

    public function checkFavorite($itemId,$user_id)
    {
        $st=$this->db->query('SELECT * from favorites where favorites.user_id='.$user_id.' AND favorites.item_id='.$itemId);
        if($st->num_rows>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
?>