<?php
/**
 * Created by PhpStorm.
 * User: Mudassar
 * Date: 10/12/2015
 * Time: 4:43 PM
 */

class Contact extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('admin_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        if($_POST)
        {
            $config=array(
                array(
                    'field'=>'name',
                    'label'=>'Name',
                    'rules'=>'trim|required'
                ),
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'trim|required|valid_email'
                ),
                array(
                    'field'=>'subject',
                    'label'=>'Message Subject',
                    'rules'=>'trim|required'
                ),
                array(
                    'field'=>'message',
                    'label'=>'Message Content',
                    'rules'=>'trim|required'
                )
            );
            $this->form_validation->set_rules($config);
            if($this->form_validation->run()==FALSE)
            {
                $data['errors']=validation_errors();

                $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
                $data['menu_cats']=$this->home_model->getMenuCategories();
                $data['categories']=$this->admin_model->getCategories();

                $this->load->view('static/head',$data);
                $this->load->view('static/header',$data);
                $this->load->view('static/contact',$data);
                $this->load->view('static/footer_view',$data);
            }
            else
            {
                $data['success']="Your message has been sent to Rulgaye.in Support team. Our representative will contact your shortly. Thanks";
                $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
                $data['menu_cats']=$this->home_model->getMenuCategories();
                $data['categories']=$this->admin_model->getCategories();
                $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
                $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
                $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
                $this->load->view('static/head',$data);
                $this->load->view('static/header',$data);
                $this->load->view('static/contact',$data);
                $this->load->view('static/footer_view',$data);
            }
        }
        else
        {
            $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
            $data['menu_cats']=$this->home_model->getMenuCategories();
            $data['categories']=$this->admin_model->getCategories();
            $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
            $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
            $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
            $this->load->view('static/head',$data);
            $this->load->view('static/header',$data);
            $this->load->view('static/contact',$data);
            $this->load->view('static/footer_view',$data);
        }

    }
}