<?php
/**
 * Created by PhpStorm.
 * User: Malik Mudassar
 * Date: 9/16/2015
 * Time: 8:49 AM
 */
class User extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
        //$this->load->model('item');
        $this->load->library('My_PHPMailer');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('home_model');
        $this->load->model('admin_model');
        $this->load->model('item_model');
	}
    public function signin(){

        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        $path['path']=$_SERVER['HTTP_REFERER'];
        if(!$this->session->userdata['path'])
        {
            $this->session->set_userdata[$path];
        }
        $this->IsUserLogIn();
        $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
        $data['menu_cats']=$this->home_model->getMenuCategories();

        if($_POST)
        {
            $config = array(
                array(
                    'field'   => 'email',
                    'label'   => 'Email',
                    'rules'   => 'trim|required|valid_email'
                ),
                array(
                    'field'   => 'password',
                    'label'   => 'Password',
                    'rules'   => 'trim|required'
                )
            );

            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE)
            {
                $data['errors']=validation_errors();
                $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
                $this->load->view('static/head',$data);
                $this->load->view('static/header');
                $this->load->view('content/signin',$data);
                $this->load->view('static/footer_view');
            }
            else
            {
                $user=$this->input->post();
                $user_data=$this->user_model->checkUser($user);
                if($user_data)
                {
                    $this->session->set_userdata($user_data);
                    redirect($this->session->userdata['path']);
                }
                else
                {
                    $data['errors']='Sorry! The credentials you have provided are not correct or Your account has not been activated yet';
                    $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';

                    $this->load->view('static/head',$data);
                    $this->load->view('static/header');
                    $this->load->view('content/signin',$data);
                    $this->load->view('static/footer_view');
                }
            }
        }
        else
        {
            $data['title']= 'RulGaye - Signin to Post Free Ads';
            $data['menu_cats']=$this->home_model->getMenuCategories();
            $data['categories']=$this->admin_model->getCategories();
            $data['items']=$this->home_model->getPopularItems();
            $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
            $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
            $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
            $this->load->view('static/head',$data);
            $this->load->view('static/header');
            $this->load->view('content/signin',$data);
            $this->load->view('static/footer_view');
        }

    }
	// facebook login starts
	
	public function login(){

		$this->load->library('facebook'); // Automatically picks appId and secret from config
       

		$user = $this->facebook->getUser();
        
        if ($user) {
            try {
                $data['user_profile'] = $this->facebook->api('/me');
            } catch (FacebookApiException $e) {
                $user = null;
            }
        }else {
            // Solves first time login issue. (Issue: #10)
            //$this->facebook->destroySession();
        }

        if ($user) {

            $data['logout_url'] = site_url('user/logoutFB'); // Logs off application
            // OR 
            // Logs off FB!
            // $data['logout_url'] = $this->facebook->getLogoutUrl();

        } else {
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => base_url(),
                'scope' => array("email") // permissions here
            ));
        }
		$this->load->view('static/head',$data);
		$this->load->view('static/header');
		$this->load->view('content/signin',$data);
		$this->load->view('static/footer_view');
        //$this->load->view('login',$data);

	}

    public function logoutFB(){

        $this->load->library('facebook');

        // Logs off session from website
        $this->facebook->destroySession();
        // Make sure you destory website session as well.

        redirect(base_url());
    }
	
	// facebook login ends
    public function signup(){
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        $this->IsUserLogIn();
        if($_POST)
        {
            $config=array(
                array(
                    'field'=> 'email',
                    'label'=> 'Email Address',
                    'rules'=> 'trim|required|valid_email|is_unique[user.email]'
                ),
                array(
                    'field'=> 'fname',
                    'label'=> 'First Name',
                    'rules'=> 'trim|required'
                ),
                array(
                    'field'=> 'lname',
                    'label'=> 'Last Name',
                    'rules'=> 'trim|required'
                ),
                array(
                    'field'=> 'password',
                    'label'=> 'Password',
                    'rules'=> 'trim|required'
                ),
                array(
                    'field'=> 'conf_password',
                    'label'=> 'Confirm Password',
                    'rules'=> 'trim|required|matches[password]'
                )
            );
            $this->form_validation->set_rules($config);
            if($this->form_validation->run()==FALSE)
            {
                $data['errors']=validation_errors();
                $data['menu_cats']=$this->home_model->getMenuCategories();
                /*echo '<pre>';
                print_r($data);*/
                $data['title']= 'RulGaye - Signup to Post Free Ads ';
                $data['items']=$this->home_model->getPopularItems();
                $this->load->view('static/head',$data);
                $this->load->view('static/header');
                $this->load->view('content/signup',$data);
                $this->load->view('static/footer_view');
            }
            else
            {
                $data['user']=$this->input->post();
                $user_id=$this->user_model->addUser($data);
                if($user_id)
                {
                    $data['menu_cats']=$this->home_model->getMenuCategories();
                    $result=$this->thanksRegister($user_id);
                    if(is_array($result))
                    {
                        print_r($result);
                    }
                    else
                    {
                        $data['menu_cats']=$this->home_model->getMenuCategories();
                        $data['title']= 'RulGaye - Signup to Post Free Ads ';
                        $data['items']=$this->home_model->getPopularItems();
                        $this->load->view('static/head',$data);
                        $this->load->view('static/header');
                        $this->load->view('static/thank_view',$data);
                        $this->load->view('static/footer_view');
                    }

                }
            }
        }
        else
        {
            $data['title']= 'RulGaye - Signup to Post Free Ads ';
            //$data['countries']=$this->user_model->get_countries();
            $data['menu_cats']=$this->home_model->getMenuCategories();
            $data['items']=$this->home_model->getPopularItems();
            $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
            $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
            $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
            /*echo '<pre>';
            print_r($data);*/
            $this->load->view('static/head',$data);
            $this->load->view('static/header');
            $this->load->view('content/signup',$data);
            $this->load->view('static/footer_view');
        }
    }
    public function thanksRegister($user_id)
    {
        // Sending User a URL in the Email to Active his account by verifying his email address
        $data['user_data']=$this->user_model->getUserById($user_id);
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "box3009.bluehost.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "accounts@rulgaye.in";  // user email address
        $mail->Password   = "ParasBilla@123";            // password in GMail
        $mail->SetFrom('accounts@rulgaye.in', 'RulGaye India');  //Who is sending the email
        $mail->AddReplyTo('accounts@rulgaye.in', 'RulGaye India');  //email address that receives the response
        $mail->Subject    = "Congratulations! You have registered with RulGaye India";
        $mail->IsHTML(true);
        $body = $this->load->view('static/thanks_email', $data, true);
        $mail->MsgHTML($body);
        $destination = $data['user_data']['email']; // Who is addressed the email to
        $mail->AddAddress($destination);
        if(!$mail->Send())
        {
            return ($mail->ErrorInfo);
        }
        else
        {
            return true;
        }

    }

    public function logout()
    {
        $newdata = array(
            'uid'   =>'',
            'username'  =>'',
            'email'     => '',
            'logged_in' => FALSE,
        );
        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function IsUserLogIn()
    {
        $user_id=$this->session->userdata('id');
        if($user_id!='')
        {
            redirect(base_url());
        }
    }
    public function activate()
    {
        $data['id']     =  $this->uri->segment(3);
        $data['session']   =  $this->uri->segment(4);
        $st=$this->user_model->activateUser($data);
        if($st)
        {
            $this->confirm_email($data['id']);
        }
    }
    public function confirm_email($id)
    {
        $user=$this->user_model->getUserById($id);
        $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $this->load->view('static/head',$data);
        $this->load->view('static/header');
        $this->load->view('static/confirm_email_view',$user);
        $this->load->view('static/footer_view');
    }
    public function forgetPassword()
    {
        if($_POST)
        {
            $config=array(
                array(
                    'field'=>'email',
                    'label'=>'Email Address',
                    'rules'=>'trim|required|valid_email'
                )
            );
            $this->form_validation->set_rules($config);
            if($this->form_validation->run()==FALSE)
            {

                $data['errors']=validation_errors();
                $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
                //$data['countries']=$this->user_model->get_countries();
                $data['menu_cats']=$this->home_model->getMenuCategories();

                /*echo '<pre>';
                print_r($data);*/
                $this->load->view('static/head',$data);
                $this->load->view('static/header');
                $this->load->view('static/forget_password',$data);
                $this->load->view('static/footer_view');
            }
            else
            {
                $data=$this->input->post();
                if(!$this->user_model->checkEmailExist($data))
                {

                    $data['errors']='Sorry! The mentioned Email address is not registered with us';
                    $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
                    //$data['countries']=$this->user_model->get_countries();
                    $data['menu_cats']=$this->home_model->getMenuCategories();

                    /*echo '<pre>';
                    print_r($data);*/
                    $this->load->view('static/head',$data);
                    $this->load->view('static/header');
                    $this->load->view('static/forget_password',$data);
                    $this->load->view('static/footer_view');
                }
                else
                {
                    $user_id=$this->user_model->getUserIdByEmail($data);
                    $this->user_model->createPasswordHash($user_id);
                    $this->forgetPasswordEmail($user_id);
                    $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
                    //$data['countries']=$this->user_model->get_countries();
                    $data['menu_cats']=$this->home_model->getMenuCategories();
                    $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
                    $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
                    $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
                    $data['title']='Rulgaye India | Password Reset';
                    $this->load->view('static/head',$data);
                    $this->load->view('static/header');
                    $this->load->view('static/frget_pwd_post_email');
                    $this->load->view('static/footer_view');
                }

            }
        }
        else
        {
            $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
            //$data['countries']=$this->user_model->get_countries();
            $data['menu_cats']=$this->home_model->getMenuCategories();
            $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
            $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
            $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
            /*echo '<pre>';
            print_r($data);*/
            $this->load->view('static/head',$data);
            $this->load->view('static/header');
            $this->load->view('static/forget_password',$data);
            $this->load->view('static/footer_view');
        }
    }
    public function isEmailExist($data)
    {
        return $this->user_model->checkEmailExist($data);
    }
    public function newPassword()
    {
        $data['user_id']=$this->uri->segment(3);
        $data['hash']=$this->uri->segment(4);
        if($_POST)
        {
            $config=array(
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'trim|required'
                ),
                array(
                    'field'=>'conf_password',
                    'label'=>'Confirm Password',
                    'rules'=>'trim|required|matches[password]'
                )
            );
            $this->form_validation->set_rules($config);
            if($this->form_validation->run()==FALSE)
            {

                $data['errors']=validation_errors();
                $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
                //$data['countries']=$this->user_model->get_countries();
                $data['menu_cats']=$this->home_model->getMenuCategories();

                /*echo '<pre>';
                print_r($data);*/
                $this->load->view('static/head',$data);
                $this->load->view('static/header');
                $this->load->view('static/forget_password_form');
                $this->load->view('static/footer_view');
            }
            else
            {
                $user_id=$this->uri->segment(3);
                $data=$this->input->post();
                $this->user_model->changePassword($data,$user_id);
                redirect(base_url().'user/signin');

            }
        }
        else
        {
            $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
            //$data['countries']=$this->user_model->get_countries();
            $data['menu_cats']=$this->home_model->getMenuCategories();
            $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
            $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
            $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
            /*echo '<pre>';
            print_r($data);*/
            $this->load->view('static/head',$data);
            $this->load->view('static/header');
            $this->load->view('static/forget_password_form');
            $this->load->view('static/footer_view');
        }
    }
    public function forgetPasswordEmail($user_id)
    {
        // Sending User a URL in the Email to Active his account by verifying his email address
        $data['user_data']=$this->user_model->getUserById($user_id);
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "box3009.bluehost.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "accounts@rulgaye.in";  // user email address
        $mail->Password   = "ParasBilla@123";            // password in GMail
        $mail->SetFrom('accounts@rulgaye.in', 'RulGaye India');  //Who is sending the email
        $mail->AddReplyTo('accounts@rulgaye.in', 'RulGaye India');  //email address that receives the response
        $mail->Subject    = "RulGaye! Password Reset";
        $mail->IsHTML(true);
        $body = $this->load->view('static/forget_password_email_link', $data, true);
        $mail->MsgHTML($body);
        $destination = $data['user_data']['email']; // Who is addressed the email to
        $mail->AddAddress($destination);
        if(!$mail->Send())
        {
            echo 'Email not send';
        }
        return true;
    }
    public function profile()
    {
        $user_id=$this->session->userdata['id'];
        if(empty($user_id))
        {
            redirect(base_url().'user/signin');
        }
        $data['title']='RulGaye - India\'s Free Classified Ad Posting Website ';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['cities'] = $this->admin_model->getCities();
        $data['active_ads']=$this->user_model->getActiveAds($user_id);
        $data['Inactive_ads']=$this->user_model->getInActiveAds($user_id);
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        //$data['messages']=$this->user_model->getMessages($user_id);
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('content/user_profile',$data);
        $this->load->view('static/footer_view');
    }
    public function messages()
    {
        $user_id=$this->session->userdata['id'];
        if(empty($user_id))
        {
            redirect(base_url().'user/signin');
        }
        $data['title']='RulGaye - India\'s Free Classified Ad Posting Website ';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['cities'] = $this->admin_model->getCities();
        $data['messages']=$this->user_model->getMessages($user_id);
        /*echo '<pre>';
        print_r($data['messages']);*/
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        //$data['messages']=$this->user_model->getMessages($user_id);
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('content/user_messages',$data);
        $this->load->view('static/footer_view');
    }
    public function settings()
    {
        $user_id=$this->session->userdata['id'];
        if(empty($user_id))
        {
            redirect(base_url().'user/signin');
        }
        if($_POST)
        {
            $cmd=$_POST['cmd'];
            $user_details=$this->input->post();
            if($cmd==1)
            {
                $this->user_model->update_personal_details($user_details,$user_id);
                if($_FILES)
                {
                    $path = './img/user/';
                    $config['upload_path'] = $path;
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '1000';
                    $config['max_width']  = '350';
                    $config['max_height']  = '350';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload())
                    {
                        $data['error'] = array('error' => $this->upload->display_errors());
                        $data['title']='RulGaye - India\'s Free Classified Ad Posting Website ';
                        $data['user']=$this->user_model->get_user_by_id($this->session->userdata['id']);
                        $data['menu_cats']=$this->home_model->getMenuCategories();
                        $data['categories']=$this->admin_model->getCategories();
                        $data['cities'] = $this->admin_model->getCities();

                        $this->load->view('static/head',$data);
                        $this->load->view('static/header',$data);
                        $this->load->view('content/user_settings',$data);
                        $this->load->view('static/footer_view');
                    }
                    else
                    {
                        $data = array('upload_data' => $this->upload->data());
                        $data['title']='RulGaye - India\'s Free Classified Ad Posting Website ';
                        $this->user_model->add_user_image($data['upload_data']['file_name']);
                        $data['user']=$this->user_model->get_user_by_id($this->session->userdata['id']);
                        $data['menu_cats']=$this->home_model->getMenuCategories();
                        $data['categories']=$this->admin_model->getCategories();
                        $data['cities'] = $this->admin_model->getCities();

                        $this->load->view('static/head',$data);
                        $this->load->view('static/header',$data);
                        $this->load->view('content/user_settings',$data);
                        $this->load->view('static/footer_view');
                    }
                }
            }
            elseif($cmd==2)
            {
                echo '<pre>Change Password';
                print_r($_POST);
            }
            elseif($cmd==3)
            {
                echo '<pre>Social Media';
                print_r($_POST);
            }
        }
        else
        {
            $data['title']='RulGaye - India\'s Free Classified Ad Posting Website ';
            $data['menu_cats']=$this->home_model->getMenuCategories();
            $data['categories']=$this->admin_model->getCategories();
            $data['cities'] = $this->admin_model->getCities();
            $data['user']=$this->user_model->get_user_by_id($this->session->userdata['id']);
            $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
            $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
            $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
            //$data['messages']=$this->user_model->getMessages($user_id);
            $this->load->view('static/head',$data);
            $this->load->view('static/header',$data);
            $this->load->view('content/user_settings',$data);
            $this->load->view('static/footer_view');
        }

    }
    public function public_profile()
    {
        $user_id=$this->uri->segment(3);
        if(empty($user_id))
        {
            redirect(base_url().'user/signin');
        }
        $data['title']='RulGaye - India\'s Free Classified Ad Posting Website ';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['cities'] = $this->admin_model->getCities();
        $data['user']=$this->user_model->get_user_by_id($user_id);
        $data['sellers']=$this->user_model->get_sellers();
        if(!empty($this->session->userdata['id']))
        {
            $data['favorites']=$this->item_model->get_favorites($this->session->userdata['id']);

        }
        $config["base_url"] = base_url() . "user/public_profile/".$user_id.'/';
        $config["total_rows"] = count($this->user_model->get_user_items(0,0,$user_id));

        $config["per_page"] = 6;
        $config["uri_segment"] = 4;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        //$config['prev_link'] = '<span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        //$config['next_link'] = '<span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(4))
        {
            $page = ($this->uri->segment(4));
        }
        else
        {
            $page=0;
        }
        /*echo 'Start ='.$page.'<br> Per Page '.$config['per_page'].'<br>Total Rows '.$config['total_rows'];*/
        $data['user_items']=$this->user_model->get_user_items($config["per_page"],$page,$user_id);
        $data["pagination"] = $this->pagination->create_links();
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('content/user_public_profile',$data);
        $this->load->view('static/footer_view');
    }
    public function deactivate_ad()
    {
        $item_id=$this->uri->segment(3);
        $this->item_model->deactivate_item($item_id);
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function activate_ad()
    {
        $item_id=$this->uri->segment(3);
        $this->item_model->activate_item($item_id);
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function create_communication()
    {
        $item_id=$this->uri->segment(3);
        $receiver_id=$this->uri->segment(4);
        $sender=$this->session->userdata['id'];
        $this->user_model->create_comm($item_id,$receiver_id,$sender,$this->input->post());
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function message_detail()
    {
        $user_id=$this->session->userdata['id'];
        if(empty($user_id))
        {
            redirect(base_url().'user/signin');
        }
        $comm_id=$this->uri->segment(3);
        if($_POST)
        {
            $config=array(
                array(
                    'field'=>'content',
                    'label'=>'Message',
                    'rules'=>'trim|required'
                )
            );
            $this->form_validation->set_rules($config);
            if($this->form_validation->run()==FALSE)
            {
                $data['errors']=validation_errors();
                $data['title']='RulGaye - India\'s Free Classified Ad Posting Website ';
                $data['menu_cats']=$this->home_model->getMenuCategories();
                $data['categories']=$this->admin_model->getCategories();
                $data['cities'] = $this->admin_model->getCities();
                $data['message']=$this->user_model->get_message_contents($comm_id);

                $this->load->view('static/head',$data);
                $this->load->view('static/header',$data);
                $this->load->view('content/user_message_detail',$data);
                $this->load->view('static/footer_view');
            }
            else
            {
                $message=$this->input->post();
                $this->user_model->save_message($message);
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        else
        {
            $data['title']='RulGaye - India\'s Free Classified Ad Posting Website ';
            $data['menu_cats']=$this->home_model->getMenuCategories();
            $data['categories']=$this->admin_model->getCategories();
            $data['cities'] = $this->admin_model->getCities();
            $data['message']=$this->user_model->get_message_contents($comm_id);
            $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
            $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
            $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
            $this->load->view('static/head',$data);
            $this->load->view('static/header',$data);
            $this->load->view('content/user_message_detail',$data);
            $this->load->view('static/footer_view');
        }
    }
}
?>