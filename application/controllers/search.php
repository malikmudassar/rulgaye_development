<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/16/2015
 * Time: 8:49 AM
 */
class Search extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('item_model');
        $this->load->model('admin_model');
        $this->load->model('search_model');
        $this->load->library('pagination');
    }

    public function index()
    {
        $searchData=$_REQUEST;
        /*if($_POST)
        {
            echo '<pre>';
            print_r($_REQUEST);
            echo '</pre>';
        }*/
        $data['items']=$this->search_model->search_item($searchData);
        $data['title']= 'RulGaye - India\'s Free Classified Ad Posting Website';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['cities'] = $this->admin_model->getCities();
        if($_REQUEST['sub_category'])
        {
            $data['similar_categories']=$this->item_model->getSimilarCategories($_REQUEST['sub_category']);
        }
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        /*echo '<pre>';
        print_r($data['items']);*/
        $this->load->view('static/head',$data);
        $this->load->view('static/header');
        $this->load->view('content/search',$data);
        $this->load->view('static/footer_view',$data);
    }
}