<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/8/2015
 * Time: 10:09 AM
 */

class Admin extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->library('form_validation');
        $this->load->library('pagination');
    }

    public function index()
    {
        if(isset($this->session->userdata['user'])=='admin')
        {
            $data['title']='RulGaye - Watch Tower';
            $data['total_ads']=$this->admin_model->getTotalAds();
            $data['total_users']=$this->admin_model->getTotalUsers();
            $data['total_jobs']=$this->admin_model->getTotalJobs();
            $data['total_autos']=$this->admin_model->getTotalAutos();
            $data['total_property']=$this->admin_model->getTotalProperty();
            $data['total_classifieds']=$this->admin_model->getTotalClassifieds();

            $this->load->view('admin/static/admin_head',$data);
            $this->load->view('admin/content/index');
            $this->load->view('admin/static/admin_footer');
        }
        else
        {
            redirect(base_url().'admin/login');
        }
        //$this->load->view('admin/categories',$data);
    }

    public function login()
    {
        if($this->session->userdata['user']=='admin')
        {
            redirect(base_url().'admin');
        }
            if ($_POST) {
                if ($_POST['name'] = 'tesloadmin@rulgaye.in' && $_POST['password'] == 'darjeeling@india123') {
                    $data['user'] = 'admin';
                    $this->session->set_userdata($data);
                    redirect(base_url() . 'admin');
                } else {
                    $data['errors'] = 'Wrong Credentials, Try Again!';
                    $data['title'] = 'RulGaye - Watch Tower';
                    $this->load->view('admin/static/admin_head',$data);
                    $this->load->view('admin/admin_login');
                }
            } else {
                $data['title'] = 'RulGaye - Watch Tower';
                $this->load->view('admin/static/admin_head',$data);
                $this->load->view('admin/admin_login');
            }

    }

    public function logout()
    {
        $data['user'] = '';
        $this->session->set_userdata($data);
        session_destroy();
        redirect(base_url().'admin/login');
    }

    public function categories()
    {
        if($this->session->userdata['user']=='admin')
        {
        $data['categories']=$this->admin_model->getCategories();
        if($_POST)
        {
            if($_POST['icon_form']==1)
            {
                $config=array(
                    array(
                        'field'=>'category',
                        'label'=>'Category Name',
                        'rules'=>'trim|required'
                    ),
                    array(
                        'field'=>'icon_name',
                        'label'=>'Icon Name',
                        'rules'=>'trim|required'
                    )
                );
                $this->form_validation->set_rules($config);
                if($this->form_validation->run()==FALSE)
                {
                    $data['errors_icon']=validation_errors();
                    $data['categories']=$this->admin_model->getCategories();
                    $this->load->view('static/head');
                    $this->load->view('admin/admin_header');
                    $this->load->view('admin/categories',$data);
                }
                else
                {
                    $data['category']=$this->input->post();
                    $this->admin_model->assignIcon($data);
                    $data['categories']=$this->admin_model->getCategories();
                    $this->load->view('static/head');
                    $this->load->view('admin/admin_header');
                    $this->load->view('admin/categories',$data);
                }
            }
            else
            {
                $config=array(
                    array(
                        'field'=>'cat_name',
                        'label'=>'Category Name',
                        'rules'=>'trim|required'
                    )
                );
                $this->form_validation->set_rules($config);
                if($this->form_validation->run()==FALSE)
                {
                    $data['errors']=validation_errors();
                    $data['categories']=$this->admin_model->getCategories();
                    $this->load->view('static/head');
                    $this->load->view('admin/admin_header');
                    $this->load->view('admin/categories',$data);
                }
                else
                {
                    $data['category']=$this->input->post();

                    $this->admin_model->addCategory($data);
                    $data['categories']=$this->admin_model->getCategories();
                    $this->load->view('static/head');
                    $this->load->view('admin/admin_header');
                    $this->load->view('admin/categories',$data);
                }
            }


            }
            else
            {
                $this->load->view('static/head');
                $this->load->view('admin/admin_header');
                $this->load->view('admin/categories',$data);
            }
        }
        else
        {
            redirect(base_url().'admin/login');
        }
    }

    public function users()
    {
        if($this->session->userdata['user']=='admin')
        {
            $data['title']='RulGaye - Watch Tower';
            $data['user']=$this->admin_model->getUsers();
            $this->load->view('static/head',$data);
            $this->load->view('admin/admin_header');
            $this->load->view('admin/list_users',$data);
        }
        else
        {
            redirect(base_url().'admin/login');
        }
    }

    public function cities()
    {
        if($this->session->userdata['user']=='admin')
        {
            if($_POST)
            {
                $config=array(
                    array(
                        'field'=>'state',
                        'label'=>'State',
                        'rules'=>'trim|required'
                    ),
                    array(
                        'field'=>'city_name',
                        'label'=>'City Name',
                        'rules'=>'trim|required'
                    )
                );
                $this->form_validation->set_rules($config);
                if($this->form_validation->run()==FALSE)
                {
                    $data['errors']=validation_errors();
                    $data['cities']=$this->admin_model->getCities();
                    $this->load->view('static/head');
                    $this->load->view('admin/admin_header');
                    $this->load->view('admin/cities',$data);
                }
                else
                {
                    $data['city']=$this->input->post();
                    $this->admin_model->addCity($data);
                    $data['cities']=$this->admin_model->getCities();
                    $this->load->view('static/head');
                    $this->load->view('admin/admin_header');
                    $this->load->view('admin/cities',$data);
                }
            }
            else
            {
                $data['cities']=$this->admin_model->getCities();
                $this->load->view('static/head');
                $this->load->view('admin/admin_header');
                $this->load->view('admin/cities',$data);
            }
        }
        else
        {
            redirect(base_url().'admin/login');
        }
    }

    public function del_item()
    {
        $item_id=$this->uri->segment(3);
        $this->admin_model->del_item_by_Id($item_id);
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function web_configs()
    {
        $data['title']='RulGaye - Watch Tower';
        //$data['jobs']=$this->admin_model->getAllAds();

        $this->load->view('static/head',$data);
        $this->load->view('admin/admin_header');
        //$this->load->view('admin/list_jobs',$data);
    }
    public function jobs()
    {
        $data['title']='RulGaye - Watch Tower';
        $data['jobs']=$this->admin_model->getAllAds();

        $this->load->view('static/head',$data);
        $this->load->view('admin/admin_header');
        //$this->load->view('admin/list_jobs',$data);
    }

    public function add_jobs()
    {
        $data['title']='RulGaye - Watch Tower';

        $this->load->view('static/head',$data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/add_job',$data);
    }

    public function edit_jobs()
    {
        $id=$this->uri->segment(3);
        $data['title']='RulGaye - Watch Tower';
        $data['job']=$this->admin_model->getJobDetailsById($id);
        $this->load->view('static/head',$data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/edit_job',$data);
    }

    public function del_job()
    {
        redirect(base_url().'admin/jobs');
    }

    public function static_pages()
    {
        $data['title']='RulGaye - Watch Tower';
        $data['pages']=$this->admin_model->getStaticPages();
        $this->load->view('static/head',$data);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/static_pages',$data);
    }
    public function add_page()
    {
        if($_POST)
        {
            $config=array(
                array(
                    'field'=>'page_type',
                    'label'=>'Page Type',
                    'rules'=>'trim|required'
                ),
                array(
                    'field'=>'title',
                    'label'=>'Page Title',
                    'rules'=>'trim|required'
                ),
                array(
                    'field'=>'content',
                    'label'=>'Page Contents',
                    'rules'=>'trim|required'
                )
            );
            $this->form_validation->set_rules($config);
            if($this->form_validation->run()==FALSE)
            {
                $data['errors']=validation_errors();
                $data['title']='RulGaye - Watch Tower';
                $data['page_types']=$this->admin_model->getStaticPageTypes();
                $this->load->view('static/head',$data);
                $this->load->view('admin/admin_header');
                $this->load->view('admin/add_static_pages',$data);
            }
            else
            {
                $page_data=$this->input->post();
                /*echo '<pre>';
                print_r($page_data);*/
                $this->admin_model->add_page_data($page_data);
                redirect(base_url().'admin/static_pages');
            }
        }
        else
        {
            $data['title']='RulGaye - Watch Tower';
            $data['page_types']=$this->admin_model->getStaticPageTypes();
            $this->load->view('static/head',$data);
            $this->load->view('admin/admin_header');
            $this->load->view('admin/add_static_pages',$data);
        }
    }
    public function newUsers()
    {
        $data['title']='RulGaye - Watch Tower';
        $data['total_ads']=$this->admin_model->getTotalAds();
        $data['total_users']=$this->admin_model->getTotalUsers();
        $data['total_jobs']=$this->admin_model->getTotalJobs();
        $data['total_autos']=$this->admin_model->getTotalAutos();
        $data['total_property']=$this->admin_model->getTotalProperty();
        $data['total_classifieds']=$this->admin_model->getTotalClassifieds();
        $data['users']=$this->admin_model->getNewUsers();
        $this->load->view('admin/static/admin_head',$data);
        $this->load->view('admin/content/newUsers',$data);
        $this->load->view('admin/static/admin_footer');
    }
    public function activeUsers()
    {
        $data['title']='RulGaye - Watch Tower';
        $data['total_ads']=$this->admin_model->getTotalAds();
        $data['total_users']=$this->admin_model->getTotalUsers();
        $data['total_jobs']=$this->admin_model->getTotalJobs();
        $data['total_autos']=$this->admin_model->getTotalAutos();
        $data['total_property']=$this->admin_model->getTotalProperty();
        $data['total_classifieds']=$this->admin_model->getTotalClassifieds();
        $data['users']=$this->admin_model->getActiveUsers();
        $this->load->view('admin/static/admin_head',$data);
        $this->load->view('admin/content/activeUsers',$data);
        $this->load->view('admin/static/admin_footer');
    }
    public function suspendedUsers()
    {
        $data['title']='RulGaye - Watch Tower';
        $data['total_ads']=$this->admin_model->getTotalAds();
        $data['total_users']=$this->admin_model->getTotalUsers();
        $data['total_jobs']=$this->admin_model->getTotalJobs();
        $data['total_autos']=$this->admin_model->getTotalAutos();
        $data['total_property']=$this->admin_model->getTotalProperty();
        $data['total_classifieds']=$this->admin_model->getTotalClassifieds();
        $data['users']=$this->admin_model->getSuspendedUsers();
        $this->load->view('admin/static/admin_head',$data);
        $this->load->view('admin/content/suspendedUsers',$data);
        $this->load->view('admin/static/admin_footer');
    }
    public function approveUser()
    {
        $user_id=$this->uri->segment(3);
        $this->admin_model->approveUser($user_id);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function suspendUser()
    {
        $user_id=$this->uri->segment(3);
        $this->admin_model->suspendUser($user_id);
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function suspendItem()
    {
        $item_id=$this->uri->segment(3);
        $this->admin_model->suspendItem($item_id);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function deleteUser()
    {
        $user_id=$this->uri->segment(3);
        $this->admin_model->deleteUser($user_id);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function active_products($offset=0)
    {
        if($this->session->userdata['user']=='admin')
        {
            $data['title']='RulGaye - Watch Tower';
            $data['total_ads']=$this->admin_model->getTotalAds();
            $data['total_users']=$this->admin_model->getTotalUsers();
            $data['total_jobs']=$this->admin_model->getTotalJobs();
            $data['total_autos']=$this->admin_model->getTotalAutos();
            $data['total_property']=$this->admin_model->getTotalProperty();
            $data['total_classifieds']=$this->admin_model->getTotalClassifieds();

            $config['base_url'] = base_url().'admin/active_products';
            $config['total_rows']=($this->admin_model->countActiveAds());
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;

            $config["num_links"] = 5;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            //$config['prev_link'] = '<span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            //$config['next_link'] = '<span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $this->pagination->initialize($config);

            $data['products']=$this->admin_model->getActiveAds($config['per_page'],$offset);
            $data['pagination']=$this->pagination->create_links();

            /*echo '<pre>';
            print_r($data['pagination']);*/
            $this->load->view('admin/static/admin_head',$data);
            $this->load->view('admin/content/list_products',$data);
            $this->load->view('admin/static/admin_footer');
        }
        else
        {
            redirect(base_url().'admin/login');
        }
    }
    public function new_products()
    {
        if($this->session->userdata['user']=='admin')
        {
            $data['title']='RulGaye - Watch Tower';
            $data['total_ads']=$this->admin_model->getTotalAds();
            $data['total_users']=$this->admin_model->getTotalUsers();
            $data['total_jobs']=$this->admin_model->getTotalJobs();
            $data['total_autos']=$this->admin_model->getTotalAutos();
            $data['total_property']=$this->admin_model->getTotalProperty();
            $data['total_classifieds']=$this->admin_model->getTotalClassifieds();
            $data['products']=$this->admin_model->getNewAds();

            $this->load->view('admin/static/admin_head',$data);
            $this->load->view('admin/content/list_products',$data);
            $this->load->view('admin/static/admin_footer');
        }
        else
        {
            redirect(base_url().'admin/login');
        }
    }
    public function approveItem()
    {
        $item_id=$this->uri->segment(3);
        $this->admin_model->approveItem($item_id);
        redirect($_SERVER['HTTP_REFERER']);
    }
    public function deleteItem()
    {
        $item_id=$this->uri->segment(3);
        $this->admin_model->deleteItem($item_id);
        redirect($_SERVER['HTTP_REFERER']);
    }

}