<?php
/**
 * Created by PhpStorm.
 * User: Malik Mudassar
 * Date: 9/21/2015
 * Time: 9:18 AM
 */
class Mbteslo extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('mbteslo_model');
        $this->load->model('search_model');
        $this->load->model('home_model');
    }

    public function getSubCategories()
    {
        $cat_id=$this->uri->segment(3);
        $data['sub_categories']=($this->mbteslo_model->getSubCat($cat_id));
        print_r(json_encode($data));
        return $data;
    }
    public function getAllCategories()
    {
        $data['categories']=$this->mbteslo_model->getMenuCategories();
        print_r(json_encode($data['categories']));
        return $data;
    }
    public function getItems()
    {
        $cat_id=$this->uri->segment(3);
        $data=json_encode($this->mbteslo_model->getItemsInSubCat($cat_id));
        print_r($data);
        return $data;
    }
    public function search_items()
    {

        $searchData=$_REQUEST;
        $data['items']=$this->search_model->search_item($searchData);
        print_r(json_encode($data));
    }
    public function signIn()
    {
        $userData=$_REQUEST;
        $check=$this->mbteslo_model->checkUser($userData);
        if($check)
        {
            $data['status']='OK';
        }
        else
        {
            $data['status']='FAILED';
        }
        print_r(json_encode($data));
        return json_encode($data);

    }
    public function signUp()
    {
        $userData=$_REQUEST;
        $check=$this->mbteslo_model->addUser($userData);
        if($check)
        {
            $data['status']='OK';
        }
        else
        {
            $data['status']='FAILED';
        }
        print_r(json_encode($data));
        return json_encode($data);

    }



}