<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Malik Mudassar
 * Date: 4/7/15
 * Time: 5:48 PM
 */


class Login extends CI_Controller {
    public $user = "";
    public function __construct(){

        parent::__construct();
        parse_str($_SERVER['QUERY_STRING'],$_REQUEST);
        $this->load->library('Facebook', array("appId"=>"698675913599902", "secret"=>"d67cbd8ef33f998a25e85baef94de4d9"));
        $this->user = $this->facebook->getUser();
        $this->load->model('user_model');

    }

    public function index() {
        if ($this->user) {
            $data['user_profile'] = $this->facebook->api('/me/');
            $data['logout_url'] = $this->facebook->getLogoutUrl(array('next' => base_url() . 'index.php/oauth_login/logout'));
            //$this->load->view('profile', $data);
            $user['id']=$data['user_profile']['id'];
            $user['fname']=$data['user_profile']['name'];
            $user['lname']='';
            $this->session->set_userdata($user);
            redirect(base_url().'home');
        } else {

            $data['login_url'] = $this->facebook->getLoginUrl();
            redirect($this->facebook->getLoginUrl());
        }
    }
    public function logout() {
        session_destroy();
        redirect(base_url().'user');
    }
    public function facebook_confirm($user)
    {
        //echo $user['first_name'].' '.$user['last_name'].' '.$user['email'];
        $data['title']= 'Thanks for Registration through Facebook';
        $user_details=array(
            'fname'=>$user['first_name'],
            'lname'=>$user['last_name'],
            'email'=>$user['email'],
            'status'=>'Active'
        );
        $data['user_id']=$this->user_model->insert_user_fb($user_details);
        $this->load->view('head',$data);
        $this->load->view('header');
        $this->load->view('fb_ch_pass', $data);
        $this->load->view('footer_view');
    }
    public function fb_confirm()
    {
        $data['title']= 'Email Address Already Exists';
        $msg=$this->uri->segment(3);
        if($msg=='ae')
        {
            $this->load->view('head',$data);
            $this->load->view('header');
            $this->load->view('fb_confirm_ae');
            $this->load->view('footer_view');
        }
    }
}
?>