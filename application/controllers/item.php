<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/10/2015
 * Time: 10:14 AM
 */

class Item extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('admin_model');
        $this->load->model('item_model');
        $this->load->model('user_model');
        $this->load->library('googlemaps');
        $this->load->library('pagination');
    }
    public function itemDetail()
    {
        $itemId=$this->uri->segment(3);
        if(empty($itemId))
        {
            redirect($_SERVER[HTTP_REFERER]);
            exit;
        }
        $this->item_model->incPageViews($itemId);

        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['item']=$this->item_model->getItemDetails($itemId);
        $data['title']= 'RulGaye -'.$data['item']['title'];
        $data['user']=$this->user_model->get_user_by_id($data['item']['user_id']);

        if(!empty($this->session->userdata['id']))
        {
            $data['favorites']=$this->item_model->get_favorites($this->session->userdata['id']);
            $data['is_favorite']=$this->item_model->checkFavorite($itemId,$this->session->userdata['id']);
        }

        $data['ads']=$this->item_model->getSimilarAds($itemId);
        $data['reviews']=$this->item_model->getReviews($itemId);
        $data['meta_title']=$data['item']['title'];
        $data['meta_description']=$data['item']['description'];
        $data['meta_keywords']=$data['item']['title'];//'Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header');
        $this->load->view('content/item_detail',$data);
        $this->load->view('static/footer_view',$data);
    }
    public function catListing()
    {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

        $catId=$this->uri->segment(3);
        if(($this->uri->segment(4)))
        {
            $offset=$this->uri->segment(4);
        }
        else
        {
            $offset=0;
        }


        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['cat_name']=$this->item_model->getCatName($catId);
        $parentId=$this->item_model->get_parent_cat($catId);
        if($parentId==4)
        {
            $data['title']= 'RulGaye - Apply-for-'.$data['cat_name']['subcategory'].'-Jobs-online';
        }
        else
        {
            $data['title']= 'RulGaye - Buy-'.$data['cat_name']['subcategory'].'-online';
        }
        $data['similar_categories']=$this->item_model->getSimilarCategories($catId);
        $data['cities'] = $this->admin_model->getCities();

        $config['base_url'] = base_url().'item/catListing/'.$catId.'/';
        $config['total_rows'] = $this->item_model->count_items_by_cat($catId);

        $config["per_page"] = 10;
        $config["uri_segment"] = 4;

        $config["num_links"] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        //$config['prev_link'] = '<span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        //$config['next_link'] = '<span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(4))
        {
            $page = ($this->uri->segment(4));
        }
        else
        {
            $page=0;
        }

        $data['items']=$this->item_model->getItemsByCat($config['per_page'],$page,$catId);
        $data['pagination']=$this->pagination->create_links();
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header');
        $this->load->view('content/category_items_list',$data);
        $this->load->view('static/footer_view',$data);
    }
    public function addtofavorite()
    {
        if(isset($this->session->userdata['id']))
        {
            $item_id=$this->uri->segment(3);
            $this->item_model->add_user_favorite($item_id);
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    public function add_rating()
    {
        $item_id=$this->uri->segment(3);
        $rating=$this->input->post();

        $this->item_model->add_item_rating($rating,$item_id);
        redirect($_SERVER['HTTP_REFERER']);
    }
}