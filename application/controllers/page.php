<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 10/9/2015
 * Time: 4:06 PM
 */
class Page extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('admin_model');
    }
    public function careers()
    {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        //error_reporting(E_ALL);
        $data['title']= 'RulGaye - Careers';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['page']=$this->admin_model->getPageContentByTypeId(1);
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('static/careers',$data);
        $this->load->view('static/footer_view',$data);
    }

    public function terms_conditions()
    {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        //error_reporting(E_ALL);
        $data['title']= 'RulGaye - Terms & Conditions';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['page']=$this->admin_model->getPageContentByTypeId(2);
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('static/terms',$data);
        $this->load->view('static/footer_view',$data);
    }
    public function privacy_policy()
    {
        header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        //error_reporting(E_ALL);
        $data['title']= 'RulGaye - Privacy Policy';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['page']=$this->admin_model->getPageContentByTypeId(3);
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('static/privacy_policy',$data);
        $this->load->view('static/footer_view',$data);
    }
    public function affiliates()
    {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        //error_reporting(E_ALL);
        $data['title']= 'RulGaye - Affiliates';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('static/affiliates',$data);
        $this->load->view('static/footer_view',$data);
    }

    public function faq()
    {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        //error_reporting(E_ALL);
        $data['title']= 'RulGaye - Frequently Asked Questions';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['page']=$this->admin_model->getPageContentByTypeId(6);
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('static/faq',$data);
        $this->load->view('static/footer_view',$data);
    }

    public function return_policy()
    {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        //error_reporting(E_ALL);
        $data['title']= 'RulGaye - Return Policy';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        //$this->load->view('content/return_policy',$data);
        $this->load->view('static/footer_view',$data);
    }
    public function support()
    {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        //error_reporting(E_ALL);
        $data['title']= 'RulGaye - Support';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('static/support',$data);
        $this->load->view('static/footer_view',$data);
    }
    public function feedback()
    {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        //error_reporting(E_ALL);
        $data['title']= 'RulGaye - Feedback';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('static/feedback',$data);
        $this->load->view('static/footer_view',$data);
    }
    public function site_map()
    {
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        //error_reporting(E_ALL);
        $data['title']= 'RulGaye - Site map';
        $data['menu_cats']=$this->home_model->getMenuCategories();
        $data['categories']=$this->admin_model->getCategories();
        $data['page']=$this->admin_model->getPageContentByTypeId(7);
        $data['meta_title']="Find Jobs in India, Buy Property, Sell Your car in India";
        $data['meta_description']='RulGaye.In is one of the leading Free Classifieds Ad Posting Website. Do you want to find a place to stay, find suitable house to buy, find an apartment to lease, find a dream job in India, buy automobiles in India online,';
        $data['meta_keywords']='Find Jobs in India, Find Property in India, Buy Mobiles Online, Sell Car in India, Buy Used Cars in India';
        $this->load->view('static/head',$data);
        $this->load->view('static/header',$data);
        $this->load->view('static/site_map',$data);
        $this->load->view('static/footer_view',$data);
    }
}