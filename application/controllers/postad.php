<?php
/**
 * Created by PhpStorm.
 * User: Zile Farooq
 * Date: 9/8/2015
 * Time: 2:21 PM
 */
class Postad extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('postad_model');
        $this->load->model('admin_model');
        $this->load->model('item_model');
        $this->load->library('form_validation');
        //$this->load->library('MY_Upload');
    }

    public function index()
    {
        if(isset($this->session->userdata['id']))
        {
            if($_POST)
            {
                $config=array(
                    array(
                        'field'=> 'category',
                        'label'=> 'Category',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'sub_category',
                        'label'=> 'Sub Category',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'title',
                        'label'=> 'Title',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'amount',
                        'label'=> 'Amount',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'description',
                        'label'=> 'Description of Ad',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'state',
                        'label'=> 'Indian State',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'city',
                        'label'=> 'City in State',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'contact_phone',
                        'label'=> 'Contact Phone number',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'contact_name',
                        'label'=> 'Contact\'s Name',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'contact_email',
                        'label'=> 'Contact Email Address',
                        'rules'=> 'trim|required|valid_email'
                    )
                );
                $this->form_validation->set_rules($config);
                if($this->form_validation->run()==FALSE)
                {
                    $data['errors']=validation_errors();
                    $data['menu_cats'] = $this->home_model->getMenuCategories();
                    $data['title'] = 'RulGaye - India\'s Free Classified Ad Posting Website';
                    $data['cities'] = $this->admin_model->getCities();
                    $data['items']=$this->home_model->getPopularItems();

                    $this->load->view('static/head', $data);
                    $this->load->view('static/header');
                    $this->load->view('content/postad', $data);
                    $this->load->view('static/footer_view');
                }
                else
                {
                    $data['item']=$this->input->post();
                    $data['ip_address']=$_SERVER['REMOTE_ADDR'];
                    $data['browser_agent']=$_SERVER['HTTP_USER_AGENT'];
                    $data['host']=$_SERVER['HTTP_HOST'];
                    $item_id=$this->postad_model->addItem($data);
                    /*
                    * This section Uploads Multiple Files uploaded from the view.
                     * If multiple files are not found, only single file will be uploaded
                     * i.e. $this->upload->do_upload() will run
                    */
                    $config['upload_path'] = './img/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '200';
                    $config['max_width'] = '1024';
                    $config['max_height'] = '768';

                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if(!$this->upload->do_multi_upload("file"))
                    {
                        print_r($this->upload->display_errors());
                    }
                    else
                    {
                        $images=($this->upload->get_multi_upload_data());
                        for($i=0;$i<count($images);$i++)
                        {
                            $this->postad_model->addItemImage($item_id,$images[$i]);
                        }
                        redirect(base_url().'home?pa=true');
                    }
                }
            }
            else
            {

                $data['menu_cats'] = $this->home_model->getMenuCategories();
                $data['title'] = 'RulGaye - India\'s Free Classified Ad Posting Website';
                $data['cities'] = $this->admin_model->getCities();
                $data['items']=$this->home_model->getPopularItems();

                $this->load->view('static/head', $data);
                $this->load->view('static/header');
                $this->load->view('content/postad', $data);
                $this->load->view('static/footer_view');
            }
        }
        else
        {
            redirect(base_url().'user/signin');
        }

    }
    public function edit()
    {
        $item_id=$this->uri->segment(3);
        if(isset($this->session->userdata['id']))
        {
            if($_POST)
            {
                $config=array(
                    array(
                        'field'=> 'category',
                        'label'=> 'Category',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'sub_category',
                        'label'=> 'Sub Category',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'title',
                        'label'=> 'Title',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'amount',
                        'label'=> 'Amount',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'description',
                        'label'=> 'Description of Ad',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'state',
                        'label'=> 'Indian State',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'city',
                        'label'=> 'City in State',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'contact_phone',
                        'label'=> 'Contact Phone number',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'contact_name',
                        'label'=> 'Contact\'s Name',
                        'rules'=> 'trim|required'
                    ),
                    array(
                        'field'=> 'contact_email',
                        'label'=> 'Contact Email Address',
                        'rules'=> 'trim|required|valid_email'
                    ),
                    array(
                        'field'=> 'term',
                        'label'=> 'Accept terms & Condition Checkbox',
                        'rules'=> 'required'
                    )
                );
                $this->form_validation->set_rules($config);
                if($this->form_validation->run()==FALSE)
                {
                    $data['errors']=validation_errors();
                    $data['menu_cats'] = $this->home_model->getMenuCategories();
                    $data['title'] = 'RulGaye - India\'s Free Classified Ad Posting Website';
                    $data['cities'] = $this->admin_model->getCities();
                    $data['item']=$this->item_model->getItemById($item_id);
                    $data['items']=$this->home_model->getPopularItems();

                    $this->load->view('static/head', $data);
                    $this->load->view('static/header');
                    $this->load->view('content/edit_postad', $data);
                    $this->load->view('static/footer_view');
                }
                else
                {
                    $data['item']=$this->input->post();
                    $this->postad_model->updateItem($data,$item_id);

                    /*
                    * This section Uploads Multiple Files uploaded from the view.
                     * If multiple files are not found, only single file will be uploaded
                     * i.e. $this->upload->do_upload() will run
                    */

                    $config['upload_path'] = './img/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '200';
                    $config['max_width'] = '1024';
                    $config['max_height'] = '768';

                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if(!$this->upload->do_multi_upload("file"))
                    {
                        print_r($this->upload->display_errors());
                    }
                    else
                    {
                        $images=($this->upload->get_multi_upload_data());
                        for($i=0;$i<count($images);$i++)
                        {
                            $this->postad_model->addItemImage($item_id,$images[$i]);
                        }
                        redirect(base_url().'home');
                    }
                }
            }
            else
            {

                $data['menu_cats'] = $this->home_model->getMenuCategories();
                $data['title'] = 'RulGaye - India\'s Free Classified Ad Posting Website';
                $data['cities'] = $this->admin_model->getCities();
                $data['item']=$this->item_model->getItemById($item_id);
                $data['items']=$this->home_model->getPopularItems();

                $this->load->view('static/head', $data);
                $this->load->view('static/header');
                $this->load->view('content/edit_postad', $data);
                $this->load->view('static/footer_view');
            }
        }
        else
        {
            redirect(base_url().'user/signin');
        }

    }

    public function delItem($item_id)
    {
        $this->postad_model->del_Item($item_id);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function test()
    {
        echo $_SERVER['REMOTE_ADDR'].'<br>';
        echo $_SERVER['HTTP_USER_AGENT'].'<br>';
        echo $_SERVER['HTTP_HOST'].'<br>';
    }

}
?>
